#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif
#include "Sgxpass.hpp"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Argument.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/InstIterator.h"

#include "llvm/IR/Instructions.h"
#include "llvm/IR/Operator.h"

class ReplaceVal final {
public:
  ReplaceVal(Instruction* I, unsigned int i, Value* V) {
    inst = I;
    index = i;
    val = V;
  }

public:
  Instruction* inst;
  unsigned int index;
  Value* val;
};

//---------------------------------
// Sgxpass::user_gep_op
//---------------------------------

// For all users type GEPOperator, globalVariable is changed with adequate globals
// From:
// %1 = load i32, i32* getelementptr inbounds (%struct.A, %struct.A* @sa, i32 0, i32 0), align 4, !dbg !31
// To:
// %1 = load %globals_type*, %globals_type** @globals
// %2 = getelementptr inbounds %globals_type, %globals_type* %1, i32 0, i32 5
// %3 = getelementptr inbounds %struct.A, %struct.A* %2, i32 0, i32 0
// %4 = load i32, i32* %3, align 4, !dbg !31

void Sgxpass::user_gep_op(unsigned int i, GlobalVariable* GV,
			  GEPOperator* UO, Type* globals_type,
			  std::vector<ReplaceVal>* replace_users) {
  for (User *UU : UO->users()) {
    if (dyn_cast<PHINode>(UU) != nullptr) {
      // If the instruction is a PHINode
      // No instruction could be added before this instruction
      // then the global variable will be replaced with a getelementptr constant
      // in change_globals
      continue;
    }
    Instruction* UOI = dyn_cast<Instruction>(UU);
    if (UOI != nullptr) {
      unsigned int index = 0;
      for (unsigned int j = 0; j < UOI->getNumOperands(); j++) {
	if (UO == UOI->getOperand(j)) {
	  index = j;
	  break;
	}
      }

      LoadInst *LI = new LoadInst(GV->getValueType(), GV, "", UOI);

      SmallVector<Value*, 2> gi_args;
      gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(GV->getContext()), 0));
      gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(GV->getContext()), i));
      GetElementPtrInst *GI = GetElementPtrInst::CreateInBounds(globals_type, LI,
								gi_args, "", UOI);

      SmallVector<Value*, 32> go_args;
      for (auto in = UO->idx_begin(); in != UO->idx_end(); ++in) {
	go_args.push_back(*in);
      }
      GetElementPtrInst *GO = GetElementPtrInst::CreateInBounds(GI->getResultElementType(), GI,
								go_args, "", UOI);

      replace_users->push_back(ReplaceVal(UOI, index, GO));
    }
  }
}

//---------------------------------
// Sgxpass::user_load_store
//---------------------------------

// For all users type LoadInst, globalVariable is changed with adequate globals
// From:
// %1 = load i32, i32* @a, align 4, !dbg !25
// To:
// %1 = load %globals_type*, %globals_type** @globals
// %2 = getelementptr inbounds %globals_type, %globals_type* %1, i32 0, i32 4
// %3 = load i32, i32* %2, align 4, !dbg !25

void Sgxpass::user_load_store(unsigned int i, GlobalVariable* GV,
			      Instruction* I, Type* globals_type,
			      std::vector<ReplaceVal>* replace_users,
			      unsigned int index) {

  LoadInst *LI = new LoadInst(GV->getValueType(), GV, "", I);

  SmallVector<Value*, 2> gi_args;
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(GV->getContext()), 0));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(GV->getContext()), i));
  GetElementPtrInst *GI = GetElementPtrInst::CreateInBounds(globals_type, LI,
							    gi_args, "", I);
  replace_users->push_back(ReplaceVal(I, index, GI));
}

//---------------------------------
// Sgxpass::create_globals
//---------------------------------

// create a structure of all Untrusted global variables named globals
// replace all Untrusted GlobalVariable uses with a element of globals
// Deleted all Untrusted GlobalVariables
// From:
// @a = global i32 2, align 4, !dbg !0
// @b = global i32 3, align 4, !dbg !6
// To:
// %globals_type = type { i32, i32 }
// @globals_val = global %globals_type { i32 2, i32 3 }
// @globals = global %globals_type* @globals_val

Type* Sgxpass::create_globals(Module &M) {

  std::vector<Type*> types_vect;
  std::vector<Type*> new_types_vect;
  // Loop over all of the global variables
  // and add all Untrusted global variables' details to the vector
  for (auto& G : M.getGlobalList()) {
    Value *V = dyn_cast<Value>(&G);
    if (G.hasName()) {
      if (G.getName().str().compare("llvm.global.annotations") == 0) {
	continue;
      }

      // In case where monochromatique function have changed type copying it will raise errors
      if (G.getName().str().find("__monochromatic__") == 0) {
	continue;
      }

      // In case where monochromatique function have changed type copying it will raise errors
      if (G.getName().str().find("llvm.used") == 0) {
	continue;
      }

      // In case where string generated by llvm: one that starts with ".str" is found we ignore it
      // these strings will be copied to all enclaves
      if (G.getName().str().find(".str") == 0) {
	global_strs.emplace(&G);
	continue;
      }
    }
    if (Values.find(V) == Values.end()) {
      globals_vect.push_back(&G);
      types_vect.push_back(G.getValueType());
      new_types_vect.push_back(get_changed_type(G.getValueType()));
    }
  }

  // Create the GlobalVariable globals
  globals_type = StructType::create(M.getContext(), types_vect, "old_globals_type");
  new_globals_type = StructType::create(M.getContext(), new_types_vect, "globals_type");
  StructMap.emplace(std::make_pair(globals_type, new_globals_type));

  GlobalVariable *GV_val;
  GV_val = new GlobalVariable(M, globals_type, 0,
			      GlobalValue::LinkageTypes::ExternalLinkage,
			      (Constant*) nullptr, "old_globals_val", (GlobalVariable*) nullptr);
  GlobalVariable *GV;
  GV = new GlobalVariable(M, globals_type->getPointerTo(), 0,
			  GlobalValue::LinkageTypes::ExternalLinkage,
			  GV_val, "old_globals", (GlobalVariable*) nullptr);
  GlobalVariable *new_GV_val;
  new_GV_val = new GlobalVariable(M, new_globals_type, 0,
			      GlobalValue::LinkageTypes::ExternalLinkage,
			      (Constant*) nullptr, "globals_val", (GlobalVariable*) nullptr);
  GlobalVariable *new_GV;
  new_GV = new GlobalVariable(M, new_globals_type->getPointerTo(), 0,
			  GlobalValue::LinkageTypes::ExternalLinkage,
			  new_GV_val, "globals", (GlobalVariable*) nullptr);

  // for users type Instructions
  std::vector<ReplaceVal> replace_users;
  // replace all Untrusted GlobalVariable uses with a element of globals
  // Instruction Level
  for (unsigned int i=0; i<globals_vect.size(); ++i) {
    for (User *U : globals_vect[i]->users()) {
      Instruction *I = dyn_cast<Instruction>(U);
      if (I != nullptr) {
	unsigned int index = 0;
	for (unsigned int j = 0; j < I->getNumOperands(); j++) {
	  if (globals_vect[i] == I->getOperand(j)) {
	    index = j;
	    break;
	  }
	}
	if (dyn_cast<PHINode>(I) != nullptr) {
	  // If the instruction is a PHINode
	  // No instruction could be added before this instruction
	  // then the global variable will be replaced with a getelementptr constant
	  // in change_globals
	} else {
	  user_load_store(i, GV, dyn_cast<Instruction>(U), globals_type, &replace_users, index);
	}
      }

      GEPOperator *UO = dyn_cast<GEPOperator>(U);
      if (UO != nullptr) {
	user_gep_op(i, GV, UO, globals_type, &replace_users);
	continue;
      }
    }
  }

  // Replace the instruction operand with the modified instruction
  for (auto &op : replace_users) {
    op.inst->setOperand(op.index, op.val);
  }

  return new_GV->getValueType();
}

void Sgxpass::mutate_user_type(User *U, std::set<User*>* changed) {
  if(changed->find(U) != changed->end()) {
    return;
  }
  changed->emplace(U);

  if(dyn_cast<Instruction>(U)){
    return;
  }

  U->mutateType(get_changed_type(U->getType()));

  for (User *Us : U->users()) {
    mutate_user_type(Us, changed);
  }
}

//---------------------------------
// Sgxpass::change_globals
//---------------------------------

void Sgxpass::change_globals(Module &M) {
  GlobalVariable *GV_val = M.getGlobalVariable("old_globals_val");
  GlobalVariable *new_GV_val = M.getGlobalVariable("globals_val");
  GlobalVariable *GV = M.getGlobalVariable("old_globals");
  GlobalVariable *new_GV = M.getGlobalVariable("globals");

  // change all Untrusted GlobalVariable and it's users type
  // GlobalVariable Level
  std::set<User*> changed;
  for (unsigned int i=0; i<globals_vect.size(); ++i) {
    mutate_user_type(globals_vect[i], &changed);
  }

  // replace all Untrusted GlobalVariable uses with a element of globals
  // GlobalVariable Level
  for (unsigned int i=0; i<globals_vect.size(); ++i) {
    SmallVector<Value*, 2> gi_args;
    gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), 0));
    gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), i));
    Constant *gep_cst = ConstantExpr::getGetElementPtr(new_globals_type,
						       new_GV_val,
						       gi_args);
    globals_vect[i]->replaceAllUsesWith(gep_cst);
  }

  // Create the constant to initialize GV_val
  std::vector<Constant*> constants_vect;
  std::vector<Constant*> new_constants_vect;
  unsigned i =0;
  for (GlobalVariable *G : globals_vect) {
    if (G->hasInitializer() && G->getValueType() == get_changed_type(G->getValueType())) {
      new_constants_vect.push_back(G->getInitializer());
    } else if (G->hasInitializer() && G->getValueType()->isPointerTy() && !(G->getInitializer()->isNullValue())) {
      new_constants_vect.push_back(G->getInitializer());
    }
    else {
      // change the old null value with new type null value
      Constant * sc = Constant::getNullValue(new_globals_type->getElementType(i));
      new_constants_vect.push_back(sc);
    }

    // Delete the GlobalVariable
    G->eraseFromParent();
    ++i;
  }

  // create constant for new_GV_val
  Constant *new_globals_constant = ConstantStruct::get(new_globals_type, new_constants_vect);

  // Initialize new_GV_val
  new_GV_val->setInitializer(new_globals_constant);

  GV->mutateType(new_GV->getType());
  GV->replaceAllUsesWith(new_GV);
  GV->eraseFromParent();
  GV_val->eraseFromParent();
}


//---------------------------------
// Sgxpass::change_globals
//---------------------------------

// Change the structtype of colored global variables
void Sgxpass::change_colored_globals(Module &M) {
  for (auto& G : M.getGlobalList()) {
    std::set<User*> changed;
    Value *V = dyn_cast<Value>(&G);
    if (Values.find(V) == Values.end()) {
      continue;
    }
    if (G.getValueType() == get_changed_type(G.getValueType())) {
      continue;
    }
    std::string name = "";
    if (G.hasName()) {
      name = G.getName();
      G.setName("old."+name);
    }
    GlobalVariable *GV;
    GV = new GlobalVariable(M,
			    get_changed_type(G.getValueType()),
			    G.isConstant(), G.getLinkage(),
			    Constant::getNullValue(get_changed_type(G.getValueType())), name,
			    (GlobalVariable*) nullptr);

    mutate_user_type(&G, &changed);
    G.replaceAllUsesWith(GV);
    Values[GV] = Values[V];
  }
}
