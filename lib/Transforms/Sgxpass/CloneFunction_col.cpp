//===- CloneFunction.cpp - Clone a function into another function ---------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file implements the CloneFunctionInto interface, which is used as the
// low-level function cloner.  This is used by the CloneFunction and function
// inliner to do the dirty work of copying the body of a function around.
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Analysis/ConstantFolding.h"
#include "llvm/Analysis/DomTreeUpdater.h"
#include "llvm/Analysis/InstructionSimplify.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DebugInfo.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/Module.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
#include <set>
#include <map>

#include "Sgxpass.hpp"

#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Verifier.h"

#include "llvm/IR/DataLayout.h"

using namespace llvm;

// return true if F and CF has at least one common color
bool Sgxpass::has_common_col(const Function* F, const Function* CF) {
  for (auto col : RewriteAttrs[F].Colors) {
    if (RewriteAttrs[CF].Colors.find(col) != RewriteAttrs[CF].Colors.end()) {
      return true;
    }
  }
  return false;
}

BasicBlock* Sgxpass::getPredecessor(const BasicBlock* BB, ValueToValueMapTy &VMap,
				    std::set<const BasicBlock*>* BBToDelete) {
  const BasicBlock *NBB = BB->getPrevNode();
  if (NBB == nullptr) {
    return nullptr;
  }

  if (BBColor.find(NBB) != BBColor.end()) {
    return getPredecessor(NBB, VMap, BBToDelete);
  }

  if (BBToDelete->find(NBB) != BBToDelete->end()) {
    return getPredecessor(NBB, VMap, BBToDelete);
  }

  const BranchInst *BI = dyn_cast<BranchInst>(NBB->getTerminator());
  if (BI == nullptr) {
    return nullptr;
  }

  if (Insts.find(BI) != Insts.end()) {
    if (Insts[BI].compare("") != 0) {
      if (BI->isConditional()) {
	return dyn_cast<BasicBlock>(VMap[NBB]);
      } else {
	return getPredecessor(NBB, VMap, BBToDelete);
      }
    }
  }

  return nullptr;
}

// Return the basicblock where sgxlr_cont and sgxlr_wait will be added
const BasicBlock* Sgxpass::successor_wait(const BranchInst* BI, Function* F,
					  std::set<const BasicBlock*>* treated) {
  const DominatorTree DT = DominatorTree (*F);
  const BasicBlock *root = BI->getParent();
  const auto *DTN = DT.getNode (root);
  const auto *prev = DTN->getIDom();
  if (prev == nullptr) {
    return nullptr;
  }
  treated->insert(prev->getBlock());
  const auto &Children = prev->getChildren();
  for (const auto N : Children) {

    if (BBColor.find(N->getBlock()) == BBColor.end() &&
	treated->find(N->getBlock()) == treated->end() &&
	N->getBlock()->hasNPredecessorsOrMore(2)) {
      return N->getBlock();
    } else {
      treated->insert(N->getBlock());
    }
  }
  const BranchInst *BBI = dyn_cast<BranchInst>(prev->getBlock()->getTerminator());
  if (BBI == nullptr) {
    return nullptr;
  }
  return successor_wait(BBI, F, treated);
}

// Send non colored arguments in case of sgxlr_spawn
void Sgxpass::send_arguments(const Function *F, std::string color, Module *M, const CallInst *CI, Instruction *spawn, ValueToValueMapTy &VMap) {
  std::vector<std::string> col_vect = RewriteAttrs[F].col_vect;
  Instruction *base = spawn;
  for (unsigned i=0; i<col_vect.size(); ++i) {
    if (col_vect[i].compare("") == 0) {
      Value *V = VMap[CI->getArgOperand(i)];
      // this case occurs if V is a constant directly send through argument
      if (V == nullptr) {
	V = CI->getArgOperand(i);
      }
      Instruction* cont = sgxlr_continue_after(F, color, M, V, base, VMap);
      base = cont;
    }
  }
}


// create the Instruction to call sgxlr_spawn to spawn function F
// <badref> = call addrspace(0) i64 @_sgxlr_spawn(i64 2, i32 0)
// After inserting the instruction in the function
// %1 = call i64 @_sgxlr_spawn(i64 1, i32 0)
Instruction* Sgxpass::spawn_color(Function *F, Module *M, std::string color) {

  auto pos = AnnotationList.find(color);
  int col_num = std::distance(AnnotationList.begin(), pos);
  auto fpos = VectFuncs.find(F);
  int fnum = std::distance(VectFuncs.begin(), fpos);
  Function* _sgxlr_spawn = M->getFunction("_sgxlr_spawn");
  SmallVector<Value*, 2> args;

  args.push_back(llvm::ConstantInt::get(llvm::Type::getInt64Ty(M->getContext()), col_num));
  args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(M->getContext()), fnum));
  CallInst* CI = CallInst::Create(_sgxlr_spawn, args);
  return CI;

}

// Return a set of color to spawn the called function CF from F
std::set<std::string> Sgxpass::to_be_spawned(std::string color, const CallInst* CI) {
  std::set<std::string> spawn_colors;

  const Function *F = CI->getFunction();
  Function* CF = CI->getCalledFunction();

  if (CF == nullptr) {
    return spawn_colors;
  }

  // If the called function is a monochromatic function there will be no need to create spawn
  if(auto it = find(MonochromaticFuncs.begin(), MonochromaticFuncs.end(), CF);
       it != MonochromaticFuncs.end()) {
    return spawn_colors;
  }

  if (RewriteAttrs.find(CF) != RewriteAttrs.end()) {
    // No need to spawn CF if color is not the master of F
    if (RewriteAttrs[F].master.compare(color) == 0) {
      // case where color is the master of f
      // and CF has a color different from F's colors
      for (auto col : RewriteAttrs[CF].Colors) {
	if (RewriteAttrs[F].Colors.find(col) == RewriteAttrs[F].Colors.end()) {
	  spawn_colors.emplace(col);
	}
      }
    }
  }
  return spawn_colors;
}

bool Sgxpass::to_be_cloned_instruction(std::string color, const Instruction* I) {
  const Function *F = I->getFunction();
  // If the instruction is a CallInst
  const CallInst *CI = dyn_cast<CallInst>(I);
  if (CI != nullptr) {
    Function* CF = CI->getCalledFunction();
    if (CF == nullptr) {
      return true;
    }

    if (CF->hasName()) {
      if (CF->getName().find("llvm.dbg") != std::string::npos) {
	return false;
      }
    }

    // In case of monochomatic function calls
    // Clone adequate colored instructions
    // in case of uncolored execute only once in master
    if(auto it = find(MonochromaticFuncs.begin(), MonochromaticFuncs.end(), CF);
       it != MonochromaticFuncs.end()) {
      update_colors(CF);
      if (Insts.find(I) != Insts.end()) {
	if (Insts[I].compare(color) != 0) {
	  return false;
	}
      } else if (RewriteAttrs[F].master.compare(color) != 0) {
	// if monochromatic and uncolored
	// execute in master
	return false;
      }
      return true;
    }

    if (RewriteAttrs.find(CF) != RewriteAttrs.end()) {
      // No need to clone the CallInst if the color is not present in RewriteAttrs[CF].Colors
      if (RewriteAttrs[CF].Colors.find(color) == RewriteAttrs[CF].Colors.end()) {
	return false;
      }
    }
    return true;
  }

  const ReturnInst *RI = dyn_cast<ReturnInst>(I);
  if (RI != nullptr) {
    return true;
  }

  // if uncolored StoreInst is to store malloc return value, then it will be executed in Untrusted
  const StoreInst *SI = dyn_cast<StoreInst>(I);
  if (SI != nullptr) {
    if (struct_mallocs_rev.find(SI) != struct_mallocs_rev.end()) {
      if (Insts.find(I) != Insts.end()) {
	if (Insts[I].compare("") == 0) {
	  if (color.compare(UNTRUSTED) == 0) {
	    return true;
	  } else {
	    return false;
	  }
	}
      } else {
	if (color.compare(UNTRUSTED) == 0) {
	  return true;
	} else {
	  return false;
	}
      }
    }
  }

  // if instruction is a unclored storeinst, it will be cloned only in master enclave
  if (SI != nullptr) {
    if (Insts.find(I) != Insts.end() &&
	RewriteAttrs.find(F) != RewriteAttrs.end()) {
      if (Insts[I].compare("") == 0) {
	if (RewriteAttrs[F].master.compare(color) == 0) {
	  return true;
	} else {
	  return false;
	}
      }
    } else {
      if (RewriteAttrs[F].master.compare(color) == 0) {
	return true;
      } else {
	return false;
      }
    }
  }

  // For other Instruction
  // Clone only uncolored and adequate colored instruction
  if (Insts.find(I) != Insts.end()) {
    if ((Insts[I].compare("") !=0)
	&& (Insts[I].compare(color) != 0)) {
      return false;
    }
  }

  return true;
}

/// See comments in Cloning.h.
BasicBlock *Sgxpass::CloneBasicBlock_col(std::string color,
					 const BasicBlock *BB, ValueToValueMapTy &VMap,
					 const Twine &NameSuffix, Function *F, Function *OldF,
					 std::map<Instruction*, const BasicBlock*>* branches,
					 std::set<const BasicBlock*>* AddWait,
					 std::set<const BasicBlock*>* AddCont,
					 std::set<GetElementPtrInst*>* TypeChange,
					 ClonedCodeInfo *CodeInfo,
					 DebugInfoFinder *DIFinder) {
  DenseMap<const MDNode *, MDNode *> Cache;
  BasicBlock *NewBB = BasicBlock::Create(BB->getContext(), "", F);
  if (BB->hasName())
    NewBB->setName(BB->getName() + NameSuffix);

  bool hasCalls = false, hasDynamicAllocas = false, hasStaticAllocas = false;
  Module *TheModule = F ? F->getParent() : nullptr;

  // Loop over all instructions, and copy them over.
  for (const Instruction &I : *BB) {

    // these 3 variables collect informations to treat callinst
    bool cloned = to_be_cloned_instruction(color, &I);
    std::set<std::string> spawn_colors;
    bool spawned = false;

    Function* CF = nullptr;

    Instruction *NewInst = nullptr;

    // GetElementPtrInst
    // Add all the GetElementPtrInst to be changed to TypeChange
    const GetElementPtrInst* GEP = dyn_cast<GetElementPtrInst>(&I);
    if (GEP != nullptr && cloned) {
      Type* GTy = GEP->getSourceElementType();
      if (GTy->isStructTy()) {
	StructType* sty = dyn_cast<StructType>(GTy);
	if (StructColors.find(sty) != StructColors.end()) {
	  if (GEP->getNumOperands() > 2) {
	    unsigned int index = dyn_cast<ConstantInt>(GEP->getOperand(2))->getSExtValue();
	    if (StructColors[sty].colors[index].compare("") != 0) {
	      NewInst = I.clone();
	      if (I.hasName())
		NewInst->setName(I.getName() + NameSuffix);
	      NewBB->getInstList().push_back(NewInst);
	      VMap[NewInst] = NewInst; // Add instruction map to value.

	      VMap[&I] = NewInst; // Add instruction map to value.
	      Type *rty = dyn_cast<GetElementPtrInst>(NewInst)->getResultElementType();
	      TypeChange->emplace(dyn_cast<GetElementPtrInst>(NewInst));
	      continue;
	    }
	  }
	}
      }
    }

    const CallInst* CI = dyn_cast<CallInst>(&I);

    // A separate block to treat malloc
    // From C:
    // struct A {
    //    int red color(red);
    //    int pink;
    //    int blue color(blue);
    // };
    // struct A *s_a1 color(blue);
    //
    // inside a function
    // s_a1 = malloc(sizeof(struct A));
    //
    // From:
    // %struct.A = type { i32, i32, i32 }
    // @s_a1 = common global %struct.A* null
    //
    // %43 = call noalias i8* @malloc(i64 12) #4
    // %44 = bitcast i8* %43 to %struct.A*
    // store %struct.A* %44, %struct.A** @s_a1, align 8
    //
    // To:
    // in blue.ll
    // %struct.A = type { i32*, i32, i32* }
    // @s_a1 = common global %struct.A* null
    //
    // %113 = call i8* @malloc(i64 24)
    // %114 = bitcast i8* %113 to %struct.A*
    // %115 = getelementptr inbounds %struct.A, %struct.A* %114, i32 0, i32 0
    // %116 = call i8* @_sgxlr_malloc_out(i64 4, i64 1, i64 2, i32 0)
    // %117 = bitcast i8* %116 to i32*
    // store i32* %117, i32** %115
    // %118 = getelementptr inbounds %struct.A, %struct.A* %114, i32 0, i32 2
    // %119 = call noalias i8* @malloc(i64 4) #3, !dbg !48
    // %120 = bitcast i8* %119 to i32*
    // store i32* %120, i32** %118
    // %121 = bitcast i8* %113 to %struct.A*, !dbg !48
    // store %struct.A* %121, %struct.A** @s_a1, align 8, !dbg !49

    if (CI != nullptr) {
      if (struct_mallocs.find(CI) != struct_mallocs.end()) {
	if ((color.compare(Insts[struct_mallocs[CI]]) == 0) ||
	    (color.compare(UNTRUSTED) == 0 && Insts[struct_mallocs[CI]].compare("") == 0)) {
	  CF = CI->getCalledFunction();

	  DataLayout* DL = new DataLayout(TheModule);
	  Type* sty;
	  if (StoreInst *SI = dyn_cast<StoreInst>(struct_mallocs[CI])) {
	    sty = dyn_cast<PointerType>(SI->getPointerOperandType())->getElementType();
	  } else if (BitCastInst *BI = dyn_cast<BitCastInst>(struct_mallocs[CI])) {
	    sty = BI->getDestTy();
	  }

	  // if the malloc is to allocate an array of structtype
	  // if the structtype is not colored the instruction will just be cloned
	  // else error

	  if (dyn_cast<PointerType>(sty)->getElementType()->isPointerTy()) {
	    StructType *psty = get_base_type(dyn_cast<PointerType>(sty));
	    if (psty != nullptr) {
	      if (!(StructColors[psty].is_colored())) {
		NewInst = I.clone();
		NewBB->getInstList().push_back(NewInst);
		VMap[NewInst] = NewInst;
		VMap[&I] = NewInst;
		continue;
	      } else {
		errs() << "Trying to malloc array of colored struct. Cannot proceed with it\n\n";
	      }
	    }
	  }

	  StructType* oldty = dyn_cast<StructType>(dyn_cast<PointerType>
						   (sty)->getElementType());
	  StructType* ty = dyn_cast<StructType>(dyn_cast<PointerType>
						(get_changed_type
						 (sty))->getElementType());

	  NewInst = CallInst::Create(CF, llvm::ConstantInt::get(llvm::Type::getInt64Ty
								(CI->getContext()),
								DL->getTypeStoreSize(ty)));
	  NewBB->getInstList().push_back(NewInst);
	  VMap[NewInst] = NewInst;
	  VMap[&I] = NewInst;

	  BitCastInst *MBI = new BitCastInst(NewInst, get_changed_type(sty));
	  NewBB->getInstList().push_back(MBI);
	  VMap[MBI] = MBI;

	  PointerType *PST = dyn_cast<PointerType>(sty);
	  StructType *ST;
	  if (PST != nullptr) {
	    ST = dyn_cast<StructType>(PST->getElementType());
	  }
	  if (ST != nullptr) {
	    for (unsigned i=0; i<ST->getNumElements(); ++i) {
	      if (StructColors[ST].colors[i].compare("") != 0) {
		SmallVector<Value*, 2> gi_args;
		gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(CI->getContext()), 0));
		gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(CI->getContext()), i));
		GetElementPtrInst *MGI = GetElementPtrInst::CreateInBounds
		  (cast<PointerType>(MBI->getType()->getScalarType())->getElementType(),
		   MBI,
		   gi_args);
		NewBB->getInstList().push_back(MGI);
		VMap[MGI] = MGI;
		CallInst* MCI;
		if (StructColors[ST].colors[i].compare(color) != 0) {
		  auto fpos = VectFuncs.find(OldF);
		  int fnum = VectFuncs.size() + 1;
		  Function* _sgxlr_malloc_out = TheModule->getFunction("_sgxlr_malloc_out");
		  auto pos = AnnotationList.find(color);
		  int color_from = std::distance(AnnotationList.begin(), pos);
		  pos = AnnotationList.find(StructColors[oldty].colors[i]);
		  int color_to = std::distance(AnnotationList.begin(), pos);
		  SmallVector<Value*, 4> args;
		  Type* eltty = oldty->getElementType(i);
		  args.push_back(llvm::ConstantInt::get(llvm::Type::getInt64Ty(CI->getContext()),
							DL->getTypeStoreSize(eltty)));
		  args.push_back(llvm::ConstantInt::get(llvm::Type::getInt64Ty(CI->getContext()),
							color_from));
		  args.push_back(llvm::ConstantInt::get(llvm::Type::getInt64Ty(CI->getContext()),
							color_to));
		  args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(CI->getContext()),
							fnum));
		  MCI = CallInst::Create(_sgxlr_malloc_out, args);
		  NewBB->getInstList().push_back(MCI);
		  VMap[MCI] = MCI;
		} else {
		  Type* eltty = oldty->getElementType(i);
		  Value* V = llvm::ConstantInt::get(llvm::Type::getInt64Ty(CI->getContext()),
						    DL->getTypeStoreSize(eltty));
		  MCI = dyn_cast<CallInst>(CI->clone());
		  MCI->setOperand(0, V);
		  NewBB->getInstList().push_back(MCI);
		  VMap[MCI] = MCI;
		}
		Type* elttype = dyn_cast<StructType>(dyn_cast<PointerType>
						     (get_changed_type(sty))
						     ->getElementType())->getElementType(i);
		BitCastInst *NBI = new BitCastInst(MCI, elttype);
		NewBB->getInstList().push_back(NBI);
		VMap[NBI] = NBI;

		StoreInst *NSI = new StoreInst(NBI, MGI);
		NewBB->getInstList().push_back(NSI);
		VMap[NSI] = NSI;
	      }
	    }
	  }
	}

	// this part is to treat uncolored modified struct malloc
	// And the struct is a local variable
	// so we detect that struct malloc only through BitCastInst and not StoreInst
	// In this case
	// malloc is executed in UNTRUSTED
	if (dyn_cast<BitCastInst>(struct_mallocs[CI]) &&
	    Insts[&I] == "" || Insts[&I] == UNTRUSTED) {
	  if( RewriteAttrs.find(OldF) != RewriteAttrs.end()) {
	    // Send the allocated pointer from UNTRUSTED by sgxlr_continue
	    // to all other colors of the function
	    if (color == UNTRUSTED) {
	      for (std::string scol : RewriteAttrs[OldF].Colors) {
		if (scol.compare(color) != 0) {
		  // continue is send after the last instruction added in the basicblock
		  sgxlr_continue_after(OldF, scol, TheModule, NewInst, &NewBB->getInstList().back(), VMap);
		}
	      }
	    } else {
	      // Other colors except UNTRUSTED recieve the allocated pointer by sgxlr_wait
	      NewInst = sgxlr_wait(OldF, NewBB, TheModule, VMap);
	      if (I.hasName())
		NewInst->setName(I.getName() + NameSuffix);
	      NewBB->getInstList().push_back(NewInst);
	      if (I.getType() == NewInst->getType()) {
		VMap[NewInst] = NewInst;
		VMap[&I] = NewInst; // Add instruction map to value.
	      } else if (I.getType()->isPointerTy()) {
		IntToPtrInst *TI = new IntToPtrInst(NewInst, I.getType());
		TI->insertAfter(NewInst);
		VMap[TI] = TI;
		VMap[NewInst] = NewInst;
		VMap[&I] = TI; // Add instruction map to value.
	      } else if (I.getType()->isVoidTy()) {
		// in case of void type no need to map NewInst
	      }else {
		TruncInst *TI = new TruncInst(NewInst, I.getType());
		TI->insertAfter(NewInst);
		VMap[NewInst] = NewInst;
		VMap[TI] = TI;
		VMap[&I] = TI; // Add instruction map to value.
	      }
	    }
	  }
	}

	continue;
      }
    }

    // A sperate block to call sgxlr_spawn
    // Modification of only CallInst will be done in an another block if needed
    if (CI != nullptr && CI->getCalledFunction()) {
      CF = CI->getCalledFunction();

      // this block is to treat llvm.ptr.annotation calls
      if (CF->hasName()) {
	if (CF->getName().find("llvm.ptr.annotation") != std::string::npos) {
	  if ((Insts[dyn_cast<Instruction>(CI->getArgOperand(0))].compare(Insts[&I]) != 0)
	      && (Insts[dyn_cast<Instruction>(CI->getArgOperand(0))].compare("") != 0)) {
	    if (Insts[dyn_cast<Instruction>(CI->getArgOperand(0))].compare(color) == 0) {
	      // send the pointer to the annotated color
	      NewInst = sgxlr_continue_after(OldF, Insts[&I], TheModule, CI->getArgOperand(0), &NewBB->getInstList().back(), VMap);
	      VMap[NewInst] = NewInst;
	    } else if (Insts[&I].compare(color) == 0) {
	      // the annotated color will receive the pointer
	      NewInst = sgxlr_wait(OldF, NewBB, TheModule, VMap);
	      NewBB->getInstList().push_back(NewInst);
	      IntToPtrInst *TI = new IntToPtrInst(NewInst, I.getType());
	      TI->insertAfter(NewInst);
	      VMap[TI] = TI;
	      VMap[NewInst] = NewInst;
	      VMap[&I] = TI; // Add instruction map to value.
	    }
	  } else if (Insts[&I].compare(color) == 0) {
	    // Remove llvm.ptr.annotation
	    // when the pointer color and the annotated color are same
	    // when the pointer color is none ("")
	    // Add the annotated pointer directly to map instead of llvm.ptr.annotation
	    VMap[&I] = VMap[CI->getArgOperand(0)];
	  }
	  continue;
	}
      }

      // Ignore llvm.var.annotation functions
      if (CF->hasName()) {
	if (CF->getName().str().find("var.annotation") != std::string::npos) {
	  continue;
	}
      }
      spawn_colors = to_be_spawned(color, CI);
      if (!spawn_colors.empty()) {
	// this variable indicates that the called function will be spawned by F
	spawned = true;
	// merge spawn_colors with RewriteAttrs' spawn_colors to create Global Variable colors
	RewriteAttrs[CI->getCalledFunction()].spawn_colors.insert(spawn_colors.begin(),
								  spawn_colors.end());
	// Create the function which will be spawned from f
	update_colors(CI->getCalledFunction());
      }
    }

    // Branchinst
    // In case of colored BranchInst in other files
    // We clone the colored BranchInst and
    // collect the BranchInst and the appropriate branchSuccessor in a map branches
    const BranchInst* BI = dyn_cast<BranchInst>(&I);
    if (BI != nullptr && !cloned) {
      const BasicBlock *BB1 = branchSuccessor(BI, OldF);
      // If it is a colored while branchinst
      // then we create a return inst
      if (NeedSyncVar.find(BI) != NeedSyncVar.end()) {
	BB1 = nullptr;
      }
      Instruction* NBI;

      if (BB1 != nullptr) {
	NBI = I.clone();
	branches->emplace(std::make_pair(NBI, BB1));
      } else if (F->getReturnType()->isVoidTy()) {
	NBI = ReturnInst::Create(BI->getContext());
      } else {
	NBI = ReturnInst::Create(BI->getContext(), Constant::getNullValue(F->getReturnType()));
      }
      // If the BranchInst is a while case and there is a need to syncronize
      // Then a sgxlr_wait is placed before returninst
      if (NeedSyncVar.find(BI) != NeedSyncVar.end()) {
	if (NeedSyncVar[BI] == 1) {
	  Instruction *waitI = sgxlr_wait(OldF, NewBB, TheModule, VMap);
	  NewBB->getInstList().push_back(waitI);
	}
      }
      NewBB->getInstList().push_back(NBI);
      VMap[&I] = NBI;
      continue;
    }

    // StoreInst
    // to know if the store instruction is colored or uncolored
    bool uncol_store = false;
    const StoreInst *SI = dyn_cast<StoreInst>(&I);

    if (SI != nullptr) {
      if (Insts.find(SI) == Insts.end()) {
	uncol_store = true;
      } else if (Insts[SI].compare("") == 0) {
	uncol_store = true;
      } else if (Insts[SI].compare(UNTRUSTED) == 0) {
	uncol_store = true;
      }
      // add sgxlr_wait instruction if the uncolored storeinst is not executed in this enclave
      if (uncol_store && !cloned) {
	NewInst = sgxlr_wait(OldF, NewBB, TheModule, VMap);
	if (I.hasName())
	  NewInst->setName(I.getName() + NameSuffix);
	NewBB->getInstList().push_back(NewInst);
	VMap[&I] = NewInst; // Add instruction map to value.
      }
    }

    if (CI != nullptr && !cloned) {

      // in case of monochromatic function call
      // add wait if the instruction is not cloned in this enclave
      if (auto it = find(MonochromaticFuncs.begin(), MonochromaticFuncs.end(), CF);
	  it != MonochromaticFuncs.end()) {
	if (BBColor.find(BB) == BBColor.end()) {
	  NewInst = sgxlr_wait(OldF, NewBB, TheModule, VMap);
	  if (I.hasName())
	    NewInst->setName(I.getName() + NameSuffix);
	  NewBB->getInstList().push_back(NewInst);
	  if (I.getType() == NewInst->getType()) {
	    VMap[NewInst] = NewInst;
	    VMap[&I] = NewInst; // Add instruction map to value.
	  } else if (I.getType()->isPointerTy()) {
	    IntToPtrInst *TI = new IntToPtrInst(NewInst, I.getType());
	    TI->insertAfter(NewInst);
	    VMap[TI] = TI;
	    VMap[NewInst] = NewInst;
	    VMap[&I] = TI; // Add instruction map to value.
	  } else if (I.getType()->isVoidTy()) {
	    // in case of void type no need to map NewInst
	  }else {
	    TruncInst *TI = new TruncInst(NewInst, I.getType());
	    TI->insertAfter(NewInst);
	    VMap[NewInst] = NewInst;
	    VMap[TI] = TI;
	    VMap[&I] = TI; // Add instruction map to value.
	  }
	} else {
	  const BranchInst *BrI = dyn_cast<BranchInst>(BB->getTerminator());
	  if (BrI != nullptr) {
	    std::set<const BasicBlock*> treated;
	    const BasicBlock *SW = successor_wait(BrI, OldF, &treated);
	    if (SW != nullptr) {
	      AddWait->emplace(SW);
	    }
	  }
	  continue;
	}
      } else if (Funcs[CF].side_effect) {
	// Synchronize in case of side_effect function call
	// A wait is added if the function is not called in this color
	// And receive the return value if the return value has no color
	bool add_wait = false;
	if (RewriteAttrs[CF].Colors.find(color) == RewriteAttrs[CF].Colors.end()) {
	  add_wait = true;
	  if (!has_common_col(OldF, CF) && RewriteAttrs[OldF].master.compare(color) == 0) {
	    add_wait = false;
	  }
	}
	if (add_wait) {
	  NewInst = sgxlr_wait(OldF, NewBB, TheModule, VMap);
	  if (I.hasName())
	    NewInst->setName(I.getName() + NameSuffix);
	  NewBB->getInstList().push_back(NewInst);
	  if (I.getType() == NewInst->getType()) {
	    VMap[NewInst] = NewInst;
	    VMap[&I] = NewInst; // Add instruction map to value.
	  } else if (I.getType()->isPointerTy()) {
	    IntToPtrInst *TI = new IntToPtrInst(NewInst, I.getType());
	    TI->insertAfter(NewInst);
	    VMap[TI] = TI;
	    VMap[NewInst] = NewInst;
	    VMap[&I] = TI; // Add instruction map to value.
	  } else if (I.getType()->isVoidTy()) {
	    // in case of void type no need to map NewInst
	  }else {
	    TruncInst *TI = new TruncInst(NewInst, I.getType());
	    TI->insertAfter(NewInst);
	    VMap[NewInst] = NewInst;
	    VMap[TI] = TI;
	    VMap[&I] = TI; // Add instruction map to value.
	  }
	}
      }
    }

    if (!cloned && !spawned)
      continue;

    if (cloned) {
      if (DIFinder && TheModule)
	DIFinder->processInstruction(*TheModule, I);

      NewInst = I.clone();

      if (I.hasName())
	NewInst->setName(I.getName() + NameSuffix);
      NewBB->getInstList().push_back(NewInst);
      VMap[&I] = NewInst; // Add instruction map to value.

      hasCalls |= (isa<CallInst>(I) && !isa<DbgInfoIntrinsic>(I));
      if (const AllocaInst *AI = dyn_cast<AllocaInst>(&I)) {
	if (isa<ConstantInt>(AI->getArraySize()))
	  hasStaticAllocas = true;
	else
	  hasDynamicAllocas = true;
      }

      // Change CalledFunction of the CallInst to adequate Function
      // according to the enclave color
      const Function* old_F = BB->getParent();
      if (CI != nullptr && CF != nullptr) {
	bool change = update_colors(CF);
	if (change) {
	  Function* new_F = RewriteAttrs[CF].get_function(color);
	  if (new_F == nullptr) {
	    ValueToValueMapTy VMap1;
	    new_F = CloneFunction_col(color, CF, VMap1);
	  }
	  // First mutate the function type before changing called function
	  // If the functiontype has StructType Elements; then the called function cannot be change
	  dyn_cast<CallInst>(NewInst)->mutateFunctionType(new_F->getFunctionType());
	  dyn_cast<CallInst>(NewInst)->setCalledFunction(new_F);
	  // Change colored argument different from enclave color to null value
	  auto min = std::min( (size_t)CI->arg_size(), new_F->arg_size());
	  for (unsigned i=0; i< min; ++i) {
	    std::string arg_color = RewriteAttrs[CF].col_vect[i];
	    if ((arg_color.compare(color) == 0) ||
		(arg_color.compare("") == 0)) {
	      continue;
	    }
	    auto *arg = CI->getArgOperand(i);
	    auto *new_arg = Constant::getNullValue(arg->getType());
	    dyn_cast<CallInst>(NewInst)->setArgOperand(i, new_arg);
	  }
	  // Syncronize after calling a function with side effect
	  // Send continue to all the colors of caller function,
	  // which are different from called function's colors
	  // The first common color between caller and called function,
	  // will send the continue with return value if the return value has no color
	  // otherwise null
	  if (Funcs[CF].side_effect) {
	    if (RewriteAttrs.find(OldF) != RewriteAttrs.end() &&
		Continue_sent.find(CI) == Continue_sent.end()) {
	      // if the Caller function has other colors than the called function
	      // and the called function is a side effect function then it need to synced
	      for (std::string scol : RewriteAttrs[OldF].Colors) {
		if ((scol.compare(color) != 0) &&
		    (RewriteAttrs[CF].Colors.find(scol) == RewriteAttrs[CF].Colors.end())) {
		  Value* arg_s = nullptr;
		  if (Insts.find(&I) == Insts.end()) {
		    arg_s = NewInst;
		    VMap[NewInst] = NewInst;
		  }
		  sgxlr_continue_after(OldF, scol, TheModule, arg_s, NewInst, VMap);
		  Continue_sent.emplace(CI);
		}
	      }
	    }
	  }
	} else if (auto it = find(MonochromaticFuncs.begin(), MonochromaticFuncs.end(), CF);
		   it != MonochromaticFuncs.end()) {
	  if( RewriteAttrs.find(OldF) != RewriteAttrs.end()) {
	    // in case of monochromatique functions
	    // if instruction is cloned, send continue to all other colors
	    if (BBColor.find(BB) == BBColor.end()) {
	      for (std::string scol : RewriteAttrs[OldF].Colors) {
		if (scol.compare(color) != 0) {
		  Value* sendVal = nullptr;
		  if (uncolor_malloc.find(CI) != uncolor_malloc.end()) {
		    VMap[NewInst] = NewInst;
		    sendVal = NewInst;
		  }
		  sgxlr_continue_after(OldF, scol, TheModule, sendVal, NewInst, VMap);
		}
	      }
	    } else {
	      const BranchInst *BrI = dyn_cast<BranchInst>(BB->getTerminator());
	      if (BrI != nullptr) {
		std::set<const BasicBlock*> treated;
		const BasicBlock *SW = successor_wait(BrI, OldF, &treated);
		if (SW != nullptr) {
		  AddCont->emplace(SW);
		}
	      }
	    }
	  }
	}
      }

      // ReturnInst
      const ReturnInst *RI = dyn_cast<ReturnInst>(&I);
      if (RI != nullptr) {

	if (Insts.find(&I) != Insts.end()) {
	  if ((Insts[&I].compare(color) != 0) &&
	      Insts[&I].compare("") != 0) {
	    if(RI->getReturnValue()) {
	      auto *ret = RI->getOperand(0);
	      if (!(ret->getType()->isVoidTy())) {
		auto *new_ret = Constant::getNullValue(ret->getType());
		NewInst->setOperand(0, new_ret);
	      }
	    }
	  }
	}

	// In case of a returnInst inside a colored basicblock
	// And the colored basicblock is of while case and needed to be syncronized
	// Then a continue is added before cloning the return inst
	if (BBColor.find(BB) != BBColor.end()) {
	  if (BBColor[BB].color.compare(color) == 0) {
	    if (BBColor[BB].needSync) {
	      if (*(BBColor[BB].needSync) == 1) {
		for (std::string scol : RewriteAttrs[OldF].Colors) {
		  if (scol.compare(color) != 0) {
		    sgxlr_continue(OldF, scol, F->getParent(), nullptr, NewInst, VMap);
		  }
		}
	      }
	    }
	  }
	}
      }

      // StoreInst (to create sgxlr_cont)
      // Add sgxlr_cont instruction to all send continue to all color except the present enclave
      if (SI != nullptr && uncol_store && RewriteAttrs.find(OldF) != RewriteAttrs.end()) {
	for (std::string scol : RewriteAttrs[OldF].Colors) {
	  if (scol.compare(color) != 0) {
	    sgxlr_continue_after(OldF, scol, TheModule, nullptr, NewInst, VMap);
	  }
	}
      }

    }

    // spawn called function for needed colors (spawn_colors)
    if (spawned) {
      CF = CI->getCalledFunction();
      // the variable add_continue is true if a sync_wrapper is need to treat the CallInst
      bool add_continue = (!has_common_col(OldF, CF)) && Funcs[CF].side_effect;
      for (std::string col : spawn_colors) {
	Instruction* spawn;
	// As we are already in the case of caller function's master
	// We spawn the sync_wrapper for called function's master
	// And we spawn directly the function for other colors
	if (add_continue && RewriteAttrs[CF].master.compare(col) == 0) {
	  Function* sync_wrap = RewriteAttrs[CF].get_sync_wrapper(OldF);
	  spawn = spawn_color(sync_wrap, TheModule, col);
	} else {
	  spawn = spawn_color(CF, TheModule, col);
	}
	if (NewInst != nullptr) {
	  spawn->insertBefore(NewInst);
	} else {
	  NewInst = spawn;
	  if (I.hasName())
	    NewInst->setName(I.getName() + NameSuffix);
	  NewBB->getInstList().push_back(NewInst);
	}
	SpawnArgs[NewBB->getParent()].push_back(SpawnArg(CF, col, TheModule,
							 CI, spawn, &VMap));
      }

      // We are in caller function's master
      // We collect the continue and return value send by sync_wrapper through a wait
      // Send the value using a continue to other colors of caller function
      if (add_continue) {
	// %258 = getelementptr inbounds %self_type, %self_type* @self, i32 0, i32 2, i32 1
	// %259 = load i64, i64* %258
	// %260 = call i64 @_sgxlr_wait(i32 1, i64 %259)
	NewInst = sgxlr_wait(OldF, NewBB, TheModule, VMap);
	if (I.hasName())
	  NewInst->setName(I.getName() + NameSuffix);
	NewBB->getInstList().push_back(NewInst);
	VMap[NewInst] = NewInst;
	for (std::string scol : RewriteAttrs[OldF].Colors) {
	  if (scol.compare(color) != 0) {
	    // We are in the case where the called function is spawned and has no common color with caller function
	    // So we can collect the return value which has no color and send it to other colors.
	    Value* arg_s = NewInst;
	    // %262 = getelementptr inbounds %self_type, %self_type* @self, i32 0, i32 2, i32 1
	    // %263 = load i64, i64* %262
	    // call void @_sgxlr_cont(i64 4, i32 1, i64 %263, i64 %260)
	    sgxlr_continue_after(OldF, scol, TheModule, arg_s, NewInst, VMap);
	  }
	}
	// After the collect value is sent, the collected value is converted to Instruction's type and added to map instead of NewInst
	// %261 = trunc i64 %260 to i32
	if (I.getType() == NewInst->getType()) {
	  VMap[NewInst] = NewInst;
	  VMap[&I] = NewInst; // Add instruction map to value.
	} else if (I.getType()->isPointerTy()) {
	  IntToPtrInst *TI = new IntToPtrInst(NewInst, I.getType());
	  TI->insertAfter(NewInst);
	  VMap[NewInst] = NewInst;
	  VMap[TI] = TI;
	  VMap[&I] = TI; // Add instruction map to value.
	} else if (I.getType()->isVoidTy()) {
	  // in case of void type no need to map NewInst
	}else {
	  TruncInst *TI = new TruncInst(NewInst, I.getType());
	  TI->insertAfter(NewInst);
	  VMap[NewInst] = NewInst;
	  VMap[TI] = TI;
	  VMap[&I] = TI; // Add instruction map to value.
	}
      }
    }
  }

  if (CodeInfo) {
    CodeInfo->ContainsCalls          |= hasCalls;
    CodeInfo->ContainsDynamicAllocas |= hasDynamicAllocas;
    CodeInfo->ContainsDynamicAllocas |= hasStaticAllocas &&
      BB != &BB->getParent()->getEntryBlock();
  }
  return NewBB;
}

// Clone OldFunc into NewFunc, transforming the old arguments into references to
// VMap values.
//
void Sgxpass::CloneFunctionInto_col(std::string color,
				    Function *NewFunc, Function *OldF, const Function *OldFunc,
				    ValueToValueMapTy &VMap,
				    bool ModuleLevelChanges,
				    SmallVectorImpl<ReturnInst*> &Returns,
				    const char *NameSuffix, ClonedCodeInfo *CodeInfo,
				    ValueMapTypeRemapper *TypeMapper,
				    ValueMaterializer *Materializer) {
  assert(NameSuffix && "NameSuffix cannot be null!");

#ifndef NDEBUG
  for (const Argument &I : OldFunc->args())
    assert(VMap.count(&I) && "No mapping from source argument specified!");
#endif

  // Copy all attributes other than those stored in the AttributeList.  We need
  // to remap the parameter indices of the AttributeList.
  AttributeList NewAttrs = NewFunc->getAttributes();
  NewFunc->copyAttributesFrom(OldFunc);
  NewFunc->setAttributes(NewAttrs);

  // Fix up the personality function that got copied over.
  if (OldFunc->hasPersonalityFn())
    NewFunc->setPersonalityFn(
			      MapValue(OldFunc->getPersonalityFn(), VMap,
				       ModuleLevelChanges ? RF_None : RF_NoModuleLevelChanges,
				       TypeMapper, Materializer));

  SmallVector<AttributeSet, 4> NewArgAttrs(NewFunc->arg_size());
  AttributeList OldAttrs = OldFunc->getAttributes();

  // Clone any argument attributes that are present in the VMap.
  for (const Argument &OldArg : OldFunc->args()) {
    if (Argument *NewArg = dyn_cast<Argument>(VMap[&OldArg])) {
      NewArgAttrs[NewArg->getArgNo()] =
	OldAttrs.getParamAttributes(OldArg.getArgNo());
    }
  }

  NewFunc->setAttributes(
			 AttributeList::get(NewFunc->getContext(), OldAttrs.getFnAttributes(),
					    OldAttrs.getRetAttributes(), NewArgAttrs));

  bool MustCloneSP =
    OldFunc->getParent() && OldFunc->getParent() == NewFunc->getParent();
  DISubprogram *SP = OldFunc->getSubprogram();
  if (SP) {
    assert(!MustCloneSP || ModuleLevelChanges);
    // Add mappings for some DebugInfo nodes that we don't want duplicated
    // even if they're distinct.
    auto &MD = VMap.MD();
    MD[SP->getUnit()].reset(SP->getUnit());
    MD[SP->getType()].reset(SP->getType());
    MD[SP->getFile()].reset(SP->getFile());
    // If we're not cloning into the same module, no need to clone the
    // subprogram
    if (!MustCloneSP)
      MD[SP].reset(SP);
  }

  SmallVector<std::pair<unsigned, MDNode *>, 1> MDs;
  OldFunc->getAllMetadata(MDs);
  for (auto MD : MDs) {
    NewFunc->addMetadata(
			 MD.first,
			 *MapMetadata(MD.second, VMap,
				      ModuleLevelChanges ? RF_None : RF_NoModuleLevelChanges,
				      TypeMapper, Materializer));
  }

  // When we remap instructions, we want to avoid duplicating inlined
  // DISubprograms, so record all subprograms we find as we duplicate
  // instructions and then freeze them in the MD map.
  // We also record information about dbg.value and dbg.declare to avoid
  // duplicating the types.
  DebugInfoFinder DIFinder;

  // A map to collect BranchInst to be modified
  std::map<Instruction*, const BasicBlock*> branches;
  // A set to collect the instructions which will be deleted
  // before remapping the newly created instructions
  std::set<Instruction*> ToDelete;
  std::set<const BasicBlock*> BBToDelete;
  std::set<const BasicBlock*> AddWait;
  std::set<const BasicBlock*> AddCont;
  std::set<GetElementPtrInst*> TypeChange;

  // Loop over all of the basic blocks in the function, cloning them as
  // appropriate.  Note that we save BE this way in order to handle cloning of
  // recursive functions into themselves.
  //
  for (Function::const_iterator BI = OldFunc->begin(), BE = OldFunc->end();
       BI != BE; ++BI) {
    const BasicBlock &BB = *BI;

    if (BBColor.find(&BB) != BBColor.end()) {
      for (const Instruction &I : BB)
	Insts[&I] = BBColor[&BB].color;
    }
    // Create a new basic block and copy instructions into it!
    BasicBlock *CBB = CloneBasicBlock_col(color, &BB, VMap, NameSuffix, NewFunc,
					  OldF, &branches, &AddWait, &AddCont,
					  &TypeChange, CodeInfo,
					  ModuleLevelChanges ? &DIFinder : nullptr);

    // Add basic block mapping.
    VMap[&BB] = CBB;

    // It is only legal to clone a function if a block address within that
    // function is never referenced outside of the function.  Given that, we
    // want to map block addresses from the old function to block addresses in
    // the clone. (This is different from the generic ValueMapper
    // implementation, which generates an invalid blockaddress when
    // cloning a function.)
    if (BB.hasAddressTaken()) {
      Constant *OldBBAddr = BlockAddress::get(const_cast<Function*>(OldFunc),
					      const_cast<BasicBlock*>(&BB));
      VMap[OldBBAddr] = BlockAddress::get(NewFunc, CBB);
    }

    // The continuation of this part is executed only if
    // the newly created BasicBlock has an instruction
    if (CBB->size() == 0) {
      BBToDelete.emplace(&BB);
      continue;
    }

    // If the colored basicblock is a part of whilecase
    // It will be deleted in other files
    if (BBColor.find(&BB) != BBColor.end()) {
      if (BBColor[&BB].color.compare(color) != 0) {
	if (BBColor[&BB].whileCase) {
	  BBToDelete.emplace(&BB);
	  continue;
	}
      }
    }

    // Note return instructions for the caller.
    if (CBB->getTerminator() != nullptr) {
      if (ReturnInst *RI = dyn_cast<ReturnInst>(CBB->getTerminator()))
	Returns.push_back(RI);
    } else {
      BBToDelete.emplace(&BB);
      continue;
    }
  }

  for (DISubprogram *ISP : DIFinder.subprograms())
    if (ISP != SP)
      VMap.MD()[ISP].reset(ISP);

  for (DICompileUnit *CU : DIFinder.compile_units())
    VMap.MD()[CU].reset(CU);

  for (DIType *Type : DIFinder.types())
    VMap.MD()[Type].reset(Type);

  // Add wait instruction at the begining of the successor basicblock
  // in case of monochromatic functions in a colored basicblock
  for (const BasicBlock* WBB : AddWait) {
    if (dyn_cast<BasicBlock>(VMap[WBB])->size() != 0) {
      Instruction *front = dyn_cast<BasicBlock>(VMap[WBB])->getFirstNonPHI();
      sgxlr_wait_before(OldF, front, NewFunc->getParent(), VMap);
    }
  }

  // Add continue instruction at the begining of the successor basicblock
  // in case of monochromatic functions in a colored basicblock
  for (const BasicBlock* CoBB : AddCont) {
    if (dyn_cast<BasicBlock>(VMap[CoBB])->size() != 0) {
      Instruction *front = dyn_cast<BasicBlock>(VMap[CoBB])->getFirstNonPHI();
      for (std::string scol : RewriteAttrs[OldF].Colors) {
	if (scol.compare(color) != 0) {
	  sgxlr_continue(OldF, scol, NewFunc->getParent(), nullptr, front, VMap);
	}
      }
    }
  }

  // A set to collect the modified branchinst Instruction which will be ignored during remap
  // The operand of these instruction is updated during the creation
  std::set<Instruction*> IgnoreRemap;

  // Update the previously cloned instruction with the with the appropriate one
  for (auto &p : branches) {
    Instruction *NBI;
    NBI = BranchInst::Create(dyn_cast<BasicBlock>(VMap[p.second]));
    NBI->insertAfter(p.first);
    IgnoreRemap.emplace(NBI);
    ToDelete.emplace(p.first);
  }

  for (const BasicBlock* BBD : BBToDelete) {
    BasicBlock* NBB = getPredecessor(BBD, VMap, &BBToDelete);
    if (NBB != nullptr) {
      dyn_cast<BasicBlock>(VMap[BBD])->removeFromParent();
      VMap[BBD] = NBB;
    }
  }

  for (auto &I : ToDelete) {
    I->eraseFromParent();
  }

  // to this part only if the function has instructions
  if (NewFunc->getInstructionCount() != 0) {
    // Loop over all of the instructions in the function, fixing up operand
    // references as we go.  This uses VMap to do all the hard work.
    for (Function::iterator BB =
	   cast<BasicBlock>(VMap[&OldFunc->front()])->getIterator(),
	   BE = NewFunc->end();
	 BB != BE; ++BB)

      // Loop over all instructions, fixing each one as we find it...
      for (Instruction &II : *BB) {
	// Ignore newly created branchinsts
	if(IgnoreRemap.find(&II) != IgnoreRemap.end()) {
	  continue;
	}

	PHINode *PN = dyn_cast<PHINode>(&II);
	if (PN != nullptr) {
	  for (unsigned i = 0; i < PN->getNumIncomingValues(); ++i) {
	    if (dyn_cast<Constant>(PN->getIncomingValue(i))) {
	      continue;
	    }
	    // If the value associated to given PHINode is not found in VMap
	    // replace it with NULL value
	    if (!VMap[PN->getIncomingValue(i)]) {
	      PN->setIncomingValue(i, Constant::getNullValue(PN->getType()));
	    }
	  }
	}

	RemapInstruction(&II, VMap,
			 ModuleLevelChanges ? RF_None : RF_NoModuleLevelChanges,
			 TypeMapper, Materializer);

	// This part is to add null values to the newly added predecessor
	BasicBlock *B = II.getParent();
	if (PN != nullptr) {
	  for (auto it = pred_begin(B), et = pred_end(B); it != et; ++it) {
	    BasicBlock* predecessor = *it;
	    if (PN->getBasicBlockIndex(predecessor) == -1) {
	      PN->addIncoming(Constant::getNullValue(PN->getType()), predecessor);
	    }
	  }
	}
      }

    // Register all DICompileUnits of the old parent module in the new parent module
    auto* OldModule = OldFunc->getParent();
    auto* NewModule = NewFunc->getParent();
    if (OldModule && NewModule && OldModule != NewModule && DIFinder.compile_unit_count()) {
      auto* NMD = NewModule->getOrInsertNamedMetadata("llvm.dbg.cu");
      // Avoid multiple insertions of the same DICompileUnit to NMD.
      SmallPtrSet<const void*, 8> Visited;
      for (auto* Operand : NMD->operands())
	Visited.insert(Operand);
      for (auto* Unit : DIFinder.compile_units())
	// VMap.MD()[Unit] == Unit
	if (Visited.insert(Unit).second)
	  NMD->addOperand(Unit);
    }
  }

  // Change type for GetElementPtr
  // Create a new GetElementPtrInst
  // Create a LoadInst
  // Delete the previous GetElementPtrInst
  // Because the type of GetElementPtrInst is different from the ResultElementType
  // from
  // %5 = getelementptr inbounds %struct.A, %struct.A* %4, i32 0, i32 0
  // to
  // %7 = getelementptr inbounds %struct.A, %struct.A* %6, i32 0, i32 0
  // %8 = load i32*, i32** %7

  for (auto *p : TypeChange) {
    SmallVector<Value*, 2> gi_args;
    gi_args.push_back(p->getOperand(1));
    gi_args.push_back(p->getOperand(2));
    GetElementPtrInst *GI = GetElementPtrInst::CreateInBounds(p->getSourceElementType(),
							      p->getOperand(0),
							      gi_args);
    GI->insertAfter(p);
    Type* rty = GI->getResultElementType();
    LoadInst *LI = new LoadInst(rty, GI);
    LI->insertAfter(GI);
    p->replaceAllUsesWith(LI);
    p->eraseFromParent();
  }

  for (Argument &I : NewFunc->args()) {
    if (I.hasByValAttr()) {
      I.addAttr(Attribute::getWithByValType(NewFunc->getContext(),
					    get_changed_type(I.getParamByValType())));
    }
  }

  // As many modifications are done to the newly created function we verify it
  verifyFunction(*NewFunc);
}

/// Return a copy of the specified function and add it to that function's
/// module.  Also, any references specified in the VMap are changed to refer to
/// their mapped value instead of the original one.  If any of the arguments to
/// the function are in the VMap, the arguments are deleted from the resultant
/// function.  The VMap is updated to include mappings from all of the
/// instructions and basicblocks in the function from their old to new values.
///
Function *Sgxpass::CloneFunction_col(std::string color,
				     Function *F, ValueToValueMapTy &VMap,
				     ClonedCodeInfo *CodeInfo) {
  std::vector<Type*> ArgTypes;

  // The user might be deleting arguments to the function by specifying them in
  // the VMap.  If so, we need to not add the arguments to the arg ty vector
  //

  for (const Argument &I : F->args())
    if (VMap.count(&I) == 0) // Haven't mapped the argument to anything yet?
      ArgTypes.push_back(get_changed_type(I.getType()));

  // Create a new function type...
  // New function type with modified new types
  FunctionType *FTy = FunctionType::get(get_changed_type(F->getFunctionType()->getReturnType()),
					ArgTypes, F->getFunctionType()->isVarArg());

  std::string fname = F->getName().str() + std::string("_") + color;

  // Create the new function...
  Function *NewF = Function::Create(FTy, F->getLinkage(), F->getAddressSpace(),
				    fname, F->getParent());

  // Add function to Fn_table in order to create functions only once
  RewriteAttrs[F].add_function(color, NewF);
  FunctionColors[NewF] = color;

  // Loop over the arguments, copying the names of the mapped arguments over...
  Function::arg_iterator DestI = NewF->arg_begin();
  for (const Argument & I : F->args())
    if (VMap.count(&I) == 0) {     // Is this argument preserved?
      DestI->setName(I.getName()); // Copy the name over...
      VMap[&I] = &*DestI++;        // Add mapping to VMap
    }

  SmallVector<ReturnInst*, 8> Returns;  // Ignore returns cloned.

  //ValueToValueMapTy VM;
  MyMapper TypeMapper = MyMapper(StructMap);

  CloneFunctionInto_col(color, NewF, F, F, VMap, F->getSubprogram() != nullptr, Returns, "",
			CodeInfo, &TypeMapper);

  // Once the function NewF is created,
  // we add instructions to send arguments after sgxlr_spawn if needed
  for (auto sp_arg : SpawnArgs[NewF]) {
    //const Value* V = sp_arg.CI->getArgOperand(1);
    ValueToValueMapTy *Vmap = sp_arg.Vmap;
    send_arguments(sp_arg.CI->getCalledFunction(), sp_arg.color, sp_arg.M,
		   sp_arg.CI, sp_arg.spawn, VMap);
  }

  return NewF;
}
