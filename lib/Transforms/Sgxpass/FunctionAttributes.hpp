#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif

#include "InstructionAttributes.hpp"
#include "llvm/IR/Value.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/Operator.h"

#include <map>
#include <set>

// No need to include if only pointers or references are used
namespace llvm {
  class Instruction;
  class GlobalVariable;
}

using namespace llvm;

// To collect the information about the basicblock in Clonefunction_attr phase
class BBAttributes final {
public:
  // constructors/destructors
  BBAttributes() {
    color = "";
    whileCase = false;
    needSync = nullptr;
  }

public:
  std::string color;
  bool whileCase;
  int* needSync;
};

// to collect the global variable with contains the global variable containing the string
class ColorAttributes final {
public:
  ColorAttributes() {
    var = nullptr;
  };
  ColorAttributes(const GlobalVariable& G, int i) {
    var = dyn_cast<GlobalVariable>
      (
       (dyn_cast<ConstantStruct>
	(dyn_cast<ConstantArray>
	 (G.getOperand(0)
	  )->getOperand(i)
	 )->getOperand(1)
	)->getOperand(0)
       );
  };
 ColorAttributes(const CallInst& CI) {
    var = dyn_cast<GlobalVariable>
      (dyn_cast<ConstantExpr>
       (CI.getArgOperand(1)
	)->getOperand(0)
       );
  };
public:
  GlobalVariable* var;
};

class StoreAttributes final {
public:
  // constructors/destructors
  StoreAttributes() {
    source_color = "";
  }
  StoreAttributes(const Function* F)
    : dest(F->arg_size(), false),
      source(F->arg_size(), false)
  {
    source_color = "";
  }

  void update_dest(std::vector<bool> dep, const Function* F){
    update(&dest, dep, F);
  }

  void update_source(std::vector<bool> dep, const Function* F){
    update(&source, dep, F);
  }

private:
  void update (std::vector<bool> *dst, std::vector<bool> src, const Function* F) {
    std::copy(src.begin(), src.begin()+F->arg_size(), dst->begin());
  }

public:
  std::vector<bool> dest;
  std::vector<bool> source;
  std::string source_color;
};

class FunctionAttributes final {
public:
  // constuctors/destructors
  FunctionAttributes();
  FunctionAttributes(const Function* F, int AnnotationNum);
  
  // public methods
  // copy tmp to constraints and clear tmp
  // return true if stabilized
  bool stabilize();

  // add tmp to constraints
  // return true if there is no new constraints added to constraints
  bool fast_stabilize();
  
public:
  std::map<const Instruction*, InstructionAttributes> constraints;
  std::map<const Instruction*, InstructionAttributes> tmp;
  std::set<std::vector<bool>> call;
  std::vector<bool> ret;
  bool terminated;
  bool correctness;
  bool bb_stabilized;
  // set of colors crossed in this function
  std::set<std::string> fn_colors;
  // Collect information about StoreInst
  std::map<Instruction*, StoreAttributes> store;
  bool isMalloc;
  std::set<CallInst*> MallocInsts;
  bool side_effect;
  std::map<const BasicBlock*, InstructionAttributes> BBconstraints;
  bool hasExternalSafe;
};
