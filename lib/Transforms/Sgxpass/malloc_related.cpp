#include "Sgxpass.hpp"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Verifier.h"
#include "llvm/IR/DerivedTypes.h"


//-------------------------------
// Sgxpass::get_malloc_inst
//-------------------------------

// Get the malloc instruction associated to the given Instruction (either StoreInst or BitCastInst)
CallInst* Sgxpass::get_malloc_inst(Instruction* I, Instruction* SI, std::set<Instruction*>* treated) {
  // to avoid cycles while detecting malloc instruction
  if (treated->find(I) == treated->end()) {
    treated->insert(I);
  } else {
    return nullptr;
  }

  StoreInst* SSI = dyn_cast<StoreInst>(SI);

  CallInst* CI = dyn_cast<CallInst>(I);
  if (CI != nullptr && CI->getCalledFunction() && CI->getCalledFunction()->hasName()) {
    if (CI->getCalledFunction()->getName().str().compare("malloc") == 0) {
      Insts[CI] = Insts[SI];
      return CI;
    }
    return nullptr;
  }
  if (dyn_cast<LoadInst>(I)) {
    return nullptr;
  }

  // This test is to eliminate tracking from storeinst of struct's sub elements (sa->a)
  if (dyn_cast<GetElementPtrInst>(I)) {
    return nullptr;
  }

  for (Use& op : I->operands()) {
    Instruction *UI = dyn_cast<Instruction>(op.get());
    if (UI == nullptr) {
      continue;
    }
    CallInst* malloc_inst = get_malloc_inst(UI, SI, treated);

    // Change the color of intermediate instructions
    // only if the base instruction to detect malloc is a StoreInst
    // also to instructions related to non-struct malloc
    if (SSI != nullptr) {
      if (Insts[I].compare("") == 0) {
	if ((Insts[SI].compare("") == 0) &&
	    (SSI->getPointerOperandType() != get_changed_type(SSI->getPointerOperandType()))) {
	  Insts[I] = UNTRUSTED;
	} else {
	  Insts[I] = Insts[SI];
	}
      }
    }
    if (malloc_inst != nullptr) {
      return malloc_inst;
    }
  }
  return nullptr;
}

//-------------------------------
// Sgxpass::detect_malloc
//-------------------------------

void Sgxpass::detect_malloc(Module &M) {
  for (Function &F : M) {
    for (BasicBlock &B : F) {
      for (Instruction &I : B) {
	StoreInst *SI = dyn_cast<StoreInst>(&I);
	BitCastInst *BI = dyn_cast<BitCastInst>(&I);
	if (SI == nullptr && BI == nullptr) {
	  continue;
	}

	Type* ty;
	if (SI != nullptr) {
	  ty = SI->getPointerOperandType();
	} else {
	  ty = BI->getDestTy();
	}

	std::set<Instruction*> treated;
	// We execute get_malloc_inst to all instructions to add colors to non-struct colored malloc
	CallInst *CI = get_malloc_inst(&I, &I, &treated);

	// This part is only for changed struct malloc
	if ((CI != nullptr) &&
	    (ty != get_changed_type(ty))) {
	  // a temprory solution
	  // add UNTRUSTED to function rewrite colors if there is a UNCOLORED malloc
	  // the function will also be cloned in UNTRUSTED
	  if (RewriteAttrs.find(&F) != RewriteAttrs.end() &&
	      ((Insts[&I] == "") || Insts[&I] == UNTRUSTED)) {
	    RewriteAttrs[&F].Colors.emplace(UNTRUSTED);
	  }

	  // To collect information of struct_mallocs
	  // StoreInst is the priority
	  // if the same malloc is collected through BitCast it will be erased
	  if ((struct_mallocs.find(CI) == struct_mallocs.end()) ||
	      (SI != nullptr)) {
	    struct_mallocs[CI] = &I;
	  }
	  if (SI != nullptr) {
	    struct_mallocs_rev[SI] = CI;
	  }
	}
      }
    }
  }

  // This part is to detect uncolored malloc which should be placed on UNTRUSTED
  // It detects the malloc which doesn't belong to struct_mallocs
  for (Function &F : M) {
    for (BasicBlock &B : F) {
      for (Instruction &I : B) {
	CallInst* CI = dyn_cast<CallInst>(&I);
	if (CI != nullptr && CI->getCalledFunction() && CI->getCalledFunction()->hasName()) {
	  if ((CI->getCalledFunction()->getName().str().compare("malloc") == 0) &&
	      (struct_mallocs.find(CI) == struct_mallocs.end()) &&
	      (Insts[&I] == "" || Insts[&I] == UNTRUSTED)) {
	    Insts[&I] = UNTRUSTED;
	    // a temprory solution
	    // add UNTRUSTED to function rewrite colors if there is a UNCOLORED malloc
	    // the function will also be cloned in UNTRUSTED
	    if (RewriteAttrs.find(&F) != RewriteAttrs.end()) {
	      RewriteAttrs[&F].Colors.emplace(UNTRUSTED);
	    }
	    uncolor_malloc.emplace(CI);
	  }

	  //Uncolored free is placed in UNTRUSTED
	  if (CI->getCalledFunction()->getName().str().compare("free") == 0) {
	    if (Insts[&I] == "") {
	      Insts[&I] = UNTRUSTED;
	    }
	  }
	}
      }
    }
  }
}

//-------------------------------
// Sgxpass::is_memory_function
//-------------------------------

// update isMalloc of F to true if a use of CI is a returnInst
bool Sgxpass::is_memory_function(std::string name) {
  for (auto m_name : memory_function_names) {
    if ((name.compare(m_name) == 0)){
      return true;
    }
  }
  return false;
}

/*
//#include "llvm/IR/User.h"

//-------------------------------
// Sgxpass::check_malloc
//-------------------------------

// update isMalloc of F if CF is malloc
// add CI to MallocInsts if return value of CF is a malloc
void Sgxpass::check_malloc(CallInst* CI, Function* CF, Function* F) {
  if (is_malloc(CF->getName().str()) || Funcs[CF].isMalloc) {
    update_isMalloc(CI, F);
    if (!is_struct_malloc(CI)) {
      return;
    }
    // Get module of the function
    Module* M = F->getParent();
    if (malloc_to_xalloc(CI, *M)) {
      CallFuncs[F].emplace(M->getFunction("xalloc"));
    }
    if (!is_malloc(CF->getName().str())) {
      for (auto CallI : Funcs[CF].MallocInsts) {
	if (malloc_to_xalloc(CallI, *M)) {
	  CallFuncs[F].emplace(M->getFunction("xalloc"));
	}
      }
      Funcs[CF].MallocInsts.clear();
    }
  }
  return;
}

//-------------------------------
// Sgxpass::is_struct_malloc
//-------------------------------

// return true if the malloc is then casted into colored struct
bool Sgxpass::is_struct_malloc(CallInst* CI) {
  for (User *U : CI->users()) {
    BitCastInst* BI = dyn_cast<BitCastInst>(U);
    if (BI == nullptr) {
      return false;
    }
    if (!BI->getDestTy()->isPointerTy()) {
      return false;
    }
    if (BI->getDestTy()->getPointerElementType()->isStructTy()) {
      StructType* ST = dyn_cast<StructType>(BI->getDestTy()->getPointerElementType());
      if (ColoredStructs.find(ST) != ColoredStructs.end()) {
	return true;
      }
    }
  }
  return false;
}

//-------------------------------
// Sgxpass::treat_colored_struct
//-------------------------------

void Sgxpass::treat_colored_struct(const CallInst* CI, std::string color) {
  // CI : %3 = call i8* @llvm.ptr.annotation.p0i8(i8* %2, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), i32 14)
  // BI : %2 = bitcast i32* %a4 to i8*
  // GI :   %a4 = getelementptr inbounds %struct.test_color, %struct.test_color* %1, i32 0, i32 0
  // ST : %struct.test_color = type { i32, i32 }
  // GI->getOperand(2) : i32 0 or i32 2 ...
  // index : 0 or 2 ...
  BitCastInst *BI = dyn_cast<BitCastInst>(CI->getArgOperand(0));
  GetElementPtrInst *GI;
  if (BI == nullptr) {
    GI = dyn_cast<GetElementPtrInst>(CI->getArgOperand(0));
  } else {
    GI = dyn_cast<GetElementPtrInst>(BI->getOperand(0));
  }
  if (GI == nullptr) {
    return;
  }
  if (!GI->getSourceElementType()->isStructTy()) {
    return;
  }
  const Module * M = CI->getModule();
  StructType* ST = dyn_cast<StructType>(GI->getSourceElementType());
  unsigned int index = dyn_cast<ConstantInt>(GI->getOperand(2))->getSExtValue();
  std::string new_name = ST->getName().str() + ".sgx." + std::to_string(index);
  std::string new_ST = ST->getName().str() + ".sgx";
  StructType* ST_new;
  LLVMContext& context = ST->getContext();
  if (ColoredStructs.find(ST) == ColoredStructs.end()) {
    std::vector<std::string> attr(ST->getNumElements(), "");
    StructType::create(context, new_ST);
    ColoredStructs.emplace(std::make_pair(ST, attr));
  }
  if (ColoredStructs[ST][index].compare("") == 0) {
    ColoredStructs[ST][index] = color;
    ST_new = StructType::create(context, new_name);
    ST_new->setBody(ST->getElementType(index));
  } else {
    ST_new = M->getTypeByName(new_name);
  }
}

//-------------------------------
// Sgxpass::create_structs
//-------------------------------

// Create structs according to it's color type
void Sgxpass::create_structs(Module &M) {
  for(auto cst : ColoredStructs) {
    StructType *ST = cst.first;
    unsigned int index= 0;
    std::vector<Type *> elt_tys;
    for (auto color : cst.second) {
      if (color.compare("") == 0) {
	elt_tys.push_back(ST->getElementType(index));
      } else {
	std::string elt_name = ST->getName().str() + ".sgx." + std::to_string(index);
	auto elt_type = M.getTypeByName(elt_name);
	elt_tys.push_back(elt_type->getPointerTo());
      }
      ++index;
    }
    std::string new_name = ST->getName().str() + ".sgx";
    auto new_ST = M.getTypeByName(new_name);
    new_ST->setBody(elt_tys);
    errs() << "new type \n" << *new_ST << "\n\n";
  }
}

//-------------------------------------
// Sgxpass::change_struct_getElementPtr
//-------------------------------------

//change colored struct's GetElementPtrInstructions
void Sgxpass::change_struct_getElementPtr(GetElementPtrInst *GI, StructType *ST, Module &M) {

  // Before
  // GI : %18 = getelementptr inbounds %struct.test_color, %struct.test_color* %9, i32 0, i32 2
  // ptr : %9 = bitcast i8* %8 to %struct.test_color*
  // GI->getResultElementType() : i32
  // GI->getType() : i32*
  // After
  // GI : %18 = getelementptr inbounds %struct.test_color.sgx, %struct.test_color.sgx* %9, i32 0, i32 2
  // ptr : %9 = bitcast i8* %8 to %struct.test_color.sgx*
  // GI->getResultElementType() : %struct.test_color.sgx.1*
  // GI->getType() : %struct.test_color_1.sgx.2**

  // FROM
  // %11 = bitcast i8* %10 to %struct.test_color_1*
  // %14 = getelementptr inbounds %struct.test_color, %struct.test_color* %9, i32 0, i32 0
  // %15 = bitcast i32* %14 to i8*
  // %16 = call i8* @llvm.ptr.annotation.p0i8(i8* %15, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), i32 17)

  // TO
  // %11 = bitcast i8* %10 to %struct.test_color_1.sgx*
  // %14 = getelementptr inbounds %struct.test_color.sgx, %struct.test_color.sgx* %9, i32 0, i32 0
  // %15 = load %struct.test_color.sgx.0*, %struct.test_color.sgx.0** %14
  // %16 = getelementptr inbounds %struct.test_color.sgx.0, %struct.test_color.sgx.0* %15, i32 0, i32 0
  //  %17 = bitcast i32* %16 to i8*
  //  %18 = call i8* @llvm.ptr.annotation.p0i8(i8* %17, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), i32 17)

  LLVMContext& context=ST->getContext();
  unsigned int index = dyn_cast<ConstantInt>(GI->getOperand(2))->getSExtValue();
  std::string new_name = ST->getName().str() + ".sgx";
  auto new_ST = M.getTypeByName(new_name);
  auto *ptr = GI->getPointerOperand();

  if (ColoredStructs[ST][index].compare("") != 0) {
    // Make a copy of GI to transfer all uses of GI to new_GI
    SmallVector<Value*, 2> gi_args;
    gi_args.push_back(GI->getOperand(1));
    gi_args.push_back(GI->getOperand(2));
    GetElementPtrInst *new_GI = GetElementPtrInst::CreateInBounds(ptr, gi_args);
    new_GI->setResultElementType( GI->getResultElementType());
    new_GI->setDebugLoc(GI->getDebugLoc());
    new_GI->mutateType(GI->getType());

    // Replace old call with new one.
    GI->replaceAllUsesWith(new_GI);
    ptr->mutateType(new_ST->getPointerTo());

    // Modify GI with new struct types
    GI->setSourceElementType(new_ST);
    GI->mutateType(new_ST->getElementType(index)->getPointerTo());
    GI->setResultElementType(new_ST->getElementType(index));
    // Create LoadInst
    LoadInst *LI = new LoadInst(GI->getResultElementType(), GI);
    LI->setDebugLoc(GI->getDebugLoc());
    LI->insertAfter(GI);
    // Modify new_GI accordingly
    new_GI->setOperand(0,LI);
    new_GI->setOperand(1,llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 0));
    new_GI->setOperand(2,llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 0));
    new_GI->setSourceElementType(dyn_cast<PointerType>(new_ST->getElementType(index))->getElementType());
    new_GI->insertAfter(LI);
  } else {
    ptr->mutateType(new_ST->getPointerTo());
    // Modify GI with new struct types
    GI->setSourceElementType(new_ST);
    GI->mutateType(new_ST->getElementType(index)->getPointerTo());
    GI->setResultElementType(new_ST->getElementType(index));
  }

  return;
}
//-------------------------------
// Sgxpass::update_malloc
//-------------------------------

// update isMalloc of F to true if a use of CI is a returnInst
void Sgxpass::update_isMalloc(CallInst* CI, Function* F) {
  for (User *U : CI->users()) {
    ReturnInst* RI = dyn_cast<ReturnInst>(U);
    if ((RI != nullptr) && !Funcs[F].isMalloc) {
      Funcs[F].isMalloc = true;
      Funcs[F].MallocInsts.emplace(CI);
      stabilized = false;
    }
  }
  return;
}

//-------------------------------
// Sgxpass::is_malloc
//-------------------------------

// update isMalloc of F to true if a use of CI is a returnInst
bool Sgxpass::is_malloc(std::string name) {
  return (name.compare("malloc") ==0);
}

//---------------------------------
// Sgxpass::malloc_to_xalloc
//---------------------------------

// change malloc to xalloc
//if it is a malloc of struct with multiple colors
//return true if malloc is modified to xalloc
bool Sgxpass::malloc_to_xalloc(CallInst* CI, Module &M) {
  Function *CF=CI->getCalledFunction();
  if (is_malloc(CF->getName().str()) &&
      (CI->getFunction()->getName().compare("xalloc") != 0)) {
    Function* xalloc = M.getFunction("xalloc");
    if (xalloc == nullptr) {
      xalloc = create_xalloc(M);
    }
    LLVMContext &context = M.getContext();
    SmallVector<Value*, 16> new_args;
    new_args.push_back(CI->getArgOperand(0));
    new_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt64Ty(context), 42));

    CallInst* new_CI = CallInst::Create(xalloc, new_args, "", CI);
    new_CI->setCallingConv(CI->getCallingConv());
    new_CI->setAttributes(CI->getAttributes());
    new_CI->setDebugLoc(CI->getDebugLoc());
    // Replace old call with new one.
    CI->replaceAllUsesWith(new_CI);
    // take the old ones name
    new_CI->takeName(CI);
    // CI in StructMallocInst will be deleted from parent afterwards
    // CI->eraseFromParent();
    StructMallocInst.emplace(CI);
    return true;
  }
  return false;
}

//---------------------------------
// Sgxpass::create_xalloc
//---------------------------------

// create new function xalloc
Function* Sgxpass::create_xalloc(Module &M) {
  LLVMContext &context = M.getContext();
  IRBuilder<> builder(context);
  Function* malloc = M.getFunction("malloc");
  // Define function's signature
  std::vector<Type*> Integers(2, builder.getInt64Ty());
  auto *funcType = FunctionType::get(builder.getInt8PtrTy(), Integers, false);

  // create the function "xalloc" and bind it to the module with ExternalLinkage,
  // so we can retrieve it later
  Function* xalloc = Function::Create(funcType, Function::ExternalLinkage, "xalloc", M);
  xalloc->setDSOLocal(1);
  xalloc->setReturnDoesNotAlias ();
  // Define the entry block and fill it with an appropriate code
  auto *entry = BasicBlock::Create(context, "entry", xalloc);
  auto args = xalloc->arg_begin();
  builder.SetInsertPoint(entry);
  auto *result = builder.CreateCall(malloc, &(*args));
  builder.CreateRet(result);
  // Verify at the end
  verifyFunction(*xalloc);
  return xalloc;
}
*/
