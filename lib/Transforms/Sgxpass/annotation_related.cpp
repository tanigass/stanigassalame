#include "Sgxpass.hpp"

//---------------------------------
// Sgxpass::arg_correct_compatible
//---------------------------------

// return false in case of ERROR
bool Sgxpass::arg_correct_compatible(const std::vector<std::string> arg_annot, const std::vector<bool> dependancy, const CallInst* CI, const Function* CF, unsigned arg_size){

  // Check correctness with argument compatibility
  if (CF->arg_size() != 0) {
    for (unsigned i=0; i<arg_size-1; i++) {
      for (unsigned j=i+1; j<arg_size; j++) {
	if (dependancy[i] && dependancy[j]) {
	  if (!is_compatible(arg_annot[i], arg_annot[j])) {
	    Insts[dyn_cast<Instruction>(CI)] = "ERROR";
	    ErrorInsts[dyn_cast<Instruction>(CI)].emplace(errorArgCombination(i, j, CF->getName().str(), arg_annot[i], arg_annot[j]));
	    return false;
	  }
	}
      }
    }
  }
  return true;
}


//-------------------------------
// Sgxpass::arg_correct
//-------------------------------

// return false in case of ERROR
bool Sgxpass::arg_correct(const std::vector<std::set<unsigned>> arg_dependancy, const std::vector<std::string> arg_annot, const std::vector<bool> dep, const CallInst* CI, const Function* CF, const Function* F){
  auto arg_size = CF->arg_size();
  if (CF->arg_size() < CI->arg_size()) {
    // example: true for printf
    arg_size = CI->arg_size();
  }
  std::vector<bool> dependancy(arg_size+AnnotationNum, true);
  std::copy(dep.begin(), dep.begin()+CF->arg_size(), dependancy.begin());
  std::copy(dep.begin()+CF->arg_size(), dep.end(), dependancy.begin()+arg_size);

  std::vector<bool> temp(F->arg_size()+AnnotationNum, false);
  // Check correctness with annotation
  for (unsigned i=arg_size; i<dependancy.size(); i++) {
    if (dependancy[i]) {
      // to update Funcs[F].call
      temp[i-arg_size+F->arg_size()] = true;
      for (unsigned j=0; j<arg_size; j++) {
	if (dependancy[j]) {
	  if (!is_compatible(Annotations[i-arg_size], arg_annot[j])) {
	    Insts[dyn_cast<Instruction>(CI)] = "ERROR";
	    ErrorInsts[dyn_cast<Instruction>(CI)].emplace(errorArgColor(j, CI, Annotations[i-arg_size], arg_annot[j]));
	    return false;
	  }
	}
      }
    }
  }

  if(!arg_correct_compatible(arg_annot, dependancy, CI, CF, arg_size)){
    return false;
  }

  // to update Funcs[F].call
  for (unsigned i=0; i<arg_size; i++) {

    if (dependancy[i]) {
      if (arg_annot[i].compare("") != 0){
	auto pos = AnnotationList.find(arg_annot[i]);
	int position = std::distance(AnnotationList.begin(), pos);
	temp[F->arg_size() + position] = true;
      }
      for (auto k : arg_dependancy[i]) {
	temp[k] = true;
      }
    }
  }

  auto ret = Funcs[F].call.emplace(temp);
  if (ret.second) {
    stabilized = false;
  }

  return true;
}

//-------------------------------
// Sgxpass::is_compatible
//-------------------------------

// return true if compatible
bool Sgxpass::is_compatible( std::string a, std::string b){
  if(!(a.compare("") ==0) && !(b.compare("") ==0)){
    return !(a.compare(b));
  } else {
    return true;
  }
}

//-------------------------------
// Sgxpass::update_annot_constraint
//-------------------------------

// return false in case of ERROR
bool Sgxpass::update_annot_constraint(Instruction* I, Function* F, std::string annot){
  auto pos = AnnotationList.find(annot);
  int position = std::distance(AnnotationList.begin(), pos);
  for (unsigned i=F->arg_size(); i<Funcs[F].tmp[I].dependancy.size(); i++){
    if (Funcs[F].tmp[I].dependancy[i]){
      if (!is_compatible(Annotations[i-F->arg_size()], annot)) {
	Insts[I] = std::string("ERROR");
	ErrorInsts[I].emplace(errorAnnotation(I, Annotations[i-F->arg_size()], annot));
	return false;
      }
    }
  }
  Funcs[F].fn_colors.emplace(annot);
  Insts[I] = annot;
  Funcs[F].tmp[I].dependancy[F->arg_size()+position] = true;
  return true;
}

//-------------------------------
// Sgxpass::update_ret
//-------------------------------

// return false in case of ERROR
bool Sgxpass::update_ret(Function* F, Instruction* I){
  /*
  for (unsigned i=F->arg_size(); i<Funcs[F].ret.size(); i++) {
    Funcs[F].ret[i] = Funcs[F].ret[i] || Funcs[F].tmp[I].dependancy[i];
  }
  */
  for (unsigned i=0; i<Funcs[F].ret.size(); i++) {
    Funcs[F].ret[i] = Funcs[F].ret[i] || Funcs[F].tmp[I].dependancy[i];
  }
  std::string temp = "";

  for (unsigned i=F->arg_size(); i<Funcs[F].ret.size(); i++) {
    if (Funcs[F].ret[i]) {
      if(!is_compatible(Annotations[i-F->arg_size()], temp)){
	Insts[I] = std::string("ERROR");
	ErrorInsts[I].emplace(errorRet(F->getName()));
	return false;
      } else {
	Funcs[F].fn_colors.emplace(Annotations[i-F->arg_size()]);
	Insts[I] = Annotations[i-F->arg_size()];
	temp = Annotations[i-F->arg_size()];
      }
    }
  }

  return true;
}

//-------------------------------
// Sgxpass::update_constraint
//-------------------------------

// return false in case of ERROR
bool Sgxpass::update_constraint(Instruction* I, Value* V, Function* F){
  /*
  for (unsigned i=F->arg_size(); i<Funcs[F].tmp[I].dependancy.size(); i++) {
    Funcs[F].tmp[I].dependancy[i] = Funcs[F].tmp[I].dependancy[i] || Funcs[F].tmp[Iu].dependancy[i];
  }
  */
  /*
  for (unsigned i=0; i<Funcs[F].tmp[I].dependancy.size(); i++) {
    Funcs[F].tmp[I].dependancy[i] = Funcs[F].tmp[I].dependancy[i] || Funcs[F].tmp[Iu].dependancy[i];
  }
  */
  Instruction* Iu = dyn_cast<Instruction>(V);
  BasicBlock* BB = dyn_cast<BasicBlock>(V);

  if (Iu != nullptr) {
    Funcs[F].tmp[I].add(Funcs[F].tmp[Iu]);
  } else if (BB != nullptr) {
    Funcs[F].tmp[I].add(Funcs[F].BBconstraints[BB]);
  }
  
  std::string temp = "";

  for (unsigned i=F->arg_size(); i<Funcs[F].tmp[I].dependancy.size(); i++) {
    if (Funcs[F].tmp[I].dependancy[i]) {
      if(!is_compatible(Annotations[i-F->arg_size()], temp)){
	Insts[I] = std::string("ERROR");
	ErrorInsts[I].emplace(errorAnnotation(I, Annotations[i-F->arg_size()], temp));
	return false;
      } else {
	Funcs[F].fn_colors.emplace(Annotations[i-F->arg_size()]);
	Insts[I] = Annotations[i-F->arg_size()];
	temp = Annotations[i-F->arg_size()];
      }
    }
  }
  
  return true;
}

//-------------------------------
// Sgxpass::store_correct
//-------------------------------

// return false in case of ERROR
bool Sgxpass::store_correct(const std::vector<std::set<unsigned>> arg_dependancy, const std::vector<std::string> arg_annot, Instruction* SI, StoreAttributes sa, const CallInst* CI, const Function* CF, Function* F){
  bool arg_dependant = false;
  StoreAttributes store(F);
  store.source_color = sa.source_color;
  std::vector<bool> temp_dest(F->arg_size(), false);
  std::vector<bool> temp_source(F->arg_size(), false);
  // if dest is colored in F
  for (unsigned i=0; i<CF->arg_size(); i++) {
    if (sa.dest[i]) {
      //update temp_dest for store
      for (auto k : arg_dependancy[i]) {
	temp_dest[k] = true;
      }
      if (is_arg_dependant(CI->getArgOperand(i), F)) {
	arg_dependant = true;
      }
      if (arg_annot[i].compare("") != 0) {
	return true;
      }
    }
  }

  // if source is colored in CF
  if ((sa.source_color.compare("") != 0) && (!arg_dependant)) {
    Insts[dyn_cast<Instruction>(CI)] = std::string("ERROR");
    ErrorInsts[dyn_cast<Instruction>(CI)].emplace(errorStoreInst(sa.source_color));
    return false;
  }

  // if source is coloured in F
  for (unsigned i=0; i<CF->arg_size(); i++) {
    if (sa.source[i]) {
      //update temp_source for store
      for (auto k : arg_dependancy[i]) {
	temp_source[k] = true;
      }
      if (arg_annot[i].compare("") != 0) {

	if (!arg_dependant) {
	  Insts[dyn_cast<Instruction>(CI)] = std::string("ERROR");
	  ErrorInsts[dyn_cast<Instruction>(CI)].emplace(errorStoreInst(arg_annot[i]));
	  return false;
	} else if (sa.source_color.compare("") == 0) {
	  store.source_color = arg_annot[i];
	}
      }
    }
  }

  store.update_dest(temp_dest, F);
  store.update_source(temp_source, F);
  auto ret = Funcs[F].store.emplace(std::make_pair(SI, store));
  if (ret.second) {
    stabilized = false;
  }
  return true;
}
