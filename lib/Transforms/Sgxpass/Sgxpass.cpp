#include "Sgxpass.hpp"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Argument.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/InstIterator.h"

//#include "llvm/IR/Dominators.h"

//---------------------------------
// Sgxpass::testing
//---------------------------------
void Sgxpass::testing() {
  //errs() << "\n\n\n";
  //print_with_color(Values);
  //errs() << "\n\n\n";
  //print_with_color(Insts);
  //errs() << "\n\n\n";
  //print_with_color_line(Insts);
  //errs() << "\n\n\n";
  print_error_line(ErrorInsts);
  errs() << "\n\n\n";
  //print_with_color(Values);
  //errs() << "\n\n\n";
  //errs() << "\n\n\n";
  /*
  errs() << "Colored Structs:\n\n";
  for (auto cst : ColoredStructs) {
    errs() << "Struct type: " << *(cst.first) << "\n";
    errs() << "Colors: ";
    for (auto color : cst.second) {
      errs() << color << ", ";
    }
    errs() << "\n\n";
  }
  */
}

//////////////////////////////////////////////////////////////////////////////////////
///////////////////////_first_pass_to_identify_instruction_colors_////////////////////
//////////////////////////////////////////////////////////////////////////////////////
void Sgxpass::first_pass_to_identify_instruction_colors(Module &M){
  //Initialise memory_function_names
  initialise();

  // Create Annotation leaf with all known annotation (llvm.global.annotations)
  treat_global_variable(M);

  // Iterate over all functions of a Module
  // Update AllFuncs and CallFuncs
  for (Function &F : M) {
    if (!(F.hasName())) {
      continue;
    }

    if ((F.getName()).compare("main") == 0) {
      main=&F;
    } else {
      AllFuncs.push_back(&F);
    }
    // add default monochromatic functions to MonochromaticFuncs list
    treat_default_monochromatic_fn(&F);
    CallFuncs.emplace(&F,std::set<Function*>());
    // Iterate over all BasicBlocks of a Function
    for (BasicBlock &B : F) {
      // Iterate over all Instructions of a BasicBlock
      for (Instruction &I: B) {
	auto CI = dyn_cast<CallInst>(&I); // auto = CallInst*
	if (CI != nullptr) {
	  // check if the called function is a function pointer
	  if (!(CI->isIndirectCall()) && !(CI->isInlineAsm())){
	    Function *CF=CI->getCalledFunction();
	    if (CF && CF->hasName()) {
	      if (CF->getName().find(".annotation") != std::string::npos) {
		// Create Annotation leaf with all known annotation
		//(llvm.var.annotation, llvm.ptr.annotation)
		treat_local_variable(CI, I, CF);
	      } else if (CF->getName().find("llvm.dbg") == std::string::npos) {
		// This part is to filter llvm generated functions for debugging
		CallFuncs[&F].emplace(CF);
	      }
	    }
	  }
	}
      }
    }
  }
  /*
  create_structs(M);

  for (Function &F : M) {
    for (BasicBlock &B : F) {
      // Iterate over all Instructions of a BasicBlock
      for (Instruction &I: B) {
	//GetElementPtrInst* GI = dyn_cast<GetElementPtrInst>(&I);
	auto GI = dyn_cast<GetElementPtrInst>(&I);
	if (GI != nullptr) {
	  if (GI->getSourceElementType()->isStructTy()) {
	    StructType* ST = dyn_cast<StructType>(GI->getSourceElementType());
	    if (ColoredStructs.find(ST) != ColoredStructs.end()) {
	      change_struct_getElementPtr(GI, ST, M);
	    }
	  }
	}
      }
    }
  }
  */



  // add UNTRUSTED to the list of Annotations
  AnnotationList.emplace(UNTRUSTED);
  AnnotationNum = AnnotationList.size();
  Annotations.resize(AnnotationNum);
  std::copy(AnnotationList.begin(), AnnotationList.end(), Annotations.begin());

  // to write colors in colors.mk
  create_colors_mk();

  //add main at last if main exist
  if(main != nullptr){
    AllFuncs.push_back(main);
  }

  while(!(AllFuncs.empty())){
    Function* cur=AllFuncs.back();
    sort_function(cur);
  }

  stabilized=true;

  for (auto* F : Sorted){
    if(!treat_function(F)){
      correct = false;
      break;
    }
    
    if(!((Funcs[F]).terminated)){
      Sorted2.push_back(F);
    }
  }

  while(!stabilized && correct){
    stabilized=true;
    for (auto F : Sorted2) {
      if(!treat_function(F)){
	correct = false;
	break;
      }
      //treat_function(F);
    }
  }
  /*
  for (auto CI : StructMallocInst) {
    CI->eraseFromParent();
  }
  */

  testing();

  // create wrapper for library functions
  create_library_functions(M);

  // create wrapper for external safe functions
  // these wrappers are created after library function
  // so that these will not be considered as library functions
  create_ext_safe_interfaces(M);

  //create different colored file .bc
  create_colored_file(M);

}

//////////////////////////////////////////////////////////////////////////////////////
/////////////////////_first_pass_to_identify_instruction_colors_end_//////////////////
//////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////_MAIN_/////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////


//---------------------------------
// Sgxpass::runOnModule
//---------------------------------

bool Sgxpass::runOnModule(Module &old_M) {

  // Clone the module to leave the old_M unchanged and avoid problems at the end of ModulePass
  std::shared_ptr<llvm::Module> M_ptr = std::move(llvm::CloneModule(old_M));

  // All annalysis and changed will be done in this new module M
  Module& M = *M_ptr;
  //Check correctness of the code and detect all colored instruction in Insts
  first_pass_to_identify_instruction_colors(M);

  return true;
}

static RegisterPass<Sgxpass> X("sgxpass", "a pass for sgx");
char Sgxpass::ID = 0;

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////_MAIN_END_//////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
