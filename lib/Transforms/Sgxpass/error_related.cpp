#include "error_related.hpp"
//---------------------------------
// errorAnnotation
//---------------------------------

LocalError errorAnnotation(const Instruction * I, std::string color1, std::string color2){
  LocalError error(ErrorType::AnnotationError);
  error << "The line has at least two incompatible colors: "<<color1<< ", " <<color2 << " ...";
  return error;
}

//---------------------------------
// errorArgColor
//---------------------------------

LocalError errorArgColor(unsigned int i, const CallInst * CI, std::string color1, std::string color2){
  LocalError error(ErrorType::ArgumentError);
  error << "The argument number "<< i <<" of function "<<CI->getCalledFunction()->getName().str()<<" should be compatible with "<<color1<<" whereas it is "<<color2;
  return error;
}
 
//---------------------------------
// errorFunction
//---------------------------------

LocalError errorFunction(std::string name){
  LocalError error(ErrorType::FunctionError);
  error << "The called function " << name <<" has errors";
  return error;
}

//---------------------------------
// errorFunctionBB
//---------------------------------

LocalError errorFunctionBB(std::string name, std::string color1, std::string color2){
  LocalError error(ErrorType::FunctionBBError);
  error << "The current BasicBlock should be " << color1 << ", whereas the called function "<< name <<" manipulates "<<color2;
  return error;
}

//---------------------------------
// errorStoreInst
//---------------------------------

LocalError errorStoreInst(std::string source_color){
  LocalError error(ErrorType::StoreError);
  error << "A trusted data of color " << source_color << " cannot be stored in a untrusted variable";
  return error;
}

//---------------------------------
// errorArgCombination
//---------------------------------

LocalError errorArgCombination(unsigned int i, unsigned int j, std::string name, std::string color1, std::string color2){
  LocalError error(ErrorType::ArgCombinationError);
  error << "The Colors of arguments number " << i << " and " << j << " of the function " << name <<" should be compatible,"<< " whereas they are "<<color1 << " and " << color2 << " respectively";
  return error;
}

//---------------------------------
// errorArgAnnotation

//---------------------------------

LocalError errorRet(std::string name){
  LocalError error(ErrorType::ReturnError);
  error << "The return values of the function " << name <<" have at least two incompatible colors";
  return error;
}

//---------------------------------
// errorArgAnnotation
//---------------------------------

LocalError errorArgAnnotation(unsigned int i, std::string name){
  LocalError error(ErrorType::ArgAnnotationError);
  error << "The given argument number " << i << " of the function " << name <<" contatins error";
  return error;
}

//---------------------------------
// errorCallInstStore
//---------------------------------

LocalError errorCallInstStore(unsigned int i,unsigned int j, std::string name, std::string source_color){
  LocalError error(ErrorType::CallInstStoreError);
  error << "By calling the function " << name << " with argument number " << i <<" uncolored and argument number " << j << " colored, a trusted data of color " << source_color << " is stored in a untrusted variable" ;
  return error;
}

//---------------------------------
// errorCallInstStore
//---------------------------------

LocalError errorCallInstStore(unsigned int j, std::string name, std::string source_color){
  LocalError error(ErrorType::CallInstStoreError);
  error << "By calling the function " << name << " with argument number " << j << " colored, a trusted data of color " << source_color << " is stored in a untrusted variable" ;
  return error;
}

//---------------------------------
// errorCallInstStore
//---------------------------------

LocalError errorCallInstStore(const Instruction* I){
  LocalError error(ErrorType::CallInstStoreError);
  error << "Error comes from line " << get_line_number(I) << " of file " << get_file_name(I) ;
  return error;
}
