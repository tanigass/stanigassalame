#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif
#include "Sgxpass.hpp"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Argument.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/InstIterator.h"

#include "llvm/IR/Instructions.h"
#include "llvm/IR/Operator.h"

#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Verifier.h"

//---------------------------------
// Sgxpass::create_constructor
//---------------------------------

// create init function
// @0 = private unnamed_addr constant [5 x i8] c"blue\00", align 1
// @1 = private unnamed_addr constant [6 x i8] c"green\00", align 1
// define i32 @init() {
// entry:
//   %0 = load %globals_type*, %globals_type** @globals
//   %1 = bitcast %globals_type* %0 to i8*
//   call void (i8*, i64, ...) @sgxlr_initialize(i8* %1, i64 3, [5 x i8]* @0, [6 x i8]* @1)
//   call void @sgxlr_start_colored_thread()
//  ret void
// }
//
// Set init as constructor
// %0 = type { i32, void ()*, i8* }
// Global Variable
// @llvm.global_ctors = appending global [1 x %0] [%0 { i32 65535, void ()* @init, i8* null }]

Function* Sgxpass::create_constructor(std::unique_ptr<llvm::Module> &M) {
  LLVMContext &context = M->getContext();
  IRBuilder<> builder(context);
  auto *funcType = FunctionType::get(builder.getVoidTy(), false);
  Function* init = Function::Create(funcType,
				    Function::ExternalLinkage, "init", *M);

  auto *entry = BasicBlock::Create(context, "entry", init);
  builder.SetInsertPoint(entry);

  // call sgxlr_initialize
  Function* sgxlr_initialize = create_sgxlr_initialize(M);
  // %0 = load %globals_type*, %globals_type** @globals
  GlobalVariable *GV = M->getGlobalVariable("globals");
  LoadInst *LI = builder.CreateLoad(GV->getValueType(), GV);
  // %1 = bitcast %globals_type* %0 to i8*
  Value *BI = builder.CreatePointerCast(LI, builder.getInt8PtrTy());
  std::vector<Value*> args;
  // i8* %1
  args.push_back(BI);
  // i64 3
  args.push_back(builder.getInt64(AnnotationNum));
  for (std::string col : AnnotationList) {
    if (col.compare(UNTRUSTED) == 0) {
      continue;
    }
    // @0 = private unnamed_addr constant [5 x i8] c"blue\00", align 1
    GlobalVariable *str_col = builder.CreateGlobalString(col);
    // [5 x i8]* @0
    Value* GEP = builder.CreateInBoundsGEP(str_col->getValueType(), str_col, builder.getInt32(0));
    args.push_back(GEP);
  }
  // call void (i8*, i64, ...) @sgxlr_initialize(i8* %1, i64 3, [5 x i8]* @0, [6 x i8]* @1)
  builder.CreateCall(sgxlr_initialize, args);

  // call void @sgxlr_start_colored_thread()
  Function* sgxlr_start_colored_thread = create_sgxlr_start_colored_thread(M);
  builder.CreateCall(sgxlr_start_colored_thread);

  // ret void
  builder.CreateRetVoid();

  // Verify at the end
  verifyFunction(*init);

  // %"type 0x5572f4eabbe0" = type { i32, void ()*, i8* }
  std::vector<Type *> elt_tys;
  elt_tys.push_back(builder.getInt32Ty());
  elt_tys.push_back(funcType->getPointerTo());
  elt_tys.push_back(builder.getInt8Ty()->getPointerTo());
  StructType* ST = StructType::create(context, elt_tys);

  // %"type 0x5572f4eabbe0" { i32 65535, void ()* @init, i8* null }
  SmallVector<Constant*, 3> st_args;
  st_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 65535));
  st_args.push_back(ConstantExpr::getPointerCast(init, ST->getElementType(1)));
  st_args.push_back(Constant::getNullValue(ST->getElementType(2)));
  Constant* ST_cst = ConstantStruct::get(ST, st_args);

  // [1 x %"type 0x5572f4eabbe0"]
  ArrayType* ctors_type = ArrayType::get(ST, 1);

  // [1 x %"type 0x5572f4eabbe0"] [%"type 0x5572f4eabbe0" { i32 65535, void ()* @init, i8* null }]
  Constant* ctors_cst = ConstantArray::get(ctors_type, ST_cst);

  // @llvm.global_ctors = appending global [1 x %0] [%0 { i32 65535, void ()* @init, i8* null }]
  GlobalVariable *GV_ctors;
  GV_ctors = new GlobalVariable(*M, ctors_type, 0,
                GlobalValue::LinkageTypes::AppendingLinkage,
                ctors_cst, "llvm.global_ctors"
                );

  return init;
}

//---------------------------------
// Sgxpass::create_destructor
//---------------------------------

// create terminate function
// define i32 @terminate() {
// entry:
//   call void @sgxlr_terminate_colored_thread()
//   call void @sgxlr_terminate()
//  ret void
// }
//
// Set terminate as destructor
// %1 = type { i32, void ()*, i8* }
// Global Variable
// @llvm.global_dtors = appending global [1 x %1] [%1 { i32 65535, void ()* @terminate, i8* null }]

Function* Sgxpass::create_destructor(std::unique_ptr<llvm::Module> &M) {
  LLVMContext &context = M->getContext();
  IRBuilder<> builder(context);
  auto *funcType = FunctionType::get(builder.getVoidTy(), false);
  Function* terminate = Function::Create(funcType,
				    Function::ExternalLinkage, "terminate", *M);

  auto *entry = BasicBlock::Create(context, "entry", terminate);
  builder.SetInsertPoint(entry);

  // call void @sgxlr_terminate_colored_thread()
  Function* sgxlr_terminate_colored_thread = create_sgxlr_terminate_colored_thread(M);
  builder.CreateCall(sgxlr_terminate_colored_thread);

  // call void @sgxlr_terminate()
  Function* sgxlr_terminate = create_sgxlr_terminate(M);
  builder.CreateCall(sgxlr_terminate);

  // ret void
  builder.CreateRetVoid();

  // Verify at the end
  verifyFunction(*terminate);

  // %"type 0x5572f4eabbe0" = type { i32, void ()*, i8* }
  std::vector<Type *> elt_tys;
  elt_tys.push_back(builder.getInt32Ty());
  elt_tys.push_back(funcType->getPointerTo());
  elt_tys.push_back(builder.getInt8Ty()->getPointerTo());
  StructType* ST = StructType::create(context, elt_tys);

  // %"type 0x5572f4eabbe0" { i32 65535, void ()* @terminate, i8* null }
  SmallVector<Constant*, 3> st_args;
  st_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 65535));
  st_args.push_back(ConstantExpr::getPointerCast(terminate, ST->getElementType(1)));
  st_args.push_back(Constant::getNullValue(ST->getElementType(2)));
  Constant* ST_cst = ConstantStruct::get(ST, st_args);

  // [1 x %"type 0x5572f4eabbe0"]
  ArrayType* dtors_type = ArrayType::get(ST, 1);

  // [1 x %"type 0x5572f4eabbe0"] [%"type 0x5572f4eabbe0" { i32 65535, void ()* @terminate, i8* null }]
  Constant* dtors_cst = ConstantArray::get(dtors_type, ST_cst);

  // @llvm.global_dtors = appending global [1 x %0] [%0 { i32 65535, void ()* @terminate, i8* null }]
  GlobalVariable *GV_dtors;
  GV_dtors = new GlobalVariable(*M, dtors_type, 0,
                GlobalValue::LinkageTypes::AppendingLinkage,
                dtors_cst, "llvm.global_dtors"
                );

  return terminate;
}

//---------------------------------
// Sgxpass::create_init_terminate
//---------------------------------

// Create constructor init and destructor terminate
void Sgxpass::create_init_terminate(std::unique_ptr<llvm::Module> &M) {
  create_constructor(M);
  create_destructor(M);
}

//---------------------------------
// Sgxpass::create_new_library_functions
//---------------------------------

// Change Untrusted version of library functions name to original name
void Sgxpass::create_new_library_functions(std::unique_ptr<llvm::Module> &M) {
  LLVMContext &context = M->getContext();
  for (Function* F : LibFuncs) {
    Function *F_u = M->getFunction(F->getName().str() + "_" +UNTRUSTED);
    if (F_u == nullptr) {
      continue;
    }
    std::string name = F->getName().str();
    F->setName("old."+name);
    F_u->setName(name);
  }
}

//---------------------------------
// Sgxpass::create_colors_mk
//---------------------------------

// create colors.mk to write all known colors in it
// colors.mk will be used afterwards to make sgx executables
void Sgxpass::create_colors_mk() {
  std::ofstream outfile ("colors.mk");
  outfile << "COLORS =";
  for (auto str : Annotations) {
    if (str.compare(UNTRUSTED) != 0) {
      outfile << " " << str;
    }
  }
  outfile << "\n";
  outfile.close();
}

//---------------------------------
// Sgxpass::create_extern_globals
//---------------------------------

// Create an external GlobalVariable named globals
// @globals = external externally_initialized global %globals_type*
void Sgxpass::create_extern_globals(std::unique_ptr<llvm::Module>& new_module, Type *ST) {
  GlobalVariable *GV;
  GV = new GlobalVariable(*new_module,
			  ST,
			  0,
			  GlobalValue::LinkageTypes::ExternalLinkage,
			  (Constant *) nullptr,
			  "globals");
  GV->setExternallyInitialized(true);
}

//---------------------------------
// Sgxpass::side_effect_function
//---------------------------------

//update the colors in ColorsInsts
bool Sgxpass::side_effect_function(Function* CF) {
  // we don't consider llvm.dbg functions as side effect function;
  // we avoid syncronizations for this
  if (CF->getInstructionCount() == 0 &&
      !((CF->getName().str().find("llvm.dbg.") == 0) ||
	(CF->getName().str().find(".annotation") != std::string::npos))) {
    return true;
  }
  return false;
}

//---------------------------------
// Sgxpass::inst_wait
//---------------------------------

// Create _sgxlr_wait after StoreInst of untrusted global variable
// %11 = getelementptr inbounds %self_type, %self_type* @self, i32 0, i32 2, i32 2
// %12 = load i64, i64* %11
// %13 = call i64 @_sgxlr_wait(i32 2, i64 %12)
void Sgxpass::inst_wait(Module &M, Instruction* I, std::string color) {
  std::vector<std::string> colors;
  for (auto col : Annotations) {
    if(col.compare(color) != 0) {
      colors.push_back(col);
    }
  }
  inst_wait(M, I, color, colors);
}

//---------------------------------
// Sgxpass::inst_wait
//---------------------------------

// Create _sgxlr_wait after StoreInst of untrusted global variable
// %11 = getelementptr inbounds %self_type, %self_type* @self, i32 0, i32 2, i32 2
// %12 = load i64, i64* %11
// %13 = call i64 @_sgxlr_wait(i32 2, i64 %12)
void Sgxpass::inst_wait(Module &M, Instruction* I, std::string color, std::vector<std::string> colors) {
  int fnum = 2;
  SmallVector<Value*, 3> gi_args;
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), 0));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), 2));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), fnum));
  StructType *selftype = M.getTypeByName("self_type");

  GetElementPtrInst *GI = GetElementPtrInst::CreateInBounds(selftype,
							    M.getGlobalVariable("self"), gi_args);

  PlaceCallInst[GI] = I;
  LoadInst *LI = new LoadInst(Type::getInt64Ty(M.getContext()), GI);
  PlaceCallInst[LI] = GI;

  Function* _sgxlr_wait = M.getFunction("_sgxlr_wait");
  SmallVector<Value*, 2> args;
  args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), fnum));
  args.push_back(LI);
  CallInst* CI = CallInst::Create(_sgxlr_wait, args);
  PlaceCallInst[CI] = LI;

  // add wait to all colors in colors list
  for (auto col : colors) {
    ColorInsts[GI].insert(col);
    ColorInsts[LI].insert(col);
    ColorInsts[CI].insert(col);
  }
}

//---------------------------------
// Sgxpass::inst_continue
//---------------------------------

//  %union.sgxlr_value = type { i64 }

//  %11 = getelementptr inbounds %self_type, %self_type* @self, i32 0, i32 2, i32 2
//  %12 = load i64, i64* %11
//  %13 = alloca %union.sgxlr_value
//  %14 = getelementptr inbounds %union.sgxlr_value, %union.sgxlr_value* %13, i32 0, i32 0
//  store i64 0, i64* %14
//  %15 = load i64, i64* %14
//  call void @_sgxlr_cont(i64 3, i32 2, i64 %12, i64 %15)

void Sgxpass::inst_continue(Module &M, Instruction* I, std::string color) {
  std::vector<std::string> colors;
  for (auto col : Annotations) {
    if(col.compare(color) != 0) {
      colors.push_back(col);
    }
  }
  inst_continue(M, I, color, colors);
}

//---------------------------------
// Sgxpass::inst_continue
//---------------------------------

//  %union.sgxlr_value = type { i64 }

//  %11 = getelementptr inbounds %self_type, %self_type* @self, i32 0, i32 2, i32 2
//  %12 = load i64, i64* %11
//  %13 = alloca %union.sgxlr_value
//  %14 = getelementptr inbounds %union.sgxlr_value, %union.sgxlr_value* %13, i32 0, i32 0
//  store i64 0, i64* %14
//  %15 = load i64, i64* %14
//  call void @_sgxlr_cont(i64 3, i32 2, i64 %12, i64 %15)

void Sgxpass::inst_continue(Module &M, Instruction* I, std::string color, std::vector<std::string> colors) {

  Function* _sgxlr_cont = M.getFunction("_sgxlr_cont");
  //Arg1
  int fnum = 2;

  //Arg2
  SmallVector<Value*, 3> gi_args;
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), 0));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), 2));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), fnum));
  StructType *selftype = M.getTypeByName("self_type");

  GetElementPtrInst *GI = GetElementPtrInst::CreateInBounds(selftype,
							    M.getGlobalVariable("self"), gi_args);

  PlaceCallInst[GI] = I;
  LoadInst *LI = new LoadInst(Type::getInt64Ty(M.getContext()), GI);
  PlaceCallInst[LI] = GI;

  //Arg3
  Type *sgxlr_value_ty = M.getTypeByName("union.sgxlr_value");
  if (sgxlr_value_ty == nullptr) {
    sgxlr_value_ty = StructType::create(M.getContext(), Type::getInt64Ty(M.getContext()),
					"union.sgxlr_value");
  }
  AllocaInst *AI = new AllocaInst(sgxlr_value_ty, 0, std::string(""), (Instruction*)nullptr);
  PlaceCallInst[AI] = LI;

  SmallVector<Value*, 2> agi_args;
  agi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), 0));
  agi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), 0));
  GetElementPtrInst *AGI = GetElementPtrInst::CreateInBounds(sgxlr_value_ty,
							    AI, agi_args);
  PlaceCallInst[AGI] = AI;

  StoreInst *SI = new StoreInst(llvm::ConstantInt::get(llvm::Type::getInt64Ty(M.getContext()), 0),
				AGI, (Instruction*)nullptr);
  PlaceCallInst[SI] = AGI;

  LoadInst *ALI = new LoadInst(Type::getInt64Ty(M.getContext()), AGI);
  PlaceCallInst[ALI] = SI;

  ColorInsts[GI].insert(color);
  ColorInsts[LI].insert(color);
  ColorInsts[AI].insert(color);
  ColorInsts[AGI].insert(color);
  ColorInsts[ALI].insert(color);
  ColorInsts[SI].insert(color);

  // create CallInst _sgxlr_cont for each col other than color and place those CallInst in color.ll
  for (auto col : colors) {
    SmallVector<Value*, 4> args;
    //Arg0
    auto pos = AnnotationList.find(col);
    int position = std::distance(AnnotationList.begin(), pos);
    args.push_back(llvm::ConstantInt::get(llvm::Type::getInt64Ty(M.getContext()), position));
    args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), fnum));
    args.push_back(LI);
    args.push_back(ALI);
    CallInst* CI = CallInst::Create(_sgxlr_cont, args);
    PlaceCallInst[CI] = ALI;
    ColorInsts[CI].insert(color);
  }
}

//---------------------------------
// Sgxpass::re_returnInst
//---------------------------------

//update the colors in ColorsInsts
void Sgxpass::re_returnInst(ReturnInst* RI) {
   if (Insts.find(RI) != Insts.end()) {
    ColorInsts[RI].insert(Insts[RI]);
    ReturnInst* NRI = ReturnInst::Create(RI->getContext());
    PlaceCallInst[NRI] = RI;
    ToBeDeleted.insert(NRI);
    for (auto color : Annotations) {
      if (color.compare(Insts[RI]) != 0) {
	ColorInsts[NRI].insert(color);
      }
    }
  } else {
    for (auto color : Annotations) {
      ColorInsts[RI].insert(color);
    }
  }
}
/*
//---------------------------------
// Sgxpass::update_colors
//---------------------------------

//update the colors in ColorsInsts
void Sgxpass::update_colors(Module &M) {
  for (Function &F : M) {
    for (BasicBlock &B : F) {
      for (Instruction &I : B) {
	BranchInst *BI = dyn_cast<BranchInst>(&I);
	if (BI != nullptr) {
	  if (BI->isConditional()) {
	    re_branchInst(BI);
	    continue;
	  }
	}
	ReturnInst *RI = dyn_cast<ReturnInst>(&I);
	if (RI != nullptr) {
	  re_returnInst(RI);
	  continue;
	}
	const CallInst *CI = dyn_cast<CallInst>(&I);
	if (CI != nullptr) {
	  // To omit llvm dbg functions
	  if (CI->getCalledFunction()->getName().str().find("llvm.dbg.") !=
	      std::string::npos) {
	    continue;
	  }
	  if (side_effect_function(CI->getCalledFunction())) {
	    if (Insts.find(&I) != Insts.end()) {
	      ColorInsts[&I].insert(Insts[&I]);
	      inst_wait(M, &I, Insts[&I]);
	      inst_continue(M, &I, Insts[&I]);
	    } else {
	      ColorInsts[&I].insert(UNTRUSTED);
	      inst_wait(M, &I, UNTRUSTED);
	      inst_continue(M, &I, UNTRUSTED);
	    }
	    continue;
	  }
	}
	StoreInst *SI = dyn_cast<StoreInst>(&I);
	if ((SI != nullptr) && (Insts.find(&I) == Insts.end())) {
	  ColorInsts[&I].insert(UNTRUSTED);
	  inst_wait(M, &I, UNTRUSTED);
	  inst_continue(M, &I, UNTRUSTED);
	  continue;
	}
	if (Insts.find(&I) != Insts.end()) {
	  ColorInsts[&I].insert(Insts[&I]);
	} else {
	  for (auto color : Annotations) {
	    ColorInsts[&I].insert(color);
	  }
	}
      }
    }
  }

  // Place all Insts related to sgxlr functions after adequate instructions
  // Sometimes both op.first and op.second could be <badref>
  while (!PlaceCallInst.empty()) {
    for (auto &op : PlaceCallInst) {
      // To check if op.second is not a <badref>
      if (PlaceCallInst.find(op.second) == PlaceCallInst.end()) {
	op.first->insertAfter(op.second);
	// remove referenced instruction from the map PlaceCallInst
	PlaceCallInst.erase(op.first);
      }
    }
  }
}
*/

//---------------------------------
// Sgxpass::update_colors
//---------------------------------

// Rewrite all functions according to it's color
// Return true when the function is rewritten
bool Sgxpass::update_colors(Function* F) {
  // in case of Monochromatic functions copy to all Enclave
  // only the non modified functions
  if (auto it = find(MonochromaticFuncs.begin(), MonochromaticFuncs.end(), F);
      it != MonochromaticFuncs.end()) {
    if (OutsideFns.find(F) == OutsideFns.end()) {
      FunctionColors[F] = "";
      RewriteAttrs[F].add_function(UNTRUSTED, OutsideFns[F]);
    }
    // the function of this case doesn't need to be rewritten
    return false;
  } else if (auto it = find(OldOutsideFnsList.begin(), OldOutsideFnsList.end(), F);
	     it != OldOutsideFnsList.end()) {
    if (SyncWrappers.find(F) == SyncWrappers.end()) {
      // if it is a outside function and not monochromatic then it is only placed in UNTRUSTED
      RewriteAttrs[F].add_function(UNTRUSTED, OutsideFns[F]);
      // the function of this case need to be changed in callinst
      return true;
    }
  }

  // add llvm.ptr.annotation to all enclave
  if (F->getName().str().find("llvm.ptr.annotation") != std::string::npos) {
    FunctionColors[F] = "";
  }

  // Rewrite only Newly created function refering to CallInst
  if (RewriteAttrs.find(F) == RewriteAttrs.end()) {
    return false;
  }

  // Create a new funtion for each color accessed by the funtion
  for (auto color : RewriteAttrs[F].Colors) {
    // In case if the function associated to color is already created
    if (RewriteAttrs[F].get_function(color) != nullptr) {
      continue;
    }

    ValueToValueMapTy VMap;
    Function* new_F = CloneFunction_col(color, F, VMap);
    // new_F is added to FunctionColors during the CloneFunction
    // FunctionColors[new_F] = color;
  }

  return true;
}

//---------------------------------
// Sgxpass::create_outside_fn
//---------------------------------

// Create new function if outside functions' functiontype is different from required type
void Sgxpass::create_outside_fns(Module &M) {
  for (Function &F : M) {
    if (F.getInstructionCount() == 0) {
      if (auto it = find(OutsideFnsList.begin(), OutsideFnsList.end(), &F);
	  it != OutsideFnsList.end()) {
	continue;
      }
      if (is_memory_function(F.getName().str())) {
	continue;
      }
      if (F.getFunctionType() != get_changed_type(F.getFunctionType())) {
	Funcs[&F].side_effect = true;
	get_new_outside_fn(&F);
      } else if (SyncWrappers.find(&F) == SyncWrappers.end()) {
	// SyncWrapper functions already has its color defined
	// By default place all ouside defined function as UNTRUSTED
	// (could be changed in future; ex: printf placed in all color)

	if (auto it = find(MonochromaticFuncs.begin(), MonochromaticFuncs.end(), &F);
	    it == MonochromaticFuncs.end()) {
	  if ((F.getName().str().find("llvm.dbg.") != 0) &&
	      (F.getName().str().find(".annotation") == std::string::npos)) {
	    // create fattr and rewriteattr to place the function at untrusted part
	    FunctionAttributes fattr = FunctionAttributes(&F, AnnotationNum);
	    Funcs.emplace(std::make_pair(&F, fattr));
	    Funcs[&F].side_effect = true;
	    std::vector<std::string> vect(F.arg_size(), UNTRUSTED);
	    CallFns[&F].add_function(vect, &F);
	    RewriteAttr rew = RewriteAttr(AnnotationNum, Annotations, vect);
	    rew.Colors.emplace(UNTRUSTED);
	    rew.master = UNTRUSTED;
	    RewriteAttrs[&F] = rew;
	    RewriteAttrs[&F].add_function(UNTRUSTED, &F);

	    FunctionColors[&F] = UNTRUSTED;
	  }
	}
      }
    }
  }
}

//---------------------------------
// Sgxpass::detect_outside_fn_call
//---------------------------------

// add UNTRUSTED to the functions which calls outside functions
void Sgxpass::detect_outside_fn_call(Module &M) {

  for (Function &F : M) {
    for (BasicBlock &B : F) {
      for (Instruction &I : B) {
	CallInst *CI = dyn_cast<CallInst>(&I);
	if (CI != nullptr && CI->getCalledFunction()) {
	  Function *CF = CI->getCalledFunction();
	  // if CF instruction count = 0
	  // if CF is not a memory function
	  // if CF is not monochromatic
	  if ((CF->getInstructionCount() == 0) &&
	      !is_memory_function(CF->getName().str()) &&
	      (CF->getName().str().find("llvm.dbg.") != 0) &&
	      (CF->getName().str().find(".annotation") == std::string::npos)) {
	    if (auto it = find(MonochromaticFuncs.begin(), MonochromaticFuncs.end(), CF);
		it == MonochromaticFuncs.end()) {
	      RewriteAttrs[&F].Colors.emplace(UNTRUSTED);
	    }
	  }
	}
      }
    }
  }
}

//---------------------------------
// Sgxpass::create_colored_file
//---------------------------------

//create different colored file .bc
void Sgxpass::create_colored_file(Module &M) {
  struct_rewrite(M);

  callinst_rewrite(M);

  // create the outside functions before function level changes
  create_outside_fns(M);
  detect_outside_fn_call(M);

  create_function_vect(M);
  side_effect_stabilised = false;
  while(!side_effect_stabilised) {
    side_effect_stabilised = true;
    update_side_effect(M);
  }

  detect_malloc(M);

  //errs() << "After\n\n\n\n\n" << M << "\n\n";
  Type *global_type = create_globals(M);
  //change_self_color(&M, UNTRUSTED);
  create_self(&M);
  create_sgxlr_functions(M);
  //AnnotationList.emplace(UNTRUSTED);

  // this is to make sure that the function color is correctly defined for monochromatic functions
  for (auto F : MonochromaticFuncs) {
    if (OutsideFns.find(F) == OutsideFns.end()) {
      FunctionColors[F] = "";
      RewriteAttrs[F].add_function(UNTRUSTED, OutsideFns[F]);
    }
  }

  // Start rewriting all the library functions
  for (Function* Fct : LibFuncs) {
    update_colors(Fct);
  }

  // change global variable global_val and globals
  change_globals(M);

  // change type of colored global variables
  change_colored_globals(M);

  // We mutate the function type at this stage to avoid errors while cloning the module
  // We mutate only if the type has changed
  for (auto &p : OldLibFuncs) {
    if (p.first->getType() != get_changed_type(p.first->getType())) {
      p.first->mutateType(get_changed_type(p.first->getType()));
    }
  }

  for (auto str : Annotations) {
    auto new_module = CloneModule(M, str);

    change_self_color(new_module, str);
    if (str.compare(UNTRUSTED) != 0) {
      create_extern_globals(new_module, global_type);
      //change_self_color(new_module, str);
    }

    if (str.compare(UNTRUSTED) == 0) {
      create_init_terminate(new_module);
      create_new_library_functions(new_module);
    }

    std::string new_module_str;
    raw_string_ostream OS(new_module_str);
    OS << *new_module;
    OS.flush();
    std::ofstream outfile (str+".ll");
    outfile << new_module_str;
    outfile << "\n";
    outfile.close();
  }

  // Add all functions to the list to be deleted
  for (Function &F : M) {
    if (F.user_empty()) {
      ToBeDeletedFn.insert(&F);
    }
  }

  // Delete all newly generated instructions related to BranchInst and ReturnInst
  for (auto I : ToBeDeleted) {
    I->eraseFromParent();
  }

  // Delete all the functions of the module (the base module) before quitting the modulepass
  for (auto I : ToBeDeletedFn) {
    I->eraseFromParent();
  }
}
