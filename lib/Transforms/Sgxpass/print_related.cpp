#include "print_related.hpp"

Color getColor(std::string input) {
  if (input.compare("blue") == 0) return Color::BLUE;
  if (input.compare("cyan") == 0) return Color::CYAN;
  if (input.compare("green") == 0) return Color::GREEN;
  if (input.compare("UNTRUSTED") == 0) return Color::MAGENTA;
  //if (input.compare("red") == 0) return Color::RED;
  if (input.compare("yellow") == 0) return Color::YELLOW;
  return Color::UNKNOWN;
}


const char * getColorForTerm(Color color) {
  switch (color) {
  case Color::ERROR: return "\033[1;31m";
  case Color::BLUE: return "\033[1;34m";
  case Color::CYAN: return "\033[1;36m";
  case Color::GREEN: return "\033[1;32m";
  case Color::MAGENTA: return "\033[1;35m";
  case Color::YELLOW: return "\033[1;33m";
  default: break;
  }
  return "";
}

//---------------------------------------
// Sgxpass::get_file_name
//---------------------------------------

//return line number of istruction if exists else 0
std::string get_file_name(const Instruction *I){
  if (auto N = I->getMetadata(0)) { // this if is never executed
    auto Loc=dyn_cast<DILocation>(N);
    std::string l = Loc->getFilename();
    return l;
  }
  else{
    return "";
  }
}

//---------------------------------------
// Sgxpass::get_line_number
//---------------------------------------

//return line number of istruction if exists else 0
unsigned get_line_number(const Instruction *I){
  if (auto N = I->getMetadata(0)) { // this if is never executed
    auto Loc=dyn_cast<DILocation>(N);
    auto l = Loc->getLine();
    return l;
  }
  else{
    return 0;
  }
}

//---------------------------
// print_with_color_line
//---------------------------
	
void print_with_color_line(const InstructionsAnnotation& Insts) {
  for (auto &p : Insts){
    if (p.second.compare("ERROR") == 0) {     
      errs() << getColorForTerm(Color::ERROR) << " Error on line number " << get_line_number(p.first) << " of file " << get_file_name(p.first) << reset << "\n";
    } else {
      Color color = getColor(p.second);
      if (color == Color::UNKNOWN)
	errs() << get_line_number(p.first) << " => " << p.second << "\n";
      else
	errs() << getColorForTerm(color) << get_line_number(p.first) << reset << "\n";
    }
  }
}

//---------------------------------------
// Sgxpass::print_error_line
//---------------------------------------

//print line number of instruction with error
void print_error_line(const std::map<const Instruction*, std::set<LocalError>> ErrorInsts){
  errs() << "Error Details\n\n";
  bool test_error=false;
  for (auto &p : ErrorInsts){
    test_error=true;
    if(unsigned l=get_line_number(p.first)){
      errs() << getColorForTerm(Color::ERROR) <<
	"Errors occured on line number " << l << " of file " << get_file_name(p.first) << " :" <<reset << " \n";
      for (const LocalError& error : p.second){
	errs() << "    " <<error.str() << "\n";
      }
      errs() << "\n";
    }
  }
  if(!test_error){
    errs() << getColorForTerm(Color::GREEN) << " There is no error " << reset  << "\n";
  }
  
}
