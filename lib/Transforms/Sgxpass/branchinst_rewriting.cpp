#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif
#include "Sgxpass.hpp"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Argument.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/InstIterator.h"

#include "llvm/IR/Instructions.h"
#include "llvm/IR/Operator.h"

#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Verifier.h"

//---------------------------------
// Sgxpass::treat_while_case
//---------------------------------

// Get adequate child node
// case if then; case while
void Sgxpass::treat_while_case(Function* F, const BranchInst* root, const BasicBlock* cur,
			       std::vector<const BasicBlock*>* res,
			       std::set<const BasicBlock*>* treated) {
  DominatorTree DT = DominatorTree (*F);
  const BranchInst *BI = dyn_cast<BranchInst>(cur->getTerminator());
  if (BI != nullptr) {
    for (unsigned i=0; i<BI->getNumSuccessors(); ++i) {
      const BasicBlock* next = BI->getSuccessor(i);
      if (treated->find(next) == treated->end()) {
	treated->insert(next);
	if (DT.dominates(root, next)) {
	  treat_while_case(F, root, next, res, treated);
	} else {
	  res->push_back(next);
	}
      }
    }
  } else {
    const BasicBlock* next = cur->getUniqueSuccessor();
    if ((next != nullptr) &&
	(treated->find(next) == treated->end())) {
      treated->insert(next);
      if (DT.dominates(root, next)) {
	treat_while_case(F, root, next, res, treated);
      } else {
	res->push_back(next);
      }
    }
  }
  return;
}

//---------------------------------
// Sgxpass::branchSuccessor
//---------------------------------

// Get adequate child node
const BasicBlock* Sgxpass::branchSuccessor(const BranchInst* BI, Function* F) {
  DominatorTree DT = DominatorTree (*F);
  // Get DominatorTree node corresponding BasicBlock of BI
  const BasicBlock *root = BI->getParent();
  auto *DTN = DT.getNode (root);
  const auto &Children = DTN->getChildren();
  for (const auto N : Children) {
    const BasicBlock* CB = N->getBlock();
    //case if then else
    if (CB->getUniquePredecessor() != root) {
      return CB;
    }
  }
  std::vector<const BasicBlock*> res;
  std::set<const BasicBlock*> treated;
  treated.insert(root);
  // case if then; case while
  treat_while_case(F, BI, root, &res, &treated);
  if (!res.empty()) {
    return res[0];
  }
  return nullptr;
}
