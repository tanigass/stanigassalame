#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif
#include "LocalError.hpp"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "print_related.hpp"

using namespace llvm;

//LocalError errorPropagation();
LocalError errorAnnotation(const Instruction * I, std::string color1, std::string color2);

LocalError errorArgColor(unsigned int i, const CallInst * CI, std::string color1, std::string color2);
LocalError errorFunction(std::string name);
LocalError errorFunctionBB(std::string name, std::string color1, std::string color2);
LocalError errorArgCombination(unsigned int i, unsigned int j, std::string name, std::string color1, std::string color2);
LocalError errorArgAnnotation(unsigned int i, std::string name);
LocalError errorRet(std::string name);
LocalError errorStoreInst(std::string source_color);
LocalError errorCallInstStore(unsigned int i,unsigned int j, std::string name, std::string source_color);
LocalError errorCallInstStore(unsigned int j, std::string name, std::string source_color);
LocalError errorCallInstStore(const Instruction* I);
