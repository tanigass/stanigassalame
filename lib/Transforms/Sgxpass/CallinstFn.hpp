#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif

#include "llvm/IR/Function.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
#include <map>

using namespace llvm;

class CallinstFn final {
public:
  CallinstFn() {
  };

  Function* get_function(std::vector<std::string> vect) {
    if (fn_table.find(vect) != fn_table.end()) {
      return fn_table[vect];
    }
    return nullptr;
  };

  void add_function(std::vector<std::string> vect, Function* F) {
    fn_table[vect] = F;
  };

public:
  std::map<std::vector<std::string>,Function*> fn_table;
};


class RewriteAttr final {
public:
  RewriteAttr() {
  };

  RewriteAttr(int AnnotationNum, const std::vector<std::string> vect,
	      std::vector<std::string> c_vect)
    : master_table(AnnotationNum, 0)
  {
    Annotations = vect;
    col_vect = c_vect;
  };

  void set_master() {
    int maxIndex = std::max_element(master_table.begin(),master_table.end()) - master_table.begin();
    if (master_table.size() == 0) {
      master = "UNTRUSTED";
      return;
    }
    int maxElement = *std::max_element(master_table.begin(), master_table.end());
    if (maxElement == 0) {
      master = "UNTRUSTED";
    } else {
      master = Annotations[maxIndex];
    }
  }

  void set_colors() {
    for (unsigned j=0; j<master_table.size(); ++j) {
      if (master_table[j]>0) {
	Colors.emplace(Annotations[j]);
      }
    }
    Colors.emplace(master);
  }

  Function* get_function(std::string color) {
    if (fn_table.find(color) != fn_table.end()) {
      return fn_table[color];
    }
    return nullptr;
  };

  void add_function(std::string color, Function* F) {
    fn_table[color] = F;
  };

  Function* get_sync_wrapper(const Function* F) {
    if (sync_wrapper.find(F) != sync_wrapper.end()) {
      return sync_wrapper[F];
    }
    return nullptr;
  }

  const Function* get_rev_sync_wrapper(const Function* F) {
    if (rev_sync_wrapper.find(F) != rev_sync_wrapper.end()) {
      return rev_sync_wrapper[F];
    }
    return nullptr;
  }

public:
  // last element of master_table (i.e master_table[AnnotationNum]) is for no color ""
  std::vector<int> master_table;
  std::string master;
  std::vector<std::string> Annotations;
  std::vector<std::string> col_vect;
  //colors used in the function
  std::set<std::string> Colors;
  //function for each enclave color
  std::map<std::string, Function*> fn_table;
  // will be used to create Global Variable colors
  std::set<std::string> spawn_colors;
  // a map with caller function and a equivalent sync_wrapped called fonction
  // caller function : main
  // called function & currents attributes associated with : f.2
  // master : green
  // sync_wrapped called function : sync_main_with_f.2_green
  std::map<const Function*, Function*> sync_wrapper;
  // reverse map for sync_wrapper
  // will be used to create Global Variable colors
  std::map<const Function*, const Function*> rev_sync_wrapper;
};

// this class is to collect information needed to send arguments to spawned function
// through sgxlr_continue
class SpawnArg final {
public:
  SpawnArg() {
  };

  SpawnArg(Function *_F, std::string _color, Module *_M, const CallInst *_CI, Instruction *_spawn, ValueToValueMapTy *_Vmap)
  {
    F = _F;
    color = _color;
    M = _M;
    CI = _CI;
    spawn = _spawn;
    Vmap = _Vmap;
  };

public:
  Function *F;
  std::string color;
  Module *M;
  const CallInst *CI;
  Instruction *spawn;
  ValueToValueMapTy *Vmap;
};

// this class is to collect information needed to rewrite ExternalSafe functions
class ExtSafeAttr final {
public:
  ExtSafeAttr() {
  };

  ExtSafeAttr(Function *_F)
  {
    F = _F;
  };

  void add_color(std::string col) {
    if (col.compare("") != 0) {
      call_colors.emplace(col);
    }
  }

  void set_function(std::string col, Function* s_f) {
    fmap[col] = s_f;
  }

  Function* get_function(std::string col) {
    if (fmap.find(col) != fmap.end()) {
      return fmap[col];
    }
    return nullptr;
  }

public:
  Function *F;
  std::set<std::string> call_colors;
  std::map<std::string, Function*> fmap;
};
