#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif

#include <map>
#include <set>
#include "llvm/IR/Instruction.h"
#include "llvm/IR/DebugInfoMetadata.h"
#include "LocalError.hpp"
#include "llvm/IR/Value.h"

using namespace llvm;
template <typename T> using ColorsMap = std::map<const T*, std::string>;
using ValuesAnnotation = ColorsMap<Value>;// std::map<Value*, std::string>;
using InstructionsAnnotation = ColorsMap<Instruction>;

constexpr auto reset = "\033[0m";
// to add color to errs()
enum class Color { ERROR, BLUE, CYAN, GREEN, MAGENTA, YELLOW, UNKNOWN };

Color getColor(std::string input);
const char * getColorForTerm(Color color);
//return line number of istruction if exists else 0
unsigned get_line_number(const Instruction *I);
std::string get_file_name(const Instruction *I);
//template <typename T> void print_with_color(const ColorsMap<T>& map);

//---------------------------
// print_with_color
//---------------------------
	
template <typename T> void print_with_color(const ColorsMap<T>& map) {
  for (auto &p : map){
    if (p.second.compare("ERROR") == 0) {
      errs() << getColorForTerm(Color::ERROR) << *p.first << reset << "\n";
    } else {
      Color color = getColor(p.second);
      if (color == Color::UNKNOWN)
	errs() << *p.first << " => " << p.second << "\n";
      else
	errs() << getColorForTerm(color) << *p.first << reset << "\n";
    }
  }
}


void print_with_color_line(const InstructionsAnnotation& Insts);
//print line number of instruction with error
void print_error_line(const std::map<const Instruction*, std::set<LocalError>> ErrorInsts);
