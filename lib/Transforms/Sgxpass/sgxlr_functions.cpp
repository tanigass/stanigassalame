#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif
#include "Sgxpass.hpp"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Argument.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/InstIterator.h"

#include "llvm/IR/Instructions.h"
#include "llvm/IR/Operator.h"

#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Verifier.h"


//---------------------------------
// Sgxpass::sgxlr_continue
//---------------------------------
// Create all arguments to call sgxlr_cont and place them before I
// Newly created instructions are all added to VMap to avoid issues during remapping
Instruction* Sgxpass::sgxlr_continue(const Function *F, std::string color,
				     Module *M, Value *arg, Instruction *I,
				     ValueToValueMapTy &VMap){
  Function* _sgxlr_cont = M->getFunction("_sgxlr_cont");
  LLVMContext &context = M->getContext();

  IRBuilder<> builder(context);
  builder.SetInsertPoint(I);

  //Arg0
  auto pos = AnnotationList.find(color);
  int position = std::distance(AnnotationList.begin(), pos);

  //Arg1
  auto fpos = VectFuncs.find(F);
  int fnum = std::distance(VectFuncs.begin(), fpos);

  //Arg2
  //%26 = load i64, i64* getelementptr inbounds (%self_type, %self_type* @self, i32 0, i32 2, i32 2)
  SmallVector<Value*, 3> gi_args;
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 0));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 2));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), fnum));
  StructType *selftype = M->getTypeByName("self_type");

  Value *GI = builder.CreateInBoundsGEP(selftype,
					M->getGlobalVariable("self"), gi_args);
  VMap[GI] = GI;
  LoadInst *LI = builder.CreateLoad(llvm::Type::getInt64Ty(context), GI);
  VMap[LI] = LI;

  //Arg3
  //%27 = sext i32 %3 to i64
  Type *sgxlr_value_ty = M->getTypeByName("union.sgxlr_value");
  if (sgxlr_value_ty == nullptr) {
    sgxlr_value_ty = StructType::create(context, Type::getInt64Ty(context),
					"union.sgxlr_value");
  }
  Value *BI;
  if (arg != nullptr) {
    BI = builder.CreateZExt(arg->stripPointerCasts(), llvm::Type::getInt64Ty(context));
  } else {
    BI = builder.getInt64(0);
  }
  VMap[BI] = BI;
  SmallVector<Value*, 4> args;
  args.push_back(llvm::ConstantInt::get(llvm::Type::getInt64Ty(context), position));
  args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), fnum));
  args.push_back(LI);
  args.push_back(BI);
  //call void @_sgxlr_cont(i64 3, i32 2, i64 %26, i64 %27)
  CallInst* CI = builder.CreateCall(_sgxlr_cont, args);
  VMap[CI] = CI;
  return CI;
}

//---------------------------------
// Sgxpass::sgxlr_continue_after
//---------------------------------
// Create all arguments to call sgxlr_cont and place them after I
// Newly created instructions are all added to VMap to avoid issues during remapping
// Send 0 as value through sgxlr_cont if arg is nullptr otherwise send arg
Instruction* Sgxpass::sgxlr_continue_after(const Function *F, std::string color,
					   Module *M, Value *arg, Instruction *I,
					   ValueToValueMapTy &VMap){
  Function* _sgxlr_cont = M->getFunction("_sgxlr_cont");
  LLVMContext &context = M->getContext();

  //Arg0
  auto pos = AnnotationList.find(color);
  int position = std::distance(AnnotationList.begin(), pos);

  //Arg1
  auto fpos = VectFuncs.find(F);
  int fnum = std::distance(VectFuncs.begin(), fpos);

  //Arg2
  //%26 = load i64, i64* getelementptr inbounds (%self_type, %self_type* @self, i32 0, i32 2, i32 2)
  SmallVector<Value*, 3> gi_args;
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 0));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 2));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), fnum));
  StructType *selftype = M->getTypeByName("self_type");

  GetElementPtrInst *GI = GetElementPtrInst::CreateInBounds(selftype,
							    M->getGlobalVariable("self"),
							    gi_args);
  GI->insertAfter(I);
  VMap[GI] = GI;
  LoadInst *LI = new LoadInst(llvm::Type::getInt64Ty(context), GI);
  LI->insertAfter(GI);
  VMap[LI] = LI;

  //Arg3
  //%27 = sext i32 %3 to i64
  Type *sgxlr_value_ty = M->getTypeByName("union.sgxlr_value");
  if (sgxlr_value_ty == nullptr) {
    sgxlr_value_ty = StructType::create(context, Type::getInt64Ty(context),
					"union.sgxlr_value");
  }
  Value *BI;
  bool BI_is_inst = false;
  if ((arg != nullptr) && !(arg->getType()->isVoidTy())) {
    if (arg->getType()->isIntegerTy(64)) {
      BI = arg;
    } else if (arg->getType()->isPointerTy()) {
      BI = new PtrToIntInst(arg, llvm::Type::getInt64Ty(context));
      BI_is_inst = true;
      dyn_cast<Instruction>(BI)->insertAfter(LI);
    } else {
      BI = new ZExtInst(arg->stripPointerCasts(), llvm::Type::getInt64Ty(context));
      BI_is_inst = true;
      dyn_cast<Instruction>(BI)->insertAfter(LI);
    }
  } else {
    BI = llvm::ConstantInt::get(llvm::Type::getInt64Ty(context), 0);
  }
  VMap[BI] = BI;

  SmallVector<Value*, 4> args;
  args.push_back(llvm::ConstantInt::get(llvm::Type::getInt64Ty(context), position));
  args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), fnum));
  args.push_back(LI);
  args.push_back(BI);
  //call void @_sgxlr_cont(i64 3, i32 2, i64 %26, i64 0)
  CallInst* CI = CallInst::Create(_sgxlr_cont, args);
  if (BI_is_inst) {
    CI->insertAfter(dyn_cast<Instruction>(BI));
  } else {
    CI->insertAfter(LI);
  }

  VMap[CI] = CI;
  return CI;
}

//---------------------------------
// Sgxpass::sgxlr_wait
//---------------------------------

// Create all arguments to call sgxlr_wait and return the call of sgxlr_wait
// %11 = getelementptr inbounds %self_type, %self_type* @self, i32 0, i32 2, i32 2
// %12 = load i64, i64* %11
// %13 = call i64 @_sgxlr_wait(i32 2, i64 %12)
// Newly created instructions are all added to VMap to avoid issues during remapping

Instruction* Sgxpass::sgxlr_wait(const Function *F, BasicBlock *BB, Module *M,
				 ValueToValueMapTy &VMap) {
  LLVMContext &context = M->getContext();
  auto fpos = VectFuncs.find(F);
  int fnum = std::distance(VectFuncs.begin(), fpos);

  SmallVector<Value*, 3> gi_args;
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 0));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 2));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), fnum));
  StructType *selftype = M->getTypeByName("self_type");
  GetElementPtrInst *GI = GetElementPtrInst::CreateInBounds(selftype,
							    M->getGlobalVariable("self"),
							    gi_args);
  BB->getInstList().push_back(GI);
  VMap[GI] = GI;
  LoadInst *LI = new LoadInst(Type::getInt64Ty(M->getContext()), GI);
  BB->getInstList().push_back(LI);
  VMap[LI] = LI;
  Function* _sgxlr_wait = M->getFunction("_sgxlr_wait");
  SmallVector<Value*, 2> c_args;
  c_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), fnum));
  c_args.push_back(LI);
  CallInst* CI = CallInst::Create(_sgxlr_wait, c_args);
  return CI;
}

//---------------------------------
// Sgxpass::sgxlr_wait_before
//---------------------------------

// Create all arguments to call sgxlr_wait and return the call of sgxlr_wait
// %11 = getelementptr inbounds %self_type, %self_type* @self, i32 0, i32 2, i32 2
// %12 = load i64, i64* %11
// %13 = call i64 @_sgxlr_wait(i32 2, i64 %12)
// Newly created instructions are all added to VMap to avoid issues during remapping

Instruction* Sgxpass::sgxlr_wait_before(const Function *F, Instruction *I, Module *M,
					ValueToValueMapTy &VMap) {
  LLVMContext &context = M->getContext();

  IRBuilder<> builder(context);
  builder.SetInsertPoint(I);

  auto fpos = VectFuncs.find(F);
  int fnum = std::distance(VectFuncs.begin(), fpos);

  SmallVector<Value*, 3> gi_args;
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 0));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 2));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), fnum));
  StructType *selftype = M->getTypeByName("self_type");
  Value *GI = builder.CreateInBoundsGEP(selftype,
					M->getGlobalVariable("self"), gi_args);
  VMap[GI] = GI;

  LoadInst *LI = builder.CreateLoad(llvm::Type::getInt64Ty(context), GI);
  VMap[LI] = LI;

  Function* _sgxlr_wait = M->getFunction("_sgxlr_wait");
  SmallVector<Value*, 2> c_args;
  c_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), fnum));
  c_args.push_back(LI);
  // %1 = call i64 @_sgxlr_wait(i32 2, i64 %0)
  CallInst* CI = builder.CreateCall(_sgxlr_wait, c_args);
  VMap[CI] = CI;
  return CI;
}


//---------------------------------
// Sgxpass::get_spawned_fn
//---------------------------------

// return the function to be completed in global variable color
// Create a wrapper function in case of non colored arguments
// The wrapper function collects non colored arguments sent by caller function
// the real function is called with non colored arguments recieved using sgxlr_wait
Function* Sgxpass::get_spawned_fn(std::unique_ptr<llvm::Module>& M, const Function* F, std::string str) {

  // contains information if the function is to be warpped or not
  bool wrap = false;
  std::vector<std::string> col_vect = RewriteAttrs[F].col_vect;
  for (std::string col : col_vect) {
    if (col.compare("") == 0) {
      wrap = true;
      break;
    }
  }
  if (!wrap) {
    // If the function is a Outside function it will be cloned using its native name
    // So no suffix color will be added
    if (OutsideFns.find(F) != OutsideFns.end()) {
      return OutsideFns[F];
    }
    return M->getFunction(F->getName().str()+"_"+str);
  }

  Function *new_Fn = M->getFunction("wrapped_"+F->getName().str()+"_"+str);

  if (new_Fn != nullptr) {
    return new_Fn;
  }

  LLVMContext &context = M->getContext();
  IRBuilder<> builder(context);

  // the real function to be called
  Function *CF = M->getFunction(F->getName().str()+"_"+str);
  // functype : returnType_of_CF ()
  FunctionType *functype = FunctionType::get(CF->getReturnType(), false);
  // Create the wrapper function
  Function *new_F = Function::Create(functype, Function::ExternalLinkage,
				     "wrapped_"+F->getName().str()+"_"+str, M.get());

  // Define the entry block and fill it with required code
  auto *entry = BasicBlock::Create(context, "entry", new_F);
  builder.SetInsertPoint(entry);
  std::vector<Value*> args;
  // Collect all arguments to call the real function
  for (unsigned i=0; i<col_vect.size(); ++i) {
    if (col_vect[i].compare("") == 0) {
      auto fpos = VectFuncs.find(F);
      int fnum = std::distance(VectFuncs.begin(), fpos);

      SmallVector<Value*, 3> gi_args;
      gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 0));
      gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 2));
      gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), fnum));
      StructType *selftype = M->getTypeByName("self_type");
      Value *GI = builder.CreateInBoundsGEP(selftype,
					    M->getGlobalVariable("self"), gi_args);
      // %0 = load i64, i64* getelementptr inbounds (%self_type, %self_type* @self, i32 0, i32 2, i32 3)
      LoadInst *LI = builder.CreateLoad(llvm::Type::getInt64Ty(context), GI);
      Function* _sgxlr_wait = M->getFunction("_sgxlr_wait");
      SmallVector<Value*, 2> c_args;
      c_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), fnum));
      c_args.push_back(LI);
      // %1 = call i64 @_sgxlr_wait(i32 2, i64 %0)
      CallInst* CI = builder.CreateCall(_sgxlr_wait, c_args);
      // %2 = trunc i64 %1 to i32 or %2 = inttoptr i64 %1 to i8*
      Value *BI;
      if (CF->getFunctionType()->getParamType(i)->isPointerTy()) {
	// %2 = inttoptr i64 %1 to i8*
	BI = builder.CreateIntToPtr(CI, CF->getFunctionType()->getParamType(i));
      } else {
	// %2 = trunc i64 %1 to i32
	BI = builder.CreateTrunc(CI, CF->getFunctionType()->getParamType(i));
      }
      args.push_back(BI);

    } else {
      args.push_back(Constant::getNullValue(CF->getFunctionType()->getParamType(i)));
    }
  }
  // %6 = call i32 @g.3_red(i32 %2, i32 %5)
  auto *result = builder.CreateCall(CF, args);
  // ret void or result
  if (F->getReturnType()->isVoidTy()) {
    builder.CreateRetVoid();
  } else {
    builder.CreateRet(result);
  }
  // Verify at the end
  verifyFunction(*new_F);
  return new_F;
}

//---------------------------------
// Sgxpass::get_sync_fn
//---------------------------------
// Create the function to sync_wrapper function
// To syncronize a side effect function call
// where there is no common color between caller and called functions
// define void @sync_main_with_f.2_green() {
// entry:
//   call void @wrapped_f.2_green()
//   %0 = load i64, i64* getelementptr inbounds (%self_type, %self_type* @self, i32 0, i32 2, i32 1)
//   call void @_sgxlr_cont(i64 3, i32 1, i64 %0, i64 10)
//   ret void
// }
Function* Sgxpass::get_sync_fn(std::unique_ptr<llvm::Module>& M, const Function* F, std::string str) {

  LLVMContext &context = M->getContext();
  IRBuilder<> builder(context);
  Function *new_F = M->getFunction(F->getName().str());
  // the real function to be called
  // either the wrapper function if the function has uncolored arguments
  // or the called function
  Function *CF = get_spawned_fn(M, SyncWrappers[F], str);

  // Define the entry block and fill it with required code
  auto *entry = BasicBlock::Create(context, "entry", new_F);
  builder.SetInsertPoint(entry);

  CallInst *result;
  // If arg size == 0
  // either wrapped function or function with no argument
  if (CF->arg_size() == 0) {
    // call void @wrapped_f.2_green()
    result = builder.CreateCall(CF);
  } else {
    // case where arg size != 0
    // Function with only colored arguments
    std::vector<Value*> args;
    for (unsigned i=0; i<CF->arg_size(); ++i) {
      args.push_back(Constant::getNullValue(CF->getFunctionType()->getParamType(i)));
    }
    result = builder.CreateCall(CF, args);
  }

  const Function* send_F = RewriteAttrs[SyncWrappers[F]].get_rev_sync_wrapper(F);
  std::string color = RewriteAttrs[send_F].master;
  Function* _sgxlr_cont = M->getFunction("_sgxlr_cont");

  //Arg0
  auto pos = AnnotationList.find(color);
  int position = std::distance(AnnotationList.begin(), pos);

  //Arg1
  auto fpos = VectFuncs.find(send_F);
  int fnum = std::distance(VectFuncs.begin(), fpos);

  //Arg2
  //%0 = load i64, i64* getelementptr inbounds (%self_type, %self_type* @self, i32 0, i32 2, i32 1)
  SmallVector<Value*, 3> gi_args;
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 0));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 2));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), fnum));
  StructType *selftype = M->getTypeByName("self_type");

  Value *GI = builder.CreateInBoundsGEP(selftype,
					M->getGlobalVariable("self"), gi_args);

  LoadInst *LI = builder.CreateLoad(llvm::Type::getInt64Ty(context), GI);

  //Arg3
  //%27 = zext i32 %3 to i64
  Type *sgxlr_value_ty = M->getTypeByName("union.sgxlr_value");
  if (sgxlr_value_ty == nullptr) {
    sgxlr_value_ty = StructType::create(context, Type::getInt64Ty(context),
					"union.sgxlr_value");
  }
  Value *BI;
  // We are adding sync wrapper for the case where there is no common color
  // between called and caller function
  // So the return value is color less
  // if the return type is void, 0 is sent
  if (result->getType()->isVoidTy()) {
    BI = llvm::ConstantInt::get(llvm::Type::getInt64Ty(context), 0);
  } else if (result->getType()->isPointerTy()) {
    BI = builder.CreatePtrToInt(result->stripPointerCasts(), llvm::Type::getInt64Ty(context));
  } else {
    BI = builder.CreateZExt(result->stripPointerCasts(), llvm::Type::getInt64Ty(context));
  }
  SmallVector<Value*, 4> args;
  args.push_back(llvm::ConstantInt::get(llvm::Type::getInt64Ty(context), position));
  args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), fnum));
  args.push_back(LI);
  args.push_back(BI);
  //call void @_sgxlr_cont(i64 3, i32 2, i64 %26, i64 %27)
  CallInst* CI = builder.CreateCall(_sgxlr_cont, args);
  // ret void
  builder.CreateRetVoid();
  // Verify at the end
  verifyFunction(*new_F);
  return new_F;
}

//---------------------------------------------
// Sgxpass::create_sgxlr_start_colored_thread
//---------------------------------------------

// declare void @sgxlr_start_colored_thread()
Function* Sgxpass::create_sgxlr_start_colored_thread(std::unique_ptr<llvm::Module>& M) {
  LLVMContext &context = M->getContext();
  IRBuilder<> builder(context);
  auto *funcType = FunctionType::get(builder.getVoidTy(), false);

  Function* sgxlr_start_colored_thread = Function::Create(funcType, Function::ExternalLinkage, "sgxlr_start_colored_thread", *M);
  // Verify
  verifyFunction(*sgxlr_start_colored_thread);
  return sgxlr_start_colored_thread;
}

//------------------------------------------------
// Sgxpass::create_sgxlr_terminate_colored_thread
//------------------------------------------------

// declare void @sgxlr_terminate_colored_thread()
Function* Sgxpass::create_sgxlr_terminate_colored_thread(std::unique_ptr<llvm::Module>& M) {
  LLVMContext &context = M->getContext();
  IRBuilder<> builder(context);
  auto *funcType = FunctionType::get(builder.getVoidTy(), false);

  Function* sgxlr_terminate_colored_thread = Function::Create(funcType, Function::ExternalLinkage, "sgxlr_terminate_colored_thread", *M);
  // Verify
  verifyFunction(*sgxlr_terminate_colored_thread);
  return sgxlr_terminate_colored_thread;
}

//---------------------------------
// Sgxpass::create_sgxlr_terminate
//---------------------------------

// declare void @sgxlr_terminate()
Function* Sgxpass::create_sgxlr_terminate(std::unique_ptr<llvm::Module>& M) {
  LLVMContext &context = M->getContext();
  IRBuilder<> builder(context);
  auto *funcType = FunctionType::get(builder.getVoidTy(), false);

  Function* sgxlr_terminate = Function::Create(funcType, Function::ExternalLinkage, "sgxlr_terminate", *M);
  // Verify
  verifyFunction(*sgxlr_terminate);
  return sgxlr_terminate;
}

//----------------------------------
// Sgxpass::create_sgxlr_initialize
//----------------------------------

// declare void @_sgxlr_initialize(i8*, i64, ...)
Function* Sgxpass::create_sgxlr_initialize(std::unique_ptr<llvm::Module>& M) {
  LLVMContext &context = M->getContext();
  IRBuilder<> builder(context);
  std::vector<Type*> Integers;
  Integers.push_back(builder.getInt8PtrTy());
  Integers.push_back(builder.getInt64Ty());
  auto *funcType = FunctionType::get(builder.getVoidTy(), Integers, true);

  Function* sgxlr_initialize = Function::Create(funcType, Function::ExternalLinkage, "sgxlr_initialize", *M);
  // Verify
  verifyFunction(*sgxlr_initialize);
  return sgxlr_initialize;
}

//---------------------------------
// Sgxpass::create_sgxlr_wait
//---------------------------------

// declare i64 @_sgxlr_wait(i32, i64)
void Sgxpass::create_sgxlr_wait(Module &M) {
  LLVMContext &context = M.getContext();
  IRBuilder<> builder(context);
  std::vector<Type*> Integers;
  Integers.push_back(builder.getInt32Ty());
  Integers.push_back(builder.getInt64Ty());
  auto *funcType = FunctionType::get(builder.getInt64Ty(), Integers, false);

  Function* _sgxlr_wait = Function::Create(funcType, Function::ExternalLinkage, "_sgxlr_wait", M);
  // Verify
  verifyFunction(*_sgxlr_wait);
}

//---------------------------------
// Sgxpass::create_sgxlr_continue
//---------------------------------

// declare void @_sgxlr_cont(i64, i32, i64, i64) #2
void Sgxpass::create_sgxlr_continue(Module &M) {
  LLVMContext &context = M.getContext();
  IRBuilder<> builder(context);
  std::vector<Type*> Integers;
  Integers.push_back(builder.getInt64Ty());
  Integers.push_back(builder.getInt32Ty());
  Integers.push_back(builder.getInt64Ty());
  Integers.push_back(builder.getInt64Ty());
  auto *funcType = FunctionType::get(builder.getVoidTy(), Integers, false);

  Function* _sgxlr_cont = Function::Create(funcType, Function::ExternalLinkage, "_sgxlr_cont", M);
  // Verify
  verifyFunction(*_sgxlr_cont);
}

//---------------------------------
// Sgxpass::create_sgxlr_spawn
//---------------------------------

// declare i64 @_sgxlr_spawn(i32, i64)
void Sgxpass::create_sgxlr_spawn(Module &M) {
  LLVMContext &context = M.getContext();
  IRBuilder<> builder(context);
  std::vector<Type*> Integers;
  Integers.push_back(builder.getInt64Ty());
  Integers.push_back(builder.getInt32Ty());
  auto *funcType = FunctionType::get(builder.getInt64Ty(), Integers, false);

  Function* _sgxlr_spawn = Function::Create(funcType, Function::ExternalLinkage, "_sgxlr_spawn", M);
  // Verify
  verifyFunction(*_sgxlr_spawn);
}

//---------------------------------
// Sgxpass::create_sgxlr_malloc_in
//---------------------------------

//declare void @_sgxlr_malloc_in(i64, i64, i64, i32)
void Sgxpass::create_sgxlr_malloc_in(std::unique_ptr<llvm::Module> &M) {
  LLVMContext &context = M->getContext();
  IRBuilder<> builder(context);
  std::vector<Type*> Integers;
  Integers.push_back(builder.getInt64Ty());
  Integers.push_back(builder.getInt64Ty());
  Integers.push_back(builder.getInt64Ty());
  Integers.push_back(builder.getInt32Ty());
  auto *funcType = FunctionType::get(builder.getVoidTy(), Integers, false);

  Function* _sgxlr_malloc_in = Function::Create(funcType, Function::ExternalLinkage, "_sgxlr_malloc_in", M.get());
  // Verify
  verifyFunction(*_sgxlr_malloc_in);
}

//---------------------------------
// Sgxpass::create_sgxlr_malloc_out
//---------------------------------

//declare i8* @_sgxlr_malloc_out(i64, i64, i64, i32)
void Sgxpass::create_sgxlr_malloc_out(std::unique_ptr<llvm::Module> &M) {
  LLVMContext &context = M->getContext();
  IRBuilder<> builder(context);
  std::vector<Type*> Integers;
  Integers.push_back(builder.getInt64Ty());
  Integers.push_back(builder.getInt64Ty());
  Integers.push_back(builder.getInt64Ty());
  Integers.push_back(builder.getInt32Ty());
  auto *funcType = FunctionType::get(builder.getInt8PtrTy(), Integers, false);

  Function* _sgxlr_malloc_out = Function::Create(funcType, Function::ExternalLinkage, "_sgxlr_malloc_out", M.get());
  // Verify
  verifyFunction(*_sgxlr_malloc_out);
}

//---------------------------------
// Sgxpass::create_sgxlr_malloc_out
//---------------------------------

//declare i8* @_sgxlr_malloc_out(i64, i64, i64, i32)
void Sgxpass::create_sgxlr_malloc_out(Module &M) {
  LLVMContext &context = M.getContext();
  IRBuilder<> builder(context);
  std::vector<Type*> Integers;
  Integers.push_back(builder.getInt64Ty());
  Integers.push_back(builder.getInt64Ty());
  Integers.push_back(builder.getInt64Ty());
  Integers.push_back(builder.getInt32Ty());
  auto *funcType = FunctionType::get(builder.getInt8PtrTy(), Integers, false);

  Function* _sgxlr_malloc_out = Function::Create(funcType, Function::ExternalLinkage, "_sgxlr_malloc_out", M);
  // Verify
  verifyFunction(*_sgxlr_malloc_out);
}

//---------------------------------
// Sgxpass::create_sgxlr_functions
//---------------------------------

// declare i64 @_sgxlr_wait(i32, i64)
// declare void @_sgxlr_cont(i64, i32, i64, i64) #2
void Sgxpass::create_sgxlr_functions(Module &M) {
  create_sgxlr_wait(M);
  create_sgxlr_continue(M);
  create_sgxlr_spawn(M);
  create_sgxlr_malloc_out(M);
}

//---------------------------------
// Sgxpass::create_selftype
//---------------------------------

// Create the StructType associated to self
// %struct.thread = type opaque
// %struct.message = type opaque
// *selftype
// { %struct.thread*, %struct.message*, [2 x i64] }

StructType* Sgxpass::create_selftype(std::unique_ptr<llvm::Module>& new_module) {
  LLVMContext &context = new_module->getContext();

  // Create new struct thread and message if it doesn't exist
  StructType *threadtype = new_module->getTypeByName("struct.thread");
  StructType *messagetype = new_module->getTypeByName("struct.message");
  if ( threadtype == nullptr || messagetype == nullptr ) {
    threadtype = StructType::create(context, "struct.thread");
    messagetype = StructType::create(context, "struct.message");
  }

  // added functions _sgxlr_malloc_in and _sgxlr_malloc_out to self count
  ArrayType *uinttype = ArrayType::get(Type::getInt64Ty(context), VectFuncs.size()+2);
  std::vector<Type *> elt_tys;
  elt_tys.push_back(threadtype->getPointerTo());
  elt_tys.push_back(messagetype->getPointerTo());
  elt_tys.push_back(uinttype);
  StructType *selftype = new_module->getTypeByName("self_type");
  if (selftype == nullptr) {
    selftype = StructType::create(context, elt_tys, "self_type");
  }
  return selftype;
}

//---------------------------------
// Sgxpass::create_selftype
//---------------------------------

// Create the StructType associated to self
// %struct.thread = type opaque
// %struct.message = type opaque
// *selftype
// { %struct.thread*, %struct.message*, [2 x i64] }

StructType* Sgxpass::create_selftype_(Module* new_module) {
  LLVMContext &context = new_module->getContext();

  // Create new struct thread and message if it doesn't exist
  StructType *threadtype = new_module->getTypeByName("struct.thread");
  StructType *messagetype = new_module->getTypeByName("struct.message");
  if ( threadtype == nullptr || messagetype == nullptr ) {
    threadtype = StructType::create(context, "struct.thread");
    messagetype = StructType::create(context, "struct.message");
  }

  // added functions _sgxlr_malloc_in and _sgxlr_malloc_out to self count
  ArrayType *uinttype = ArrayType::get(Type::getInt64Ty(context), VectFuncs.size()+2);
  std::vector<Type *> elt_tys;
  elt_tys.push_back(threadtype->getPointerTo());
  elt_tys.push_back(messagetype->getPointerTo());
  elt_tys.push_back(uinttype);
  StructType *selftype = new_module->getTypeByName("self_type");
  if (selftype == nullptr) {
    selftype = StructType::create(context, elt_tys, "self_type");
  }
  return selftype;
}

//---------------------------------
// Sgxpass::create_self
//---------------------------------

// Create the GlobalVariable self required by the runtime
// %struct.thread = type opaque
// %struct.message = type opaque
// @self = thread_local global { %struct.thread*, %struct.message*, [2 x i64] } zeroinitializer
void Sgxpass::create_self(Module* new_module) {
  StructType *selftype = create_selftype_(new_module);
  // Create Zeroinitializer constant associate to self
  Constant* cst = Constant::getNullValue(selftype);

  // Create the GlobalVariable self
  GlobalVariable *GV;
  GV = new GlobalVariable(*new_module, selftype, 0,
			  GlobalValue::LinkageTypes::ExternalLinkage,
			  cst, "self", (GlobalVariable*) nullptr,
			  GlobalValue::ThreadLocalMode::GeneralDynamicTLSModel
			  );
}

//---------------------------------
// Sgxpass::change_self_color
//---------------------------------

// Create the GlobalVariable self required by the runtime
// %struct.thread = type opaque
// %struct.message = type opaque
// @self = thread_local global %self_type zeroinitializer
// @str = private constant [6 x i8] c"green\00"
// @color = global %color_type { i8* getelementptr inbounds ([6 x i8], [6 x i8]* @str, i32 0, i32 0), i64 2, [5 x void (...)*] [void (...)* bitcast (void ()* @sync_g.3_with_f.2_green to void (...)*), void (...)* null, void (...)* bitcast (void ()* @wrapped_f.2_green to void (...)*), void (...)* null, void (...)* bitcast (void ()* @sync_main_with_f.2_green to void (...)*)] }

void Sgxpass::change_self_color(std::unique_ptr<llvm::Module>& new_module, std::string str) {

  if (str.compare(UNTRUSTED) != 0) {
    StructType *selftype = create_selftype(new_module);
    // Create Zeroinitializer constant associate to self
    Constant* cst = Constant::getNullValue(selftype);

    // Create the GlobalVariable self
    GlobalVariable *GV;
    GV = new GlobalVariable(*new_module, selftype, 0,
			    GlobalValue::LinkageTypes::ExternalLinkage,
			    cst, "self", (GlobalVariable*) nullptr,
			    GlobalValue::ThreadLocalMode::GeneralDynamicTLSModel
			    );
  }

  LLVMContext &context = new_module->getContext();
  StructType *colortype = create_colortype(new_module);

  StringRef col_str = StringRef(str);
  Constant *color = ConstantDataArray::getString(context, col_str);
  GlobalVariable *GV_col;
  GV_col = new GlobalVariable(*new_module, color->getType(), 1,
			      GlobalValue::LinkageTypes::PrivateLinkage,
			      color, "str", (GlobalVariable*) nullptr);
  std::vector<Constant*> vect;
  auto pos = AnnotationList.find(str);
  int position = std::distance(AnnotationList.begin(), pos);
  SmallVector<Value*, 2> gi_args;
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 0));
  gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 0));

  Constant *cst_col = ConstantExpr::getPointerCast(GV_col, colortype->getElementType((unsigned)0));
  vect.push_back(cst_col);

  vect.push_back(llvm::ConstantInt::get(llvm::Type::getInt64Ty(context), position));

  ArrayType *arrayty = dyn_cast<ArrayType>(colortype->getElementType(2));
  std::vector<Constant *> array_cst;
  for (auto *F : VectFuncs) {
    Function* new_F;
    // Create and add sync_wrapper to the global variable color
    if (SyncWrappers.find(F) != SyncWrappers.end()) {
      if (RewriteAttrs[SyncWrappers[F]].master.compare(str) == 0) {
	new_F = get_sync_fn(new_module, F, str);
	array_cst.push_back(ConstantExpr::getPointerCast(new_F, arrayty->getElementType()));
      } else {
	array_cst.push_back(Constant::getNullValue(arrayty->getElementType()));
      }
      continue;
    }
    if (RewriteAttrs[F].spawn_colors.find(str) != RewriteAttrs[F].spawn_colors.end()) {
      new_F = get_spawned_fn(new_module, F, str);
      array_cst.push_back(ConstantExpr::getPointerCast(new_F, arrayty->getElementType()));
    } else {
      array_cst.push_back(Constant::getNullValue(arrayty->getElementType()));
    }
  }

  // created functions _sgxlr_malloc_in and _sgxlr_malloc_out
  // added them to the global variable "color" of all .ll files
  create_sgxlr_malloc_in(new_module);
  Function* _sgxlr_malloc_in = new_module->getFunction("_sgxlr_malloc_in");
  array_cst.push_back(ConstantExpr::getPointerCast(_sgxlr_malloc_in, arrayty->getElementType()));
  //create_sgxlr_malloc_out(new_module);
  Function* _sgxlr_malloc_out = new_module->getFunction("_sgxlr_malloc_out");
  array_cst.push_back(ConstantExpr::getPointerCast(_sgxlr_malloc_out, arrayty->getElementType()));

  //malloc_num = VectFuncs.size() + 1;
  malloc_num = VectFuncs.size();

  vect.push_back(ConstantArray::get(arrayty, array_cst));

  Constant *cstst = ConstantStruct::get(colortype, vect);

  // Create the GlobalVariable color
  GlobalVariable *GV_color;
  GV_color = new GlobalVariable(*new_module, colortype, 0,
				GlobalValue::LinkageTypes::ExternalLinkage,
				cstst, "color", (GlobalVariable*) nullptr,
				//GlobalValue::ThreadLocalMode::GeneralDynamicTLSModel
				GlobalValue::ThreadLocalMode::NotThreadLocal
				);

  GlobalVariable *g_malloc_num;
  g_malloc_num = new GlobalVariable(*new_module, llvm::Type::getInt32Ty(context), 0,
				    GlobalValue::LinkageTypes::ExternalLinkage,
				    ConstantInt::get(Type::getInt32Ty(context), malloc_num),
				    "malloc_num", (GlobalVariable*) nullptr,
				    GlobalValue::ThreadLocalMode::NotThreadLocal
				    );
}

//---------------------------------
// Sgxpass::create_colortype
//---------------------------------

// Create the StructType associated to color
// %color_type = type { i8*, i64, [6 x void (...)*] }


StructType* Sgxpass::create_colortype(std::unique_ptr<llvm::Module>& new_module) {
  //GlobalVariable *color = new_module->getGlobalVariable("color");
  LLVMContext &context = new_module->getContext();
  IRBuilder<> builder(context);
  // ReturnType for function :: void
  Type *rty = Type::getVoidTy(context);
  // Function type :: void (...)
  FunctionType *fty = FunctionType::get(rty, true);
  // Arraytype for third element of structtype :: [6 x void (...)*]
  // added functions _sgxlr_malloc_in and _sgxlr_malloc_out to color
  ArrayType *arrtype = ArrayType::get(fty->getPointerTo(), VectFuncs.size()+2);
  std::vector<Type *> elt_tys;
  elt_tys.push_back(builder.getInt8Ty()->getPointerTo());
  elt_tys.push_back(builder.getInt64Ty());
  elt_tys.push_back(arrtype);
  StructType *colortype = new_module->getTypeByName("color_type");
  if (colortype == nullptr) {
    colortype = StructType::create(context, elt_tys, "color_type");
  }
  return colortype;
}
