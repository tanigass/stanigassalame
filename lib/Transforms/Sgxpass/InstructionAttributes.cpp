#include "InstructionAttributes.hpp"

// constuctors/destructors
InstructionAttributes::InstructionAttributes()
  : dependancy(0, false)
{
  root = true;
}

InstructionAttributes::InstructionAttributes(const Function* F, int AnnotationNum)
  : dependancy(F->arg_size()+AnnotationNum, false)
{
  root = true;
}

//-------------------------------
// InstructionAttributes::is_equal
//-------------------------------


bool InstructionAttributes::is_equal(InstructionAttributes i_attr){
  // Root is ignored because root doesn't affect the constraints
  if (dependancy.size() != i_attr.dependancy.size()) {
    return false;
  } else {
    for(unsigned i = 0; i<dependancy.size(); i++){
      if(dependancy[i] != i_attr.dependancy[i]){
	return false;
      }
    }
  }
  return true;
}

//-------------------------------
// InstructionAttributes::add
//-------------------------------
  
//return false if new constraints are added to dependancy
bool InstructionAttributes::add(InstructionAttributes i_attr){
  bool result = true;
  for(unsigned i=0; i<dependancy.size(); i++){
    if((dependancy[i] == false) && (i_attr.dependancy[i] == true)) {
      result = false;
    }
    dependancy[i] = dependancy[i] || i_attr.dependancy[i];
    i_attr.dependancy[i] = dependancy[i] || i_attr.dependancy[i];
  }
  //root ignored
  //root = root || i_attr.root;
  return result;
}



/*
  bool add(InstructionAttributes i_attr){
  for(unsigned i=0; i<dependancy.size(); i++){
  if((dependancy[i] == false) && (i_attr.dependancy[i] == true)) {
  return false;
  }
  dependancy[i] = dependancy[i] || i_attr.dependancy[i];
  }
  //root ignored
  //root = root || i_attr.root;
  return true;
  }
*/
