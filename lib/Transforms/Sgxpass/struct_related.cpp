#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif
#include "Sgxpass.hpp"

//-------------------------------------
// Sgxpass::get_base_type
//-------------------------------------

// return the structtype pointed by the pointer (even mutiple *)
// return nullptr if it is not structtype
StructType* Sgxpass::get_base_type(PointerType *pty) {
  Type *ty = pty;
  while (ty->isPointerTy()) {
    ty=dyn_cast<PointerType>(ty)->getElementType();
  }
  return dyn_cast<StructType>(ty);
}

//-------------------------------------
// Sgxpass::initialize_StructColors
//-------------------------------------

// Rename all struct with "old." in prefix
// Create new struct with old name
// Initialize StructAttributes with "" for colors and old structs elements for elt_tys
void Sgxpass::initialize_StructColors(Module &M) {
  auto STs = M.getIdentifiedStructTypes();
  LLVMContext &context = M.getContext();
  for (StructType* ST : STs) {
    std::string name = ST->getName().str();
    std::string new_name = "old." + ST->getName().str();
    ST->setName(new_name);
    StructType* new_ST = StructType::create(context, name);
    StructAttributes attr(ST, new_ST);
    StructColors.emplace(std::make_pair(ST, attr));
    StructMap.emplace(std::make_pair(ST, new_ST));
  }
}

//---------------------------------
// Sgxpass::update_structatt_colors
//---------------------------------

// update the color member of all structattributes of StructColors
void Sgxpass::update_structatt_colors(Module &M){
  // Iterate over all functions of a Module
  for (Function &F : M) {
    // Iterate over all BasicBlocks of a Function
    for (BasicBlock &B : F) {
      // Iterate over all Instructions of a BasicBlock
      for (Instruction &I: B) {
	auto CI = dyn_cast<CallInst>(&I); // auto = CallInst*
	if (CI != nullptr) {
	  // check if the called function is a function pointer
	  if (!(CI->isIndirectCall()) && !(CI->isInlineAsm())){
	    Function *CF=CI->getCalledFunction();
	    // to extract all llvm.ptr.annotation function and treat them
	    //in order to update structattr with appropriate colors
	    if (CF && CF->hasName()) {
	      if (CF->getName().find("ptr.annotation") != std::string::npos) {
		const std::string& color=get_annotation(*CI);
		treat_colored_struct(CI, color);
	      }
	    }
	  }
	}
      }
    }
  }
}

//-------------------------------
// Sgxpass::treat_colored_struct
//-------------------------------

void Sgxpass::treat_colored_struct(const CallInst* CI, std::string color) {
  // CI : %3 = call i8* @llvm.ptr.annotation.p0i8(i8* %2, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), i32 14)
  // BI : %2 = bitcast i32* %a4 to i8*
  // GI :   %a4 = getelementptr inbounds %struct.test_color, %struct.test_color* %1, i32 0, i32 0
  // ST : %struct.test_color = type { i32, i32 }
  // GI->getOperand(2) : i32 0 or i32 2 ...
  // index : 0 or 2 ...
  BitCastInst *BI = dyn_cast<BitCastInst>(CI->getArgOperand(0));
  GetElementPtrInst *GI;
  StructType* ST;
  unsigned int index;
  if (BI == nullptr) {
    GI = dyn_cast<GetElementPtrInst>(CI->getArgOperand(0));
  } else {
    GI = dyn_cast<GetElementPtrInst>(BI->getOperand(0));
  }
  if (GI == nullptr) {
    GEPOperator *GEPO = dyn_cast<GEPOperator>(BI->getOperand(0));
    if (GEPO != nullptr) {
      if (!GEPO->getSourceElementType()->isStructTy()) {
	return;
      } else {
	ST = dyn_cast<StructType>(GEPO->getSourceElementType());
	index = dyn_cast<ConstantInt>(GEPO->getOperand(2))->getSExtValue();
      }
    } else {
      return;
    }
  } else {
    if (!GI->getSourceElementType()->isStructTy()) {
      return;
    } else {
      ST = dyn_cast<StructType>(GI->getSourceElementType());
      index = dyn_cast<ConstantInt>(GI->getOperand(2))->getSExtValue();
    }
  }
  StructColors[ST].colors[index] = color;
}

//---------------------------------
// Sgxpass::update_StructColors
//---------------------------------

// No need to create a new struct type if the struct is never affected by color
void Sgxpass::update_StructColors(){
  for (auto it : StructColors) {
    auto ST = it.second.ST;
    std::set<Type*> treated;
    if (is_colored_type(ST, &treated)) {
      continue;
    } else {
      std::string name = StructColors[ST].new_ST->getName().str();
      std::string new_name = "new." + name;
      StructColors[ST].new_ST->setName(new_name);
      ST->setName(name);
      StructColors[ST].new_ST = ST;
      StructMap[ST] = ST;
    }
  }
}

//---------------------------------
// Sgxpass::is_colored_type
//---------------------------------

// return true if the type is affected by color
// treated is to avoid loop
bool Sgxpass::is_colored_type(Type *src, std::set<Type*>* treated){
  if (src == nullptr) {
    return false;
  }

  if (treated->find(src) == treated->end()) {
    treated->insert(src);
  } else {
    return false;
  }

  int count = 0;
  Type* ty = src;
  while (ty->isPointerTy()) {
    ty=dyn_cast<PointerType>(ty)->getElementType();
    ++count;
  }

  if ((!ty->isStructTy()) && (!ty->isFunctionTy()) && (!ty->isArrayTy()) && (!ty->isVectorTy())) {
    return false;
  }

  if (ty->isFunctionTy()) {
    FunctionType* fty = dyn_cast<FunctionType>(ty);
    for (unsigned i=0; i<fty->getNumParams(); ++i) {
      if (is_colored_type(fty->getParamType(i), treated)) {
	return true;
      }
    }
  } else if (ty->isArrayTy()) {
    if (is_colored_type(ty->getArrayElementType(), treated)) {
      return true;
    }
  } else if (ty->isVectorTy()) {
    if (is_colored_type(ty->getVectorElementType(), treated)) {
      return true;
    }
  } else if (ty->isStructTy()) {
    if (StructColors[dyn_cast<StructType>(ty)].is_colored()) {
      return true;
    }
    auto elts = dyn_cast<StructType>(ty)->elements();
    for (Type *elt : elts) {
      if (is_colored_type(elt, treated)) {
	return true;
      }
    }
  }

  return false;
}

//-------------------------------------
// Sgxpass::update_elt_tys
//-------------------------------------

void Sgxpass::update_elt_tys(Module &M) {
  for (auto it : StructColors) {
    StructType* ST = it.first;
    if (!ST) {
      continue;
    }
    if (StructColors[ST].new_ST == ST) {
      continue;
    }
    // if ST type is opaque; let it be opaque without changing it
    if (ST->isOpaque()) {
      continue;
    }
    // if ST has no elements; let it be without changing it
    if (ST->getNumElements() == 0) {
      continue;
    }
    auto new_ST = it.second.new_ST;
    for (unsigned i=0; i<ST->getNumElements(); ++i) {
      if (it.second.colors[i].compare("") != 0) {
	it.second.elt_tys.push_back(get_changed_type(ST->getElementType(i))->getPointerTo());
      } else {
	it.second.elt_tys.push_back(get_changed_type(ST->getElementType(i)));
      }
    }
    // Create new struct with struct pointer towards colored elements
    // before
    // struct old.A {
    //   int a color(blue);
    //   int b;
    // }
    // after
    // struct A {
    //   int* a;
    //   int b;
    // }
    new_ST->setBody(it.second.elt_tys);
  }
}

//---------------------------------
// Sgxpass::get_changed_type
//---------------------------------

// update struct body according to it's color type
Type* Sgxpass::get_changed_type(Type *src) {
  if (src == nullptr) {
    return src;
  }
  int count = 0;
  Type* ty = src;
  while (ty->isPointerTy()) {
    ty=dyn_cast<PointerType>(ty)->getElementType();
    ++count;
  }

  if ((!ty->isStructTy()) && (!ty->isFunctionTy()) && (!ty->isArrayTy()) && (!ty->isVectorTy())) {
    return src;
  }
  Type* new_ty;
  // this part create a new functiontype, ArrayType, VectorType according to new struct
  if (ty->isFunctionTy()) {
    FunctionType* fty = dyn_cast<FunctionType>(ty);
    std::vector<Type* > argTys;
    for (unsigned i=0; i<fty->getNumParams(); ++i) {
      argTys.push_back(get_changed_type(fty->getParamType(i)));
    }
    new_ty = FunctionType::get(get_changed_type(fty->getReturnType()), argTys, fty->isVarArg());
  } else if (ty->isArrayTy()) {
    new_ty = ArrayType::get(get_changed_type(ty->getArrayElementType()), ty->getArrayNumElements());
  } else if (ty->isVectorTy()) {
    new_ty = VectorType::get(get_changed_type(ty->getVectorElementType()), ty->getVectorNumElements(), dyn_cast<VectorType>(ty)->isScalable());
  } else if (ty->isStructTy()) {
    if (StructColors.find(dyn_cast<StructType>(ty)) == StructColors.end()) {
      return src;
    }
    new_ty = StructColors[dyn_cast<StructType>(ty)].new_ST;
  }
  while (count != 0) {
    new_ty=new_ty->getPointerTo();
    --count;
  }

  return new_ty;
}

//---------------------------------
// Sgxpass::testing
//---------------------------------

void Sgxpass::testing(Module &M) {
  errs() << "\n\n\n";
  for (auto elt : StructColors) {
    elt.second.print();
  }
  errs() << "\n\n\n";
  auto STs = M.getIdentifiedStructTypes();
  for (StructType* ST : STs) {
    errs() << *ST << "\n";
  }
  errs() << "\n\n\n";
}

//---------------------------------
// Sgxpass::struct_rewrite
//---------------------------------

void Sgxpass::struct_rewrite(Module &M) {
  initialize_StructColors(M);
  update_structatt_colors(M);
  update_StructColors();
  update_elt_tys(M);
  //testing(M);
  //ValueToValueMapTy VM;
  //MyMapper TypeMapper = MyMapper(StructMap);
}
