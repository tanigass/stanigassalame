#include "Sgxpass.hpp"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Argument.h"
#include "llvm/IR/Value.h"

//////////////////////////////////////////////////////////////////////////////////////
////////////////////////////_Sgxpass_related_functions_///////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

//---------------------------
// Sgxpass::sort_function
//---------------------------

void Sgxpass::sort_function( Function* cur){
   
  //for(Function* F = CallFuncs[cur].begin(); F != CallFuncs[cur].end(); ++F ){
  for(Function* F : CallFuncs[cur]){
    //Function *F=CI->getCalledFunction();
    if(auto it = find(AllFuncs.begin(), AllFuncs.end(), F); it != AllFuncs.end()){
      AllFuncs.erase(it);
      sort_function(F);
    }
  }
  Sorted.push_back(cur);
  if(auto it = find(AllFuncs.begin(), AllFuncs.end(), cur); it != AllFuncs.end()){
      AllFuncs.erase(it);
  }
  return;
}

//-------------------------------
// Sgxpass::treat_callInst
//-------------------------------

// return false in case of ERROR
bool Sgxpass::treat_callInst(CallInst* CI, Function* F){

  Function *CF = CI->getCalledFunction();
  Instruction *I = dyn_cast<Instruction>(CI);
  if (CF->hasName()) {
    if ((CF->getName().str().find("llvm.dbg.value") == 0) ||
	(CF->getName().str().find(".annotation") != std::string::npos)) {
      return true;
    }
  }

  if (!check_calledFunction(CF, F)) {
    return false;
  }
  initialize_constraint(I, F);

  if (Funcs[F].tmp.find(I) != Funcs[F].tmp.end()) {
    return true;
  }
  initialize_tmp(I, F);

  // Skip called function annotated as "sgx_ignore"
  if (auto it = find(IgnoredFuncs.begin(), IgnoredFuncs.end(), CF); it != IgnoredFuncs.end()) {
    return true;
  }

  if (ExternalSafeFuncs.find(CF) != ExternalSafeFuncs.end()) {
    Funcs[F].hasExternalSafe = true;
  }

  if(!(Funcs[CF].correctness)){
    return false;
  }
  if (!check_correctness(CI, F)) {
    return false;
  }

  // update annotation constraints in InstructionAttributes associated to I
  update_annot_callInst(CF, F, I);
  // Update argument constraints in InstructionAttributes associated to I
  if (!update_arg_callInst(CF, F, I, CI)) {
    return false;
  }
  if (!check_returnInst(I, F)) {
    return false;
  }

  //check_malloc(CI, CF, F);
  return true;
}

//-------------------------------
// Sgxpass::treat_instruction
//-------------------------------

// return false in case of error
bool Sgxpass::treat_instruction(Instruction* I, Function* F){
  auto CI = dyn_cast<CallInst>(I);
  if (CI != nullptr) {
    //if the call is an indirect function call
    //i.e. pointer function call
    if (CI->isIndirectCall() || CI->isInlineAsm()){
      initialize_constraint(I, F);
      initialize_tmp(I, F);
      return true;
    }

    if (!check_callInst(CI, F)) {
      return false;
    }
    return true;
  }

  initialize_constraint(I, F);

  if (Funcs[F].tmp.find(I) == Funcs[F].tmp.end()) {
    initialize_tmp(I, F);

    for (const Use &U : I->operands()){
      //Update constraints according to the use of an instruction
      if (!treat_use(U, I, F)) {
	return false;
      }
    }
    if (!check_returnInst(I, F)) {
      return false;
    }
    // check if I is a StoreInst and store a colored data in uncolored
    if (!treat_storeInst(I, F)) {
      return false;
    }
  }
  return true;
}

//-------------------------------
// Sgxpass::check_constraint_bb
//-------------------------------

// return false in case of ERROR
bool Sgxpass::check_constraint_bb(Instruction* I, Value* V, Function* F){
  Instruction* Iu = dyn_cast<Instruction>(V);
  BasicBlock* BB = dyn_cast<BasicBlock>(V);
  InstructionAttributes copy = InstructionAttributes(F, AnnotationNum);

  for(unsigned i=0; i<copy.dependancy.size(); i++){
    // check if I exists in tmp
    if (Funcs[F].tmp.find(I) != Funcs[F].tmp.end()) {
      if (Iu != nullptr) {
	copy.dependancy[i] = Funcs[F].tmp[I].dependancy[i] || Funcs[F].tmp[Iu].dependancy[i];
      } else if (BB != nullptr) {
	copy.dependancy[i] = Funcs[F].tmp[I].dependancy[i] || Funcs[F].BBconstraints[BB].dependancy[i];
      }
    }
  }

  std::string temp = "";

  for (unsigned i=F->arg_size(); i<copy.dependancy.size(); i++) {
    if (copy.dependancy[i]) {
      if(!is_compatible(Annotations[i-F->arg_size()], temp)){
	Insts[I] = std::string("ERROR");
	ErrorInsts[I].emplace(errorAnnotation(I, Annotations[i-F->arg_size()], temp));
	return false;
      } else {
	temp = Annotations[i-F->arg_size()];
      }
    }
  }

  return true;
}

//---------------------------------
// Sgxpass::treat_basicBlock_while
//---------------------------------

// return false in case of ERROR
// update constraints of Basicblock B and all it's instruction as the same as I
bool Sgxpass::update_constraints_bb(BasicBlock* B, Function* F, Instruction* I){
  // update constraints of B, according to the instruction I
  Funcs[F].BBconstraints[B].add(Funcs[F].constraints[I]);
  for (Instruction &ChI: *B) {
    if (dyn_cast<BranchInst>(&ChI)) {
      if (!update_constraint(&ChI, I, F)) {
	return false;
      }
    } else {
      if (!check_constraint_bb(&ChI, I, F)) {
	return false;
      }
    }
    CallInst* CI = dyn_cast<CallInst>(&ChI);
    if (CI != nullptr) {
      if(!(CI->isIndirectCall()) && !(CI->isInlineAsm())){
	Function *CF = CI->getCalledFunction();
	for(auto col : Funcs[CF].fn_colors){
	  if(!is_compatible(col, Insts[I])){
	    Insts[&ChI] = "ERROR";
	    ErrorInsts[&ChI].emplace(errorFunctionBB(CF->getName().str(), Insts[I], col));
	    return false;
	  }
	}
	if (auto it = find(MonochromaticFuncs.begin(), MonochromaticFuncs.end(), CF);
	    it != MonochromaticFuncs.end()) {
	  continue;
	}
	if (auto it = find(IgnoredFuncs.begin(), IgnoredFuncs.end(), CF);
	    it != IgnoredFuncs.end()) {
	  continue;
	}
	if (Funcs[CF].hasExternalSafe) {
	  if (ExternalSafeFuncs_wrap.find(CF) == ExternalSafeFuncs_wrap.end() &&
	      Insts[I].compare("") != 0 && Insts[I].compare(UNTRUSTED) != 0) {
	    ExtSafeAttr Eattr = ExtSafeAttr(CF);
	    ExternalSafeFuncs_wrap[CF] = Eattr;
	    ExternalSafeFuncs_wrap[CF].add_color(Insts[I]);
	    continue;
	  }
	}
	if (ExternalSafeFuncs.find(CF) != ExternalSafeFuncs.end()) {
	  ExternalSafeFuncs[CF].add_color(Insts[I]);
	  continue;
	}
	if ((CF->getInstructionCount() == 0) &&
	    !(CF->getName().str().find("llvm.") == 0)) {
	  if(!is_compatible(UNTRUSTED, Insts[I])){
	    Insts[&ChI] = "ERROR";
	    ErrorInsts[&ChI].emplace(errorFunctionBB(CF->getName().str(), Insts[I], UNTRUSTED));
	    return false;
	  }
	}
      }
    }
  }
  return true;
}

//---------------------------------
// Sgxpass::treat_basicBlock_while
//---------------------------------

// return false in case of ERROR
bool Sgxpass::treat_basicBlock_while(BasicBlock* B, Function* F, std::set<BasicBlock*>* treated, BranchInst *base_BI){
  BranchInst* BI = dyn_cast<BranchInst>(B->getTerminator());

  if (BI == nullptr) {
    return true;
  }

  for (auto *CB : BI->successors()) {
    if (treated->find(CB) != treated->end()) {
      continue;
    }
    update_constraints_bb(CB, F, base_BI);
    treated->insert(CB);
    if (!treat_basicBlock_while(CB, F, treated, base_BI)) {
      return false;
    }
    BBDependancy[CB] = base_BI;
  }
  return true;
}

//--------------------------
// Sgxpass::is_while_case
//--------------------------

// return true if the basicblock is while
// Check all children nodes to detect a loop
bool Sgxpass::is_while_case(const BasicBlock* Base, const BasicBlock* B, Function* F) {
  DominatorTree DT = DominatorTree (*F);
  auto *DTN = DT.getNode (B);
  const auto &Children = DTN->getChildren();
  for (const auto N : Children) {
    BasicBlock* CB = N->getBlock();
    // Detect a loop
    if (is_parent_of(CB, Base)) {
      return true;
    }
    if (is_while_case(Base, CB, F)) {
      return true;
    }
  }
  return false;
}

//---------------------------
// Sgxpass::treat_basicBlock
//---------------------------

// return false in case of ERROR
bool Sgxpass::treat_basicBlock(BasicBlock* B, Function* F){
  Instruction* I = B->getTerminator();
  BranchInst* BI = dyn_cast<BranchInst>(I);
  if (BI == nullptr){
    return true;
  }
  if (I != nullptr){
    DominatorTree DT = DominatorTree (*F);
    // Get DominatorTree node corresponding BasicBlock B
    auto *DTN = DT.getNode (B);
    // List of nodes directly dominated by BasicBlock B
    const auto &Children = DTN->getChildren();
    for (const auto N : Children) {
      BasicBlock* CB = N->getBlock();
      // To test if B (the direct dominator) is a direct predecessor of CB
      // Test if B is the unique predecessor of CB
      //if (is_parent_of(B,CB)) {
      if (CB->getUniquePredecessor() == B) {
	if (is_while_case(B, B, F)) {
	  std::set<BasicBlock*> treated;
	  BranchInst* BranchI = dyn_cast<BranchInst>(B->getTerminator());
	  if (!treat_basicBlock_while(B, F, &treated, BranchI)) {
	    return false;
	  }
	}
	// update constraints of CB, according to the terminator of B, the instruction I
	if (!update_constraints_bb(CB, F, I)) {
	  return false;
	}
	if(!treat_basicBlock(CB, F)){
	  return false;
	}
	BBDependancy[CB] = BI;
      } else {
	Funcs[F].BBconstraints[CB].add(Funcs[F].BBconstraints[B]);
	for (Instruction &ChI: *CB) {
	  if (dyn_cast<BranchInst>(&ChI)) {
	    if (!update_constraint(&ChI, B, F)) {
	      return false;
	    }
	  } else {
	    if (!check_constraint_bb(&ChI, B, F)) {
	      return false;
	    }
	  }

	  std::string BBcolor = "";
	  for (unsigned i=F->arg_size(); i<Funcs[F].tmp[I].dependancy.size(); i++) {
	    if (Funcs[F].BBconstraints[B].dependancy[i]) {
	      BBcolor = Annotations[i-F->arg_size()];
	      break;
	    }
	  }
	  CallInst* CI = dyn_cast<CallInst>(&ChI);
	  if (CI != nullptr) {
	    if(!(CI->isIndirectCall()) && !(CI->isInlineAsm())){
	      Function *CF = CI->getCalledFunction();
	      for(auto col : Funcs[CF].fn_colors){
		if(!is_compatible(col, BBcolor)){
		  Insts[&ChI] = "ERROR";
		  ErrorInsts[&ChI].emplace(errorFunctionBB(CF->getName().str(), Insts[I], col));
		  return false;
		}
	      }
	      if (auto it = find(MonochromaticFuncs.begin(), MonochromaticFuncs.end(), CF);
		  it != MonochromaticFuncs.end()) {
		continue;
	      }
	      if (auto it = find(IgnoredFuncs.begin(), IgnoredFuncs.end(), CF);
		  it != IgnoredFuncs.end()) {
		continue;
	      }
	      if (Funcs[CF].hasExternalSafe) {
		if (ExternalSafeFuncs.find(CF) == ExternalSafeFuncs.end() &&
		    BBcolor.compare("") != 0 && BBcolor.compare(UNTRUSTED) != 0) {
		  ExtSafeAttr Eattr = ExtSafeAttr(CF);
		  ExternalSafeFuncs_wrap[CF] = Eattr;
		  ExternalSafeFuncs_wrap[CF].add_color(BBcolor);
		  continue;
		}
	      }
	      if (ExternalSafeFuncs.find(CF) != ExternalSafeFuncs.end()) {
		ExternalSafeFuncs[CF].add_color(BBcolor);
		continue;
	      }
	      if ((CF->getInstructionCount() == 0) &&
		  !(CF->getName().str().find("llvm.") == 0)) {
		if(!is_compatible(UNTRUSTED, BBcolor)){
		  Insts[&ChI] = "ERROR";
		  ErrorInsts[&ChI].emplace(errorFunctionBB(CF->getName().str(), Insts[I], UNTRUSTED));
		  return false;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  return true;
}

//---------------------------
// Sgxpass::treat_extern_fn
//---------------------------

// default case for external functions
// argument should be either UNTRUSTED or colorless
bool Sgxpass::treat_extern_fn(Function* F){
  if (F->getInstructionCount() == 0) {
    std::vector<bool> temp(F->arg_size()+AnnotationNum, false);
    for (unsigned i=0; i<F->arg_size(); i++) {
      temp[i] = true;
      //Funcs[F].ret[i] = true;
    }
    auto pos = AnnotationList.find(UNTRUSTED);
    int position = std::distance(AnnotationList.begin(), pos);
    temp[F->arg_size() + position] = true;
    Funcs[F].call.emplace(temp);
    //Funcs[F].ret[F->arg_size() + position] = true;
    return true;
  }
  return false;
}

//------------------------------
// Sgxpass::treat_monochromatic
//------------------------------

// function annotated as "monochromatic"
bool Sgxpass::treat_monochromatic(Function* F){
  if (auto it = find(MonochromaticFuncs.begin(), MonochromaticFuncs.end(), F); it != MonochromaticFuncs.end()) {
    //create one array with all arguments interdependent
    //Which will make the function monochromatique
    std::vector<bool> temp(F->arg_size()+AnnotationNum, false);
    for (unsigned i=0; i<F->arg_size(); i++) {
      temp[i] = true;
      Funcs[F].ret[i] = true;
    }
    Funcs[F].call.emplace(temp);
    return true;
  }
  return false;
}

//----------------------------
// Sgxpass::fn_special_cases
//----------------------------

//treat special cases related to function
bool Sgxpass::fn_special_cases(Function* F){
  // skip debug functions and annotation related functions generated by llvm
  if (F->hasName()) {
    if ((F->getName().str().find("llvm.dbg.") == 0) ||
	(F->getName().str().find(".annotation") != std::string::npos)) {
      return true;
    }
  }

  // Skip function annotated as "sgx_ignore"
  if (auto it = find(IgnoredFuncs.begin(), IgnoredFuncs.end(), F);
      it != IgnoredFuncs.end()) {
    return true;
  }

  // function annotated as "monochromatic"
  if(treat_monochromatic(F)){
    return true;
  }

  // default case for external functions
  if(treat_extern_fn(F)){
    return true;
  }

  return false;
}

//---------------------------
// Sgxpass::treat_function
//---------------------------

// return false in case of ERROR
bool Sgxpass::treat_function(Function* F){
  if (Funcs.find(F) == Funcs.end()) {
    FunctionAttributes f_attr=FunctionAttributes(F, AnnotationNum);
    Funcs.emplace(std::make_pair(F, f_attr));
  }

  if(fn_special_cases(F)){
    return true;
  }

  Funcs[F].bb_stabilized = true;
  // Iterate over all BasicBlocks of a Function
  for (BasicBlock &B : *F) {
    // Initialise BBconstraints, constraints related to BasicBlocks
    // same constraints will be updated with each treat_function
    if (Funcs[F].BBconstraints.find(&B) == Funcs[F].BBconstraints.end()) {
      InstructionAttributes i_attr = InstructionAttributes(F, AnnotationNum);
      Funcs[F].BBconstraints.emplace(std::make_pair(&B, i_attr));
    }
    // Iterate over all Instructions of a BasicBlock
    for (Instruction &I: B) {
      if(!treat_instruction(&I, F)) {
	Funcs[F].correctness = false;
	return false;
      }
    }
  }
  ///////// BasicBlock part ///////////
  if (!Funcs[F].fast_stabilize()) {
    stabilized = false;
  }
  for (BasicBlock &B : *F) {
    if(!treat_basicBlock(&B,F)) {
      Funcs[F].correctness = false;
      return false;
    }
    /*
    if (!Funcs[F].fast_stabilize()) {
      Funcs[F].bb_stabilized = false;
    }
     */
  }
  // If any constraint is addded during BasicBlock part
  if (!Funcs[F].stabilize()) {
    treat_function(F);
  }
  //////// End BasicBlock part ////////
  if (!Funcs[F].stabilize()) {
    stabilized = false;
  }

  return true;
}

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////_Sgxpass_related_functions_end_/////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////_run_on_module_related_//////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

//---------------------------------
// Sgxpass::treat_global_variable
//---------------------------------

// Create Annotation leaf with all known annotation (llvm.global.annotations)
// get monochromatic and sgx_ignore functions list
void Sgxpass::treat_global_variable(Module &M) {
  // Iterate over global variables
  for (const auto& G : M.getGlobalList()) {
    if (G.hasName()) {
      if (G.getName() == "llvm.global.annotations"){
	//number of global variables
	auto num =get_num_GlobalVariable(G);
	for(unsigned int i=0; i<num; i++){
	  std::string global_color=get_annotation(G, i);
	  const Value* GV=get_GlobalVariable(G, i);

	  //Annotation *annot = new Annotation(global_color);
	  Values.emplace(std::make_pair(GV, global_color));
	  // If the user of colored GlobalVariable is not an Instruction
	  // Add the user as an annotation leaf
	  for (auto *UU : GV->users()) {
	    if (!dyn_cast<Instruction>(UU)) {
	      Values.emplace(std::make_pair(UU, global_color));
	    }
	  }
	  AnnotationList.emplace(global_color);
	  if (Colors.find(global_color) == Colors.end()) {
	    Colors[global_color] = ColorAttributes(G, i);
	  }
	}
      }
    }
  }

  //get monochromatic, sgx_ignore and libfunction functions list
  for (const auto& G : M.getGlobalList()) {
    if (!G.hasName()) {
      continue;
    }
    auto name=G.getName().str();
    if (name.find("__monochromatic__")==0){
      Function* MCF=get_fn_from_GV(G);
      MonochromaticFuncs.push_back(MCF);
      check_intrinsic_mono(name, M);
    } else if (name.find("__sgx_ignore__")==0){
      Function* IF=get_fn_from_GV(G);
      IgnoredFuncs.push_back(IF);
      check_intrinsic_ig(name, M);
    } else if (name.find("__external_safe__")==0){
      Function* EF=get_fn_from_GV(G);
      ExtSafeAttr Eattr = ExtSafeAttr(EF);
      ExternalSafeFuncs[EF] = Eattr;
    } else if (name.find("__libfunction__")==0){
      Function* LF=get_fn_from_GV(G);
      LibFunctions.emplace(LF);
    }
    AnnotationList.emplace(UNTRUSTED);
    /*
    else if (name != "llvm.global.annotations") {
      //const GlobalVariable* GV=dyn_cast<GlobalVariable>(&G);
      const Value* V=dyn_cast<GlobalVariable>(&G);
      if ( Values.find(V) == Values.end() ) {
	//Annotation *annot = new Annotation(std::string(UNTRUSTED));
	Values.emplace(std::make_pair(V, std::string(UNTRUSTED)));
	AnnotationList.emplace(std::string(UNTRUSTED));
      }
    }
    */
  }

  if (LibFunctions.size() != 0) {
    Libfunction_activated = true;
    errs() << "\nLibFunction activated\n\n";
  }

  // add UNTRUSTED as color to external variables
  for (const auto& G : M.getGlobalList()) {
    // isDeclaration() Return true if the primary definition of this global value
    // is outside of the current translation unit.
    if (G.isDeclaration()) {
      Values.emplace(std::make_pair(&G, UNTRUSTED));
      // If the user of colored GlobalVariable is not an Instruction
      // Add the user as an annotation leaf
      for (auto *UU : G.users()) {
	if (!dyn_cast<Instruction>(UU)) {
	  Values.emplace(std::make_pair(UU, UNTRUSTED));
	}
      }
    }

  }
  return;
}

//---------------------------------
// Sgxpass::treat_local_variable
//---------------------------------

// Create Annotation leaf with all known annotation (llvm.var.annotation, llvm.ptr.annotation)
void Sgxpass::treat_local_variable(const CallInst* CI, const Instruction &I, const Function *CF) {
  const std::string& color=get_annotation(*CI);
  if (Colors.find(color) == Colors.end()) {
    Colors[color] = ColorAttributes(*CI);
  }
  //Annotation *annot = new Annotation(color);
  if (CF->hasName()) {
    if (CF->getName().find("var.annotation") != std::string::npos) {
      auto val = get_Value(*CI);
      Values.emplace(std::make_pair(val, color));
      AnnotationList.emplace(color);
      Instruction* Ii = dyn_cast<Instruction>(val);
      Insts.emplace(std::make_pair(Ii, color));
      Insts.emplace(std::make_pair(&I, color));
    } else if (CF->getName().find("ptr.annotation") != std::string::npos) {
      auto *val = dyn_cast<Value>(CI);
      Values.emplace(std::make_pair(val, color));
      AnnotationList.emplace(color);
      auto* Ii = dyn_cast<Instruction>(val);
      Insts.emplace(std::make_pair(Ii, color));
      //treat_colored_struct(CI, color);
    }
  }
  return;
}

//---------------------------------
// Sgxpass::initialise
//---------------------------------

// Initialise some specific members of Sgxpass
// memory_function_names
void Sgxpass::initialise() {
  // monochromatic by default
  memory_function_names.insert({"malloc", "free", "memcpy", "memmove", "memset"});
  C_library_intrinsic.insert({"memcpy", "memmove", "memset", "sqrt", "powi", "sin",
	"cos", "pow", "exp", "exp2", "log", "log10", "log2", "fma", "fabs",
	"minnum", "maxnum", "minimum", "maximum", "copysign", "floor", "ceil", "trunc",
	"rint", "nearbyint", "round", "lround", "llround", "lrint", "llrint", "bswap",
	"ctlz", "ctpop", "isunordered"});
  return;
}

//-----------------------------------------
// Sgxpass::treat_default_monochromatic_fn
//-----------------------------------------

// Create Annotation leaf with all known annotation (llvm.var.annotation, llvm.ptr.annotation)
void Sgxpass::treat_default_monochromatic_fn(Function *F) {
  if (F->getInstructionCount() != 0) {
    return;
  }
  auto Fname = F->getName().str();
  for (auto intrinsic : C_library_intrinsic) {
    std::string in_name = std::string("llvm.") + intrinsic + std::string(".");
    if ((Fname.find(in_name) == 0)){
      MonochromaticFuncs.push_back(F);
      return;
    }
  }
  for (auto m_name : memory_function_names) {
    if ((Fname.compare(m_name) == 0)){
      MonochromaticFuncs.push_back(F);
      return;
    }
  }
  return;
}

//-----------------------------------------
// Sgxpass::create_library_functions
//-----------------------------------------

// create a wrapper function to call all functions as library functions
// define i32 @g(i32 %0, i32 %1, i32 %2) {
// entry:
//   %3 = call i32 @old.g(i32 %0, i32 %1, i32 %2)
//   ret i32 %3
// }


void Sgxpass::create_library_functions(Module &M) {
  LLVMContext &context = M.getContext();
  for (Function &F : M) {
    if (!F.hasName()) {
      continue;
    }
    if (LibFuncs.find(&F) != LibFuncs.end()) {
      continue;
    }

    std::string fname = F.getName().str();

    // Add all the function that could be potentially used as Function pointers to a map
    // Associate it with it's name
    // This map will be used while cloning UNTRUSTED.ll to update VMap with newly created functions
    // If the function is not already treated we add it to the map
    OldLibFuncs[&F] = fname;

    if (F.getInstructionCount() == 0) {
      continue;
    }

    // If any of the function is noted as libfunction then,
    // If the function is not noted as libfunction, it will not be added as Library function
    // If no function is noted as libfunction
    // by default all functions are added as Library function
    if (Libfunction_activated &&
	(LibFunctions.find(&F) == LibFunctions.end())) {
      continue;
    }

    F.setName("old."+fname);

    Function* F_new = Function::Create(F.getFunctionType(),
				       Function::ExternalLinkage, fname, M);

    LibFuncs.emplace(F_new);
    // Added the LibFuncs to IgnoredFuncs because
    // we don't need to propage colors inside this wrapper function
    // We don't treat this function only for the color propagation part
    IgnoredFuncs.push_back(F_new);
    IRBuilder<> builder(context);
    auto *entry = BasicBlock::Create(context, "entry", F_new);
    builder.SetInsertPoint(entry);
    Value *result;
    if (F_new->arg_size() > 0) {
      std::vector<Value*> args;
      //auto args = F_new->arg_begin();
      for(auto arg = F_new->arg_begin(); arg != F_new->arg_end(); ++arg) {
	args.push_back(arg);
      }
      //%3 = call i32 @old.g(i32 %0, i32 %1, i32 %2)
      result = builder.CreateCall(&F, args);
    } else {
      result = builder.CreateCall(&F);
    }

    if (F_new->getReturnType()->isVoidTy()) {
      // ret void
      builder.CreateRetVoid();
    } else {
      // ret i32 %3
      builder.CreateRet(result);
    }
    // Verify at the end
    verifyFunction(*F_new);

    // treat the wrapper function for color detection like other functions
    treat_function(F_new);
  }
}


//-----------------------------------------
// Sgxpass::create_ext_safe_wrapper
//-----------------------------------------

Function* Sgxpass::create_ext_safe_wrapper(Module &M, Function *F, std::string fname) {
  LLVMContext &context = M.getContext();
  Function* F_new = Function::Create(F->getFunctionType(),
				     Function::ExternalLinkage, fname, M);
  IRBuilder<> builder(context);
  auto *entry = BasicBlock::Create(context, "entry", F_new);
  builder.SetInsertPoint(entry);
  Value *result;
  if (F_new->arg_size() > 0) {
    std::vector<Value*> args;
    //auto args = F_new->arg_begin();
    for(auto arg = F_new->arg_begin(); arg != F_new->arg_end(); ++arg) {
      args.push_back(arg);
    }
    //%3 = call i32 @old.g(i32 %0, i32 %1, i32 %2)
    result = builder.CreateCall(F, args);
  } else {
    result = builder.CreateCall(F);
  }

  if (F_new->getReturnType()->isVoidTy()) {
    // ret void
    builder.CreateRetVoid();
  } else {
    // ret i32 %3
    builder.CreateRet(result);
  }
  // Verify at the end
  verifyFunction(*F_new);

  // treat the wrapper function for color detection like other functions
  treat_function(F_new);

  return F_new;
}

//-----------------------------------------
// Sgxpass::create_ext_safe_interfaces
//-----------------------------------------

// the first wrapper
// define i32 @pthread_cond_signal_blue(%union.pthread_cond_t* %0) {
// entry:
//   %1 = call i32 @pthread_cond_signal(%union.pthread_cond_t* %0)
//   ret i32 %1
// }

// the second wrapper which serves as interface
// This function will be noted blue while rewriting
// in order to call the external safe function a colored BasicBlock
// define i32 @pthread_cond_signal_blue_interface(%union.pthread_cond_t* %0) {
// entry:
//   %1 = call i32 @pthread_cond_signal_blue(%union.pthread_cond_t* %0)
//   ret i32 %1
// }


void Sgxpass::create_ext_safe_interfaces(Module &M) {
  LLVMContext &context = M.getContext();
  for (auto &p : ExternalSafeFuncs_wrap) {
    ExternalSafeFuncs[p.first] = p.second;
  }
  for (auto &p : ExternalSafeFuncs) {
    Function *F = p.first;
    for (auto col : ExternalSafeFuncs[F].call_colors) {

      // these colors doesn't need interfaces
      if (col.compare("") == 0 || col.compare(UNTRUSTED) == 0) {
	continue;
      }
      std::string first_name = F->getName().str() + "_" + col;
      // the first wrapper
      Function* f_first = create_ext_safe_wrapper(M, F, first_name);

      std::string second_name = first_name + "_interface";
      // the second wrapper
      Function* f_second = create_ext_safe_wrapper(M, f_first, second_name);

      ExternalSafeFuncs[F].set_function(col, f_second);
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////_run_on_module_related_end_//////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
