#include "Sgxpass.hpp"

//////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////_treat_callInst_related_//////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

//-------------------------------
// Sgxpass::check_calledFunction
//-------------------------------

// This block is to make sure that the called function is treated
bool Sgxpass::check_calledFunction(Function* CF, Function* F){
  // This block is to make sure that the called function is treated
  if (Funcs.find(CF) == Funcs.end()){
    bool temp = Funcs[F].terminated;
    Funcs[F].terminated = false;
    if (!treat_function(CF)) {
      return false;
    }
    if (Funcs[CF].terminated == true){
      Funcs[F].terminated = temp;
    }
  }
  if (Funcs[CF].terminated == false){
    Funcs[F].terminated = false;
  }
  return true;
}

//-------------------------------
// Sgxpass::update_annot_callInst
//-------------------------------

// Update annotation constraints in InstructionAttributes associated to I
void Sgxpass::update_annot_callInst(Function* CF, Function* F, Instruction* I){
  for (unsigned i=CF->arg_size(); i<Funcs[CF].ret.size(); i++) {
    if ( Funcs[CF].ret[i] ) {
      Insts[I] = Annotations[i-CF->arg_size()];
      Funcs[F].fn_colors.emplace(Annotations[i-CF->arg_size()]);
      Funcs[F].tmp[I].dependancy[i-CF->arg_size()+F->arg_size()] = true;
    }
  }
  return;
}

//-------------------------------
// Sgxpass::update_arg_callInst
//-------------------------------

// Update argument constraints in InstructionAttributes associated to I
bool Sgxpass::update_arg_callInst(Function* CF, Function* F, Instruction* I, CallInst* CI){
  if (CF->arg_size() == 0) {
    return true;
  }
  auto arg_size = CF->arg_size();
  if (CF->arg_size() < CI->arg_size()) {
    // example: true for printf
    arg_size = CI->arg_size();
  }
  std::vector<bool> ret_val(arg_size+AnnotationNum, true);
  std::copy(Funcs[CF].ret.begin(), Funcs[CF].ret.begin()+CF->arg_size(), ret_val.begin());
  std::copy(Funcs[CF].ret.begin()+CF->arg_size(), Funcs[CF].ret.end(), ret_val.begin()+arg_size);

  for (unsigned i=0; i<arg_size; ++i) {
    if (ret_val[i]) {
      auto Ar = CI->arg_begin()+i;
      //Same as treat_instruction
      if (!treat_use(*Ar, I, F)) {
	return false;
      }
    }
  }
  return true;
}

//----------------------------------
// Sgxpass::check_correct_callInst
//----------------------------------

// return false in case of ERROR
// Check the correctness of the callInst
bool Sgxpass::check_correct_callInst(const std::vector<std::set<unsigned>> arg_dependancy, const std::vector<std::string> arg_annot, const CallInst* CI, const Function* CF, Function* F){

  //check constraints in constraints of CF
  for (const auto attr : Funcs[CF].constraints){
    if (attr.second.root) {
      if(!arg_correct(arg_dependancy, arg_annot, attr.second.dependancy, CI, CF, F)){
	return false;
      }
    }
  }

  //check constraints in call of CF
  for (const auto dependancy : Funcs[CF].call){
    if(!arg_correct(arg_dependancy, arg_annot, dependancy, CI, CF, F)){
      return false;
    }
  }

  //check constraints in ret of CF
  if(!arg_correct(arg_dependancy, arg_annot, Funcs[CF].ret, CI, CF, F)){
    return false;
  }

  //check constraints in store of CF
  //go through the map store
  //SA.second is the StoreAttributes associated to Instruction
  for (const auto SA : Funcs[CF].store){
    if(!store_correct(arg_dependancy, arg_annot, SA.first, SA.second , CI, CF, F)){
      ErrorInsts[dyn_cast<Instruction>(CI)].emplace(errorCallInstStore(SA.first));
      return false;
    }
  }

  return true;
}


//----------------------------
// Sgxpass::check_correctness
//----------------------------

// return false in case of ERROR
bool Sgxpass::check_correctness(CallInst* CI, Function* F){
  Function *CF = CI->getCalledFunction();

  auto arg_size = CF->arg_size();
  if (CF->arg_size() < CI->arg_size()) {
    // example: true for printf
    arg_size = CI->arg_size();
  }
  //this block is to get callInst argument information as an annotation vector
  std::vector<std::string> arg_annot(arg_size, "");
  //this block is to get callInst argument information as an argument set vector
  std::vector<std::set<unsigned>> arg_dependancy(arg_size);
  //get_arg_annot_dependancy(&arg_dependancy, &arg_annot, CI, CF, F);

  unsigned i=0;
  for (const auto* arg = CI->arg_begin(); arg != CI->arg_end(); arg++ ) {
    Argument* A = dyn_cast<Argument>(arg);
    if (A != nullptr) {
      arg_dependancy[i].emplace(A->getArgNo());
    }
    Value* V = dyn_cast<Value>(arg);
    if (V != nullptr) {
      if (Values.find(V) != Values.end()) {
	arg_annot[i] = Values[V];
      } else {
	Instruction* Ia = dyn_cast<Instruction>(arg);
	if (Ia != nullptr) {
	  if (Funcs[F].tmp.find(Ia) == Funcs[F].tmp.end()){
	    if(!treat_instruction(Ia, F)){
	      return false;
	    }
	  }
	  if(Insts.find(Ia) != Insts.end()){
	    arg_annot[i] = Insts[Ia];
	  }
	  for (unsigned j=0; j<F->arg_size(); j++) {
	    if (Funcs[F].tmp[Ia].dependancy[j]) {
	      arg_dependancy[i].emplace(j);
	    }
	  }
	}
      }
    }
    i++;
  }

  if (!check_correct_callInst(arg_dependancy, arg_annot, CI, CF, F)) {
    return false;
  }
  return true;
}

//////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////_treat_callInst_related_end_/////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////_treat_instruction_related_////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

//-------------------------------
// Sgxpass::check_callInst
//-------------------------------

// return false in case of error
bool Sgxpass::check_callInst(CallInst* CI, Function* F){
  // fast stabilise is done to add new known constraintes from tmp to constraints
  // and ensure to carry a maximum information in case if we treat a new function
  if(!Funcs[F].fast_stabilize()) {
    stabilized = false;
  }
  if(!treat_callInst(CI, F)){
    return false;
  }
  return true;
}

//-------------------------------
// Sgxpass::initialize_constraint
//-------------------------------

//create InstructionAttributes associated to the Instruction I,
//if it doesn't exist in constraints
void Sgxpass::initialize_constraint(Instruction* I, Function* F){
  if (Funcs[F].constraints.find(I) == Funcs[F].constraints.end()) {
    InstructionAttributes i_attr = InstructionAttributes(F, AnnotationNum);
    Funcs[F].constraints.emplace(std::make_pair(I, i_attr));
  }
  return;
}

//-------------------------------
// Sgxpass::initialize_tmp
//-------------------------------

//create InstructionAttributes associated to the Instruction I in tmp
void Sgxpass::initialize_tmp(Instruction* I, Function* F){
  InstructionAttributes i_attr = Funcs[F].constraints[I];
  Funcs[F].tmp.emplace(std::make_pair(I, i_attr));
  return;
}

//-------------------------------
// Sgxpass::check_argument
//-------------------------------

//Update InstructionAttributes according to argument number if U is an argument
void Sgxpass::check_argument(const Use &U, Instruction* I, Function* F){
  Argument* A = dyn_cast<Argument>(U);
  if (A != nullptr) {
    Funcs[F].tmp[I].dependancy[A->getArgNo()] = true;
  }
  return;
}

//-------------------------------
// Sgxpass::check_instruction
//-------------------------------

//Update InstructionAttributes associated to I according to U's
//InstructionAttributes if U is an instruction
//Treat U if it was not treated before
bool Sgxpass::check_instruction(const Use &U, Instruction* I, Function* F){
  Instruction* Iu = dyn_cast<Instruction>(U);
  if (Iu != nullptr) {
    if (Funcs[F].tmp.find(Iu) == Funcs[F].tmp.end()) {
      if (!treat_instruction(Iu, F)){
	return false;
      }
    }
    if(!update_constraint(I, Iu, F)){
      return false;
    }
    Funcs[F].tmp[Iu].root = false;
  }
  return true;
}


//-------------------------------
// Sgxpass::check_returnInst
//-------------------------------

//If I is a returnInst
bool Sgxpass::check_returnInst(Instruction* I, Function* F){
  ReturnInst *RI = dyn_cast<ReturnInst>(I);
  if (RI != nullptr){
    if(!update_ret(F, I)){
      return false;
    }
  }
  return true;
}


//-------------------------------
// Sgxpass::treat_use
//-------------------------------

//common part between treat_instruction and treat_callInst
//Update constraits according to the use of an instruction
bool Sgxpass::treat_use(const Use &U, Instruction* I, Function* F){
  //Update InstructionAttributes according to argument number if U is an argument
  check_argument(U, I, F);

  Value* V = dyn_cast<Value>(U);
  if (V != nullptr) {
    if (Values.find(V) != Values.end()) {
      Funcs[F].fn_colors.emplace(Values[V]);
      if (!update_annot_constraint(I, F, Values[V])){
	return false;
      }
    } else {
      //Update InstructionAttributes associated to I according to U's
      //InstructionAttributes if U is an instruction
      if (!check_instruction(U, I, F)) {
	return false;
      }
    }
  }
  return true;
}

//-------------------------------
// Sgxpass::treat_storeInst
//-------------------------------

// check if I is a StoreInst and store a colored data in uncolored
// update InstructionAttributes of I
// return false in case of error
bool Sgxpass::treat_storeInst(Instruction* I, Function* F){
  // check if I is a storeInst
  auto SI = dyn_cast<StoreInst>(I);
  if (SI == nullptr) {
    return true;
  }

  StoreAttributes sa = StoreAttributes(F);
  // get destination of the store
  Value* dest = SI->getPointerOperand();
  std::string dest_color = get_store_color(dest, F);

  if (dest_color.compare("") != 0) {
    return true;
  }

  bool dest_arg = is_arg_dependant(dest, F);

  // This part is to check if there is any uncolored store in colored basicblock
  // then return false
  std::string inst_color = get_store_color(I, F);
  if (inst_color.compare("") != 0 && !(dest_arg) && (inst_color.compare(UNTRUSTED) != 0)) {
    Insts[I] = std::string("ERROR");
    ErrorInsts[I].emplace(errorStoreInst(inst_color));
    return false;
  }

  Value* source = SI->getValueOperand();
  std::string source_color = get_store_color(source, F);
  sa.source_color = source_color;

  // if source is colored and destinator doesn't depends on function argument
  // if source is not UNTRUSTED
  if ((source_color.compare("") != 0) && !(dest_arg) && (source_color.compare(UNTRUSTED) != 0)) {
    Insts[I] = std::string("ERROR");
    ErrorInsts[I].emplace(errorStoreInst(source_color));
    return false;
  }

  update_storeAtt(I, dest, source, F, &sa);
  Funcs[F].store[I]=sa;
  return true;
}

//-------------------------------
// Sgxpass::update_storeatt
//-------------------------------

// Update StoreAttribute according to dest and source of StoreInst
void Sgxpass::update_storeAtt(Instruction* I, Value* dest, Value* source, Function* F, StoreAttributes *sa){
  sa->update_dest(get_store_dep(dest, F), F);

  // source information is updated with dependancies from both source and Instruction
  // Instruction's information is added to treat the case of colored basicblock
  std::vector<bool> dep_global(F->arg_size(), false);
  std::vector<bool> dep_source = get_store_dep(source, F);
  std::vector<bool> dep_I = get_store_dep(I, F);
  for (unsigned i = 0; i<dep_global.size(); ++i) {
    dep_global[i] = dep_source[i] || dep_I[i];
  }

  sa->update_source(dep_global, F);
  return;
}

//-------------------------------
// Sgxpass::get_store_dep
//-------------------------------

// return the dependency of a value according to  argument
std::vector<bool> Sgxpass::get_store_dep(Value* val, Function* F){
  std::vector<bool> ret(F->arg_size(), false);
  Argument *A = dyn_cast<Argument>(val);
  if (A != nullptr) {
    ret[A->getArgNo()] = true;
    return ret;
  }

  Instruction *I = dyn_cast<Instruction>(val);
  if (I != nullptr) {
    return Funcs[F].tmp[I].dependancy;
  }

  return ret;
}

//-------------------------------
// Sgxpass::is_arg_dependant
//-------------------------------

// return true if the value depends on a function argument
bool Sgxpass::is_arg_dependant(Value* val, Function* F){
  if (F->arg_size() == 0) {
    return false;
  }

  Argument* A = dyn_cast<Argument>(val);
  if (A != nullptr) {
    return true;
  }

  Instruction* valI = dyn_cast<Instruction>(val);
  if (valI != nullptr) {
    // at this stage valI should be treated
    // the code should not enter this bloc
    if (Funcs[F].tmp.find(valI) == Funcs[F].tmp.end()) {
      treat_instruction(valI, F);
    }

    for (unsigned i=0; i<F->arg_size(); i++) {
      if (Funcs[F].tmp[valI].dependancy[i]) {
	return true;
      }
    }
  }
  return false;
}

//-------------------------------
// Sgxpass::get_store_color
//-------------------------------

// return true if the value is colored
std::string Sgxpass::get_store_color(Value* val, Function* F){
  if (val != nullptr) {
    if (Values.find(val) != Values.end()) {
      return Values[val];
    }
  }
  Instruction* valI = dyn_cast<Instruction>(val);
  if (valI != nullptr) {
    // at this stage valI should be treated
    // the code should not enter this bloc
    if (Funcs[F].tmp.find(valI) == Funcs[F].tmp.end()) {
      treat_instruction(valI, F);
    }
    if (Insts.find(valI) != Insts.end()) {
      return Insts[valI];
    }
  }
  return "";
}



//////////////////////////////////////////////////////////////////////////////////////
////////////////////////////_treat_instruction_related_end_///////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
