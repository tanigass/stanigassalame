#include "Sgxpass.hpp"


//--------------------------------------
// Sgxpass::get_intrinsic_fn_name
//--------------------------------------
std::string Sgxpass::get_intrinsic_fn_name(const std::string Gname){
  /*
  if(Gname.find("memcpy") != std::string::npos) {
    return "llvm.memcpy.";
  } else if (Gname.find("memmove") != std::string::npos) {
    return "llvm.memmove.";
  } else if (Gname.find("memset") != std::string::npos) {
    return "llvm.memset.";
  } else if (Gname.find("isunordered") != std::string::npos) {
    return "llvm.isunordered.";
  } else if (Gname.find("sqrt") != std::string::npos) {
    return "llvm.sqrt.";
  } else if (Gname.find("bswap") != std::string::npos) {
    return "llvm.bswap.";
  } else if (Gname.find("ctpop") != std::string::npos) {
    return "llvm.ctpop.";
  } else if (Gname.find("ctlz") != std::string::npos) {
    return "llvm.ctlz.";
  } else if (Gname.find("powi") != std::string::npos) {
    return "llvm.powi.";
  } else if (Gname.find("sin") != std::string::npos) {
    return "llvm.sin.";
  } else if (Gname.find("cos") != std::string::npos) {
    return "llvm.cos.";
  } else if (Gname.find("pow") != std::string::npos) {
    return "llvm.pow.";
  } else if (Gname.find("exp") != std::string::npos) {
    return "llvm.exp.";
  } else if (Gname.find("exp2") != std::string::npos) {
    return "llvm.exp2.";
  } else if (Gname.find("log") != std::string::npos) {
    return "llvm.log.";
  } 
  */

  for (auto intrinsic : C_library_intrinsic) {
    std::string m_name = std::string("__monochromatic__") + intrinsic;
    std::string ig_name = std::string("__sgx_ignore__") + intrinsic;
    if ((Gname.compare(m_name) == 0) || (Gname.compare(ig_name) == 0)){
      return std::string("llvm.") + intrinsic + std::string(".");
    }
  }
  return "";
}

//--------------------------------------
// Sgxpass::check_intrinsic_mono
//--------------------------------------
void Sgxpass::check_intrinsic_mono(const std::string Gname, Module &M){
  auto Fname = get_intrinsic_fn_name(Gname);
  if(Fname.compare("") != 0){
    for (Function &F : M) {
      if(F.getName().str().find(Fname) != std::string::npos) {
	MonochromaticFuncs.push_back(&F);
      }
    }
  }
  return;
}

//--------------------------------------
// Sgxpass::check_intrinsic_ig
//--------------------------------------
void Sgxpass::check_intrinsic_ig(const std::string Gname, Module &M){
  auto Fname = get_intrinsic_fn_name(Gname);
  if(Fname.compare("") != 0){
    for (Function &F : M) {
      if(F.getName().str().find(Fname) != std::string::npos) {
	IgnoredFuncs.push_back(&F);
      }
    }
  }
  return;
}
