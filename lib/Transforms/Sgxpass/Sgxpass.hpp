#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif

#include "llvm/Pass.h"
#include "llvm/Support/Allocator.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
#include "llvm/Transforms/Utils/Cloning.h"

#include <sstream>
#include <iostream>
#include <fstream>

#include "FunctionAttributes.hpp"
#include "llvm_utils.hpp"
#include "error_related.hpp"
#include "print_related.hpp"
#include "CallinstFn.hpp"
#include "CloneInfo.hpp"
#include "StructAttributes.hpp"

// No need to include if only pointers or references are used
namespace llvm {
  class GlobalVariable;
  class Instruction;
  class Module;
  class Value;
  class Use;
  class StructType;
}

//class FunctionAttributes;
//class InstructionAttributes;
//class LocalError;
class ReplaceVal;

using namespace llvm;
template <typename T> using ColorsMap = std::map<const T*, std::string>;
using ValuesAnnotation = ColorsMap<Value>;// std::map<Value*, std::string>;
using InstructionsAnnotation = ColorsMap<Instruction>;
using BBsAnnotation = std::map<const BasicBlock*, BBAttributes>;
using FunctionsAttr = std::map<const Function*, FunctionAttributes>;
using InstructionsAttr = std::map<const Instruction*, InstructionAttributes>;

class Sgxpass : public ModulePass {
  using parent_t = ModulePass;

public:
  // Constructors
  Sgxpass() : parent_t(ID) {}
  
  // Public methods
  bool runOnModule(Module &M) override;

  // Public for llvm internal work
  static char ID;

private:
  // private methods
  ////////////////////////////_main_steps_/////////////////////////////////////
  void first_pass_to_identify_instruction_colors(Module &M);

  /////////////////////////_run_on_module_related_/////////////////////////////
  /////////////_first_pass_to_identify_instruction_colors_related_////////////
  void initialise();
  void treat_global_variable(Module &M);
  void treat_local_variable(const CallInst* CI, const Instruction &I, const Function *CF);
  void treat_default_monochromatic_fn(Function* F);
  void testing();
  Function* create_ext_safe_wrapper(Module &M, Function *F, std::string fname);
  void create_ext_safe_interfaces(Module &M);
  void create_library_functions(Module &M);

  ///////////////////////////_treat_intrinsic_////////////////////////////////
  void check_intrinsic_mono(const std::string Gname, Module &M);
  void check_intrinsic_ig(const std::string Gname, Module &M);
  std::string get_intrinsic_fn_name(const std::string Gname);

  /////////////////////////_Sgxpass_related_functions_/////////////////////////
  void sort_function(Function* cur);
  bool treat_function(Function* F);
  bool treat_instruction(Instruction* I, Function* F);
  bool treat_callInst(CallInst* CI, Function* F);
  bool treat_basicBlock(BasicBlock* B, Function* F);
  bool treat_basicBlock_while(BasicBlock* B, Function* F, std::set<BasicBlock*>* treated, BranchInst *BI);
  bool is_while_case(const BasicBlock* Base, const BasicBlock* B, Function* F);
  bool update_constraints_bb(BasicBlock* B, Function* F, Instruction* I);
  bool check_constraint_bb(Instruction* I, Value* Iu, Function* F);

  /////////////////////////_treat_function_related_///////////////////////////
  bool treat_extern_fn(Function* F);
  bool treat_monochromatic(Function* F);
  bool fn_special_cases(Function* F);
  
  /////////////////////////_treat_instruction_related_/////////////////////////
  bool check_callInst(CallInst* CI, Function* F);
  void initialize_constraint(Instruction* I, Function* F);
  void initialize_tmp(Instruction* I, Function* F);
  void check_argument(const Use &U, Instruction* I, Function* F);
  bool check_instruction(const Use &U, Instruction* I, Function* F);
  bool check_returnInst(Instruction* I, Function* F);
  bool treat_use(const Use &U, Instruction* I, Function* F);
  bool treat_storeInst(Instruction* I, Function* F);
  std::string get_store_color(Value* val, Function* F);
  bool is_arg_dependant(Value* val, Function* F);
  void update_storeAtt(Instruction* I, Value* dest, Value* source, Function* F, StoreAttributes *sa);
  std::vector<bool> get_store_dep(Value* val, Function* F);

  //////////////////////////_treat_callInst_related_///////////////////////////
  bool check_calledFunction(Function* CF, Function* F);
  void update_annot_callInst(Function* CF, Function* F, Instruction* I);
  bool update_arg_callInst(Function* CF, Function* F, Instruction* I, CallInst* CI);
  bool check_correct_callInst(const std::vector<std::set<unsigned>> arg_dependancy, const std::vector<std::string> arg_annot, const CallInst* CI, const Function* CF, Function* F);
  bool check_correctness(CallInst* CI, Function* F);

  /////////////////////////////_annotation_related_/////////////////////////////
  // update constraints related to annotation and return false in case of ERROR
  bool update_annot_constraint(Instruction* I, Function* F, std::string annot);
  bool update_constraint(Instruction* I, Value* Iu, Function* F);
  bool update_ret(Function* F, Instruction* I);
  // return true if compatible
  bool is_compatible( std::string a, std::string b);
  bool arg_correct(const std::vector<std::set<unsigned>> arg_dependancy, const std::vector<std::string> arg_annot, const std::vector<bool> dependancy, const CallInst* I, const Function* CF, const Function* F);
  bool arg_correct_compatible(const std::vector<std::string> arg_annot, const std::vector<bool> dependancy, const CallInst* CI, const Function* CF, unsigned arg_size);
  bool store_correct(const std::vector<std::set<unsigned>> arg_dependancy, const std::vector<std::string> arg_annot, Instruction* SI, StoreAttributes sa, const CallInst* CI, const Function* CF, Function* F);

  //////////////////////////////_rewriting_//////////////////////////////////////
  void create_colors_mk();
  void create_colored_file(Module &M);
  void create_extern_globals(std::unique_ptr<llvm::Module>& new_module, Type *ST);
  bool update_colors(Function *F);
  bool side_effect_function(Function* CF);
  void inst_wait(Module &M, Instruction* I, std::string color);
  void inst_wait(Module &M, Instruction* I, std::string color, std::vector<std::string> colors);
  void inst_continue(Module &M, Instruction* I, std::string color);
  void inst_continue(Module &M, Instruction* I, std::string color, std::vector<std::string> colors);
  void re_branchInst(BranchInst* BI);
  void re_returnInst(ReturnInst* RI);
  void create_init_terminate(std::unique_ptr<llvm::Module>& M);
  void create_new_library_functions(std::unique_ptr<llvm::Module>& M);
  void detect_outside_fn_call(Module &M);

  //callinst_rewriting
  void callinst_rewrite(Module& M);
  Function* add_new_fn(Function* F, std::vector<std::string> vect);
  std::string get_color_inst(std::vector<bool> dependancy, std::vector<std::string> vect);
  void update_side_effect(Module& M);
  void create_function_vect(Module& M);
  Function* create_constructor(std::unique_ptr<llvm::Module> &M);
  Function* create_destructor(std::unique_ptr<llvm::Module> &M);
  Function* get_new_outside_fn(Function* F);
  void create_outside_fns(Module& M);

  //branchinst_rewriting
  const BasicBlock* branchSuccessor(const BranchInst* BI, Function* F);
  void treat_while_case(Function* F, const BranchInst* root, const BasicBlock* cur, std::vector<const BasicBlock*>* res, std::set<const BasicBlock*>* treated);

  //cloning functions with attr
  std::vector<std::string> get_arg_vect(const CallInst *CI, const Function* old_F,
				    std::vector<std::string> vect);
  Function *CloneFunction_attr(std::vector<std::string> vect,
			       Function *F, ValueToValueMapTy &VMap,
			       ClonedCodeInfo *CodeInfo = nullptr);
  void CloneFunctionInto_attr(std::vector<std::string> vect,
			      Function *NewFunc, const Function *OldFunc,
			      ValueToValueMapTy &VMap, bool ModuleLevelChanges,
			      SmallVectorImpl<ReturnInst*> &Returns,
			      const char *NameSuffix = "",
			      ClonedCodeInfo *CodeInfo = nullptr,
			      ValueMapTypeRemapper *TypeMapper = nullptr,
			      ValueMaterializer *Materializer = nullptr);
  BasicBlock* CloneBasicBlock_attr(std::vector<std::string> vect,
				   const BasicBlock *BB, ValueToValueMapTy &VMap,
				   const Twine &NameSuffix, Function *F,
				   ClonedCodeInfo *CodeInfo,
				   DebugInfoFinder *DIFinder);
  void update_BBAttributes(Function *F, BasicBlock *root);
  void update_BBWhileCase(BasicBlock *B, Function *F, int *sync);
  bool sync_needed(const Instruction* I);

  //cloning functions according to color

  std::set<std::string> to_be_spawned(std::string color, const CallInst* CI);
  Instruction* spawn_color(Function *F, Module *M, std::string color);
  Instruction* sgxlr_continue(const Function *F, std::string color, Module *M,
			      Value *arg, Instruction* I, ValueToValueMapTy &VMap);
  Instruction* sgxlr_continue_after(const Function *F, std::string color, Module *M,
				    Value *arg, Instruction* I, ValueToValueMapTy &VMap);
  Instruction* sgxlr_wait(const Function *F, BasicBlock *BB, Module *M, ValueToValueMapTy &VMap);
  Instruction* sgxlr_wait_before(const Function *F, Instruction *I, Module *M,
				 ValueToValueMapTy &VMap);
  void send_arguments(const Function *F, std::string color, Module *M,
		      const CallInst *CI, Instruction* spawn, ValueToValueMapTy &VMap);
  BasicBlock* getPredecessor(const BasicBlock* BB, ValueToValueMapTy &VMap,
			     std::set<const BasicBlock*>* BBToDelete);
  const BasicBlock* successor_wait(const BranchInst* BI, Function* F,
				   std::set<const BasicBlock*>* treated);

  bool has_common_col(const Function* F, const Function* CF);
  Function *CloneFunction_col(std::string color,
			      Function *F, ValueToValueMapTy &VMap,
			      ClonedCodeInfo *CodeInfo = nullptr);
  void CloneFunctionInto_col(std::string color,
			     Function *NewFunc, Function *OldF, const Function *OldFunc,
			     ValueToValueMapTy &VMap, bool ModuleLevelChanges,
			     SmallVectorImpl<ReturnInst*> &Returns,
			     const char *NameSuffix = "",
			     ClonedCodeInfo *CodeInfo = nullptr,
			     ValueMapTypeRemapper *TypeMapper = nullptr,
			     ValueMaterializer *Materializer = nullptr);
  BasicBlock* CloneBasicBlock_col(std::string color,
				  const BasicBlock *BB, ValueToValueMapTy &VMap,
				  const Twine &NameSuffix, Function *F, Function *OldF,
				  std::map<Instruction*, const BasicBlock*>* branches,
				  std::set<const BasicBlock*>* AddWait,
				  std::set<const BasicBlock*>* AddCont,
				  std::set<GetElementPtrInst*>* TypeChange,
				  ClonedCodeInfo *CodeInfo,
				  DebugInfoFinder *DIFinder);

  //sgxlr functions
  void create_self(Module* new_module);
  StructType* create_selftype_(Module* new_module);
  void change_self_color(std::unique_ptr<llvm::Module>& new_module, std::string str);
  StructType* create_selftype(std::unique_ptr<llvm::Module>& new_module);
  StructType* create_colortype(std::unique_ptr<llvm::Module>& new_module);
  void create_sgxlr_functions(Module &M);
  Function* create_sgxlr_start_colored_thread(std::unique_ptr<llvm::Module>& M);
  Function* create_sgxlr_terminate_colored_thread(std::unique_ptr<llvm::Module>& M);
  Function* create_sgxlr_terminate(std::unique_ptr<llvm::Module>& M);
  Function* create_sgxlr_initialize(std::unique_ptr<llvm::Module>& M);
  void create_sgxlr_wait(Module &M);
  void create_sgxlr_continue(Module &M);
  void create_sgxlr_spawn(Module &M);
  void create_sgxlr_malloc_in(std::unique_ptr<llvm::Module> &M);
  void create_sgxlr_malloc_out(std::unique_ptr<llvm::Module> &M);
  void create_sgxlr_malloc_out(Module &M);
  Function* get_spawned_fn(std::unique_ptr<llvm::Module>& M, const Function* F, std::string str);
  Function* get_sync_fn(std::unique_ptr<llvm::Module>& M, const Function* F, std::string str);

  //globals_rewriting
  Type* create_globals(Module &M);
  void user_load_store(unsigned int i, GlobalVariable* GV, Instruction* I, Type* globals_type, std::vector<ReplaceVal>* replace_users, unsigned int index);
  void user_gep_op(unsigned int i, GlobalVariable* GV, GEPOperator* UO, Type* globals_type, std::vector<ReplaceVal>* replace_users);
  void change_globals(Module &M);
  void change_colored_globals(Module &M);

  //cloning
  std::unique_ptr<Module> CloneModule(const Module &M, std::string color);
  std::unique_ptr<Module> CloneModule(const Module &M, ValueToValueMapTy &VMap, std::string color);
  std::unique_ptr<Module> CloneModule(const Module &M, ValueToValueMapTy &VMap,
				      function_ref<bool(const GlobalValue *)> ShouldCloneDefinition,
				      std::string color);
  bool to_be_cloned_instruction(std::string color, const Instruction* I);
  void CloneFunctionInto(Function *NewFunc, const Function *OldFunc,
			 ValueToValueMapTy &VMap, bool ModuleLevelChanges,
			 SmallVectorImpl<ReturnInst*> &Returns,
			 const char *NameSuffix = "",
			 ClonedCodeInfo *CodeInfo = nullptr,
			 ValueMapTypeRemapper *TypeMapper = nullptr,
			 ValueMaterializer *Materializer = nullptr);
  BasicBlock* CloneBasicBlock(const BasicBlock *BB, ValueToValueMapTy &VMap,
			      const Twine &NameSuffix, Function *F,
			      ClonedCodeInfo *CodeInfo,
			      DebugInfoFinder *DIFinder);

  ////////////////////////////////////_struct_related_///////////////////////////
  void struct_rewrite(Module &M);
  void initialize_StructColors(Module &M);
  void testing(Module &M);
  Type* get_changed_type(Type *ty_init);
  void update_elt_tys(Module &M);
  void update_structatt_colors(Module &M);
  void treat_colored_struct(const CallInst* CI, std::string color);
  StructType* get_base_type(PointerType *pty);
  void update_StructColors();
  bool is_colored_type(Type *ty, std::set<Type*>* treated);


  ////////////////////////////////////_malloc_related_///////////////////////////
  void detect_malloc(Module &M);
  CallInst* get_malloc_inst(Instruction* I, Instruction* SI, std::set<Instruction*>* treated);
  bool is_memory_function(std::string name);
  /*
  bool malloc_to_xalloc(CallInst* CI, Module &M);
  Function* create_xalloc(Module &M);
  void check_malloc(CallInst* CI, Function* CF, Function* F);
  void update_isMalloc(CallInst* CI, Function* F);
  bool is_malloc(std::string name);
  bool is_struct_malloc(CallInst* CI);
  void treat_colored_struct(const CallInst* CI, std::string color);
  void create_structs(Module &M);
  void change_struct_getElementPtr(GetElementPtrInst *GI, StructType *ST, Module &M);
  */
  // private members
  ////////////////////////////_related_to_annotation_////////////////////////////
  // map of all annotation given by the developer
  ValuesAnnotation Values;
  // map of all instruction associated with its annotation
  // this variable is to debugging purpose
  // this variable is used to store instruction colors for rewriting
  InstructionsAnnotation Insts;
  // map of all BasicBlock associated with its annotation
  // this variable is used to store BB colors for rewriting
  BBsAnnotation BBColor;
  // list of functions annoted as "sgx_ignore"
  std::vector<const Function*> IgnoredFuncs;
  // list of functions annoted as "external_safe"
  std::map<Function*, ExtSafeAttr> ExternalSafeFuncs;
  std::map<Function*, ExtSafeAttr> ExternalSafeFuncs_wrap;
  // A map to collect Interface functions for External Safe function with their color
  std::map<Function*, std::string> InterfaceFuncs;
  // list of functions annoted as "monochromatic"
  std::vector<const Function*> MonochromaticFuncs;
  // list of functions annoted as "libfunction"
  std::set<const Function*> LibFunctions;
  bool Libfunction_activated = false;
  // vector of all known annotation
  std::set<std::string> AnnotationList;
  std::vector<std::string> Annotations; 
  FunctionsAttr Funcs;
  int AnnotationNum = 0;
  bool correct = true;
  std::string UNTRUSTED = "UNTRUSTED";

  /////////////////////////_related_to_function_sorting_/////////////////////////
  // list of all function
  std::vector<Function*> AllFuncs;
  // map associating evry functions with it's called functions
  std::map<Function *,std::set<Function*>> CallFuncs;
  std::vector<Function*> Sorted;
  std::vector<Function*> Sorted2;
  bool stabilized = true;
  Function* main = nullptr;

  ///////////////////////////////_related_to_error_//////////////////////////////
  std::map<const Instruction*, std::set<LocalError>> ErrorInsts;

  ///////////////////////////_special_functions_///////////////////////////
  std::set<std::string> memory_function_names;
  std::set<std::string> C_library_intrinsic;

  ////////////////////////////////////_malloc_related_///////////////////////////
  std::map<const CallInst*, Instruction*> struct_mallocs;
  std::map<const StoreInst*, const CallInst*> struct_mallocs_rev;
  std::set<const CallInst*> uncolor_malloc;
  /*
  std::set<CallInst*> StructMallocInst;
  std::map<StructType*, std::vector<std::string>> ColoredStructs;
  */
  int malloc_num;

  ///////////////////////////////_rewriting_////////////////////////////////////
  std::set<const Function*> VectFuncs;
  std::map<const Instruction*, std::set<std::string>> ColorInsts;
  std::map<Instruction*, Instruction*> PlaceCallInst;
  std::map<const BasicBlock*, Instruction*> BBDependancy;
  std::set<Instruction*> ToBeDeleted;
  std::set<Function*> ToBeDeletedFn;
  std::map<std::string, ColorAttributes> Colors;

  std::map<const Function*, CallinstFn> CallFns;
  std::map<const Function*, RewriteAttr> RewriteAttrs;
  bool side_effect_stabilised;
  std::map<const Function*, std::string> FunctionColors;
  std::map<Function*, std::vector<SpawnArg>> SpawnArgs;
  std::map<const BranchInst*, int> NeedSyncVar;
  std::set<const CallInst*> Continue_sent;
  // associates newly created wrapper function with actually called function
  std::map<const Function*, const Function*> SyncWrappers;
  std::set<Function*> LibFuncs;
  std::map<Function*, std::string> OldLibFuncs;
  std::map<const Function*, Function*> OutsideFns;
  std::vector<const Function*> OldOutsideFnsList;
  std::vector<const Function*> OutsideFnsList;


  ///////////////////////////////_struct_related_//////////////////////////////
  std::map<StructType*, StructAttributes> StructColors;
  std::map<Type*, Type*> StructMap;


  ////////////////////temprory_var_to_GV modification//////////////////////

  std::vector<GlobalVariable*> globals_vect;
  StructType *globals_type;
  StructType *new_globals_type;
  void mutate_user_type(User *U, std::set<User*>* changed);
  std::set<const Value*> global_strs;
};
