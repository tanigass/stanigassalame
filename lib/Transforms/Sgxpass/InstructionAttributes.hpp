#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif

#include "llvm/IR/Function.h"

using namespace llvm;
class InstructionAttributes final {
public:
  // constuctors/destructors
  InstructionAttributes();
  InstructionAttributes(const Function* F, int AnnotationNum);
  
  // public methods
  bool is_equal(InstructionAttributes i_attr);
  
  //return false if new constraints are added to dependancy
  bool add(InstructionAttributes i_attr);

public:
  // private members
  std::vector<bool> dependancy;
  bool root;
};



