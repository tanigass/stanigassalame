#include "FunctionAttributes.hpp"

// constuctors/destructors
FunctionAttributes::FunctionAttributes()  {
  terminated = true;
  correctness = true;
  bb_stabilized = true;
  isMalloc = false;
  hasExternalSafe = false;
}
FunctionAttributes::FunctionAttributes(const Function* F, int AnnotationNum)
  : ret(F->arg_size()+AnnotationNum, false)
{
  call.emplace(F->arg_size()+AnnotationNum, false);
  terminated = true;
  correctness = true;
  bb_stabilized = true;
  isMalloc = false;
  side_effect = false;
  hasExternalSafe = false;
}

//-------------------------------
// FunctionAttributes::stabilize
//-------------------------------
// copy tmp to constraints and clear tmp
// return true if stabilized
bool FunctionAttributes::stabilize(){
  bool result = true;
  for (auto &v : constraints){
    if(tmp.find(v.first) != tmp.end()){
      if(!(v.second.is_equal(tmp[v.first]))){
	result = false;
	constraints[v.first] = tmp[v.first];
	//constraints[v.first].add(tmp[v.first]);
      }
    }
  }
  tmp.clear();
  return result;
}

//-------------------------------
// FunctionAttributes::fast_stabilize
//-------------------------------
// add tmp to constraints
// return true if there is no new constraints added to constraints
bool FunctionAttributes::fast_stabilize(){
  for (auto &v : tmp){
    if(constraints.find(v.first) != constraints.end()){
      if(!(v.second.is_equal(constraints[v.first]))){
	if(!constraints[v.first].add(tmp[v.first])){
	  return false;
	}
      }
    }
  }
  return true;
}
