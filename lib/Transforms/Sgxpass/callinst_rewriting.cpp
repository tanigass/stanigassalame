#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif
#include "Sgxpass.hpp"

#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Verifier.h"

using namespace llvm;

//---------------------------------
// Sgxpass::get_new_outside_fn
//---------------------------------

// Create new outiside function with modified type
Function* Sgxpass::get_new_outside_fn(Function* F) {
  if (OutsideFns.find(F) != OutsideFns.end()) {
    return OutsideFns[F];
  }
  OutsideFns[F] = nullptr;
  ValueToValueMapTy VMap;
  std::vector<std::string> vect(F->arg_size(), UNTRUSTED);
  Function* new_F = CloneFunction_attr(vect, F, VMap);
  std::string fname = F->getName().str();
  F->setName("old."+fname);
  new_F->setName(fname);
  OutsideFns[F] = new_F;

  // SyncWrapper functions already has its color defined
  if (SyncWrappers.find(new_F) == SyncWrappers.end()) {
    FunctionColors[new_F] = UNTRUSTED;
  }

  // in case of monochromatic function add the newly created function to MonochromaticFuncs
  if (auto it = find(MonochromaticFuncs.begin(), MonochromaticFuncs.end(), F);
      it != MonochromaticFuncs.end()) {
    MonochromaticFuncs.push_back(new_F);
    FunctionColors[new_F] = "";
  } else {
    OldOutsideFnsList.push_back(F);
    OutsideFnsList.push_back(new_F);
  }

  return new_F;
}

//---------------------------------
// Sgxpass::add_new_fn
//---------------------------------

Function* Sgxpass::add_new_fn(Function* F, std::vector<std::string> vect) {
  if (OutsideFns.find(F) != OutsideFns.end()) {
    return OutsideFns[F];
  }
  if (CallFns.find(F) == CallFns.end()) {
    CallFns[F] = CallinstFn();
  }
  Function* new_F = CallFns[F].get_function(vect);
  if (new_F != nullptr) {
    return new_F;
  }

  ValueToValueMapTy VMap;
  new_F = CloneFunction_attr(vect, F, VMap);
  return new_F;
}

//---------------------------------
// Sgxpass::callinst_rewrite
//---------------------------------

// clone_attr all libfuncs for untrusted arg vect and change the clone name to original name
void Sgxpass::callinst_rewrite(Module& M) {
  for (Function *F : LibFuncs) {
    if (RewriteAttrs.find(F) != RewriteAttrs.end() ||
	F->getInstructionCount() == 0) {
      continue;
    }

    std::string fname = F->getName().str();
    F->setName("old."+fname);
    std::vector<std::string> vect(F->arg_size(), "");
    Function* F_new = add_new_fn(F, vect);
    F_new->setName(fname);
    LibFuncs.emplace(F_new);
    RewriteAttrs[F_new].Colors.emplace(UNTRUSTED);
    RewriteAttrs[F_new].master = UNTRUSTED;
  }

  // changing rewriteattributes after finishing all the cloneattr
  for (auto &p : InterfaceFuncs) {
    // we change rewriteattributes of these functions
    // it will only be executed in col
    Function *F = p.first;
    std::string col = p.second;
    std::vector<std::string> vect(F->arg_size(), "");
    RewriteAttr rew = RewriteAttr(AnnotationNum, Annotations, vect);
    rew.Colors.emplace(col);
    rew.master = col;
    RewriteAttrs[F] = rew;
  }
}

//---------------------------------
// Sgxpass::update_side_effect
//---------------------------------

void Sgxpass::update_side_effect(Module& M) {
  for (Function &F : M) {
    if ((F.getInstructionCount() == 0) &&
	!((F.getName().str().find("llvm.dbg.") == 0) ||
	  (F.getName().str().find(".annotation") != std::string::npos))) {
      if (!Funcs[&F].side_effect) {
	side_effect_stabilised = false;
	Funcs[&F].side_effect = true;
      }
      continue;
    }
    // explicitly indicates that llvm.dbg and llvm.*.annotation
    // function as non side effect functions
    // we avoid syncronizations for these functions
    if ((F.getInstructionCount() == 0) &&
	((F.getName().str().find("llvm.dbg.") == 0) ||
	 (F.getName().str().find(".annotation") != std::string::npos))) {
      if (Funcs[&F].side_effect) {
	side_effect_stabilised = false;
	Funcs[&F].side_effect = false;
      }
      continue;
    }
    for (BasicBlock &B : F) {
      for (Instruction &I : B) {
	StoreInst *SI = dyn_cast<StoreInst>(&I);
	if (SI != nullptr) {
	  Value* dest = SI->getPointerOperand();
	  // store towards a untrusted value
	  if (Values.find(dest) == Values.end()) {
	    if (!Funcs[&F].side_effect) {
	      side_effect_stabilised = false;
	      Funcs[&F].side_effect = true;
	      continue;
	    }
	  }
	}
	CallInst *CI = dyn_cast<CallInst>(&I);
	if (CI != nullptr && CI->getCalledFunction()) {
	  Function *CF = CI->getCalledFunction();
	  if (Funcs[CF].side_effect || side_effect_function(CF)) {
	    if (!Funcs[&F].side_effect) {
	      side_effect_stabilised = false;
	      Funcs[&F].side_effect = true;
	      continue;
	    }
	  }
	}
      }
    }
  }
}


//---------------------------------
// Sgxpass::create_function_vect
//---------------------------------
// Update field VectFuncs with new functions created for callinst
// These functions will be used to update Global Variable color according to spawn
void Sgxpass::create_function_vect(Module& M) {
  // If there is a side effect function call
  // where called function and caller function have no common color
  // We create a sync_wrapper to ensure syncronization with wait and continue
  // We check this after cloning function according to argument color in CloneFunction_attr.cpp
  // a void sync_wrapper function is created and it will be updated in appropriate color file
  // sync_wrapper function is added only to the master color of called function
  // the sync_wrapper function is added to Vectfuncs
  for (auto &p : RewriteAttrs) {
    for (const BasicBlock &B : *p.first) {
      for (const Instruction &I : B) {
	const CallInst *CI = dyn_cast<CallInst>(&I);
	if (CI != nullptr) {
	  const Function *CF = CI->getCalledFunction();

	  //if (Funcs[CF].side_effect && !has_common_col(p.first, CF)) {
	  if (RewriteAttrs.find(CF) != RewriteAttrs.end()) {
	    if (auto it = find(MonochromaticFuncs.begin(), MonochromaticFuncs.end(), CF);
		it != MonochromaticFuncs.end()) {
	      continue;
	    }
	    if (!has_common_col(p.first, CF)) {
	      Function *new_Fn;
	      // If the function is a Outside function it will be cloned using its native name
	      // So no suffix color will be added
	      if (auto it = find(OutsideFnsList.begin(), OutsideFnsList.end(), CF);
		  it != OutsideFnsList.end()) {
		new_Fn = M.getFunction("sync_"+p.first->getName().str()+
				       "_with_"+CF->getName().str());
	      } else {
		new_Fn = M.getFunction("sync_"+p.first->getName().str()+
				       "_with_"+CF->getName().str()+"_"+
				       RewriteAttrs[CF].master);
	      }
	      if (new_Fn != nullptr) {
		continue;
	      }
	      LLVMContext &context = M.getContext();
	      IRBuilder<> builder(context);
	      // functype : void ()
	      FunctionType *functype = FunctionType::get(builder.getVoidTy(), false);
	      // Create the wrapper function
	      Function *new_F;
	      // If the function is a Outside function it will be cloned using its native name
	      // So no suffix color will be added
	      if (auto it = find(OutsideFnsList.begin(), OutsideFnsList.end(), CF);
		  it != OutsideFnsList.end()) {
		new_F = Function::Create(functype, Function::ExternalLinkage,
					 "sync_"+p.first->getName().str()+
					 "_with_"+CF->getName().str());
	      } else {
		new_F = Function::Create(functype, Function::ExternalLinkage,
					 "sync_"+p.first->getName().str()+
					 "_with_"+CF->getName().str()+"_"+
					 RewriteAttrs[CF].master, M);
	      }
	      RewriteAttrs[CF].sync_wrapper[p.first] = new_F;
	      RewriteAttrs[CF].rev_sync_wrapper[new_F] = p.first;
	      SyncWrappers[new_F] = CF;
	      FunctionColors[new_F] = RewriteAttrs[CF].master;
	      VectFuncs.emplace(new_F);
	    }
	  }
	}
      }
    }
  }

  for (const auto &p : RewriteAttrs) {
    VectFuncs.emplace(p.first);
  }
}
