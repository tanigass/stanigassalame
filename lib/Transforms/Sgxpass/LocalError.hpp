#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif

#include <sstream>
enum class ErrorType { PropagatedError, AnnotationError, ArgumentError, FunctionError, ArgCombinationError, ArgAnnotationError, DefaultError, ReturnError, FunctionBBError, StoreError, CallInstStoreError };


class LocalError final {
public:
  LocalError(ErrorType type) : _type(type) {}
 
  template <typename Arg> auto operator<<(Arg&& arg) -> LocalError& {
    std::ostringstream oss;
    oss << _str << std::forward<Arg>(arg);
    _str = oss.str();
    return *this;
  }  
  
  ErrorType type() const { return _type; } 
  const std::string& str() const { return _str; }
  
private:
  ErrorType _type;
  std::string _str;
};


inline auto operator<(const LocalError& lhs, const LocalError& rhs) -> bool {
  if (lhs.type() == rhs.type()) {
    return lhs.str() < rhs.str();
  }
  return lhs.type() < rhs.type();
}

