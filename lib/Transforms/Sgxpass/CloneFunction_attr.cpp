//===- CloneFunction.cpp - Clone a function into another function ---------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file implements the CloneFunctionInto interface, which is used as the
// low-level function cloner.  This is used by the CloneFunction and function
// inliner to do the dirty work of copying the body of a function around.
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Analysis/ConstantFolding.h"
#include "llvm/Analysis/DomTreeUpdater.h"
#include "llvm/Analysis/InstructionSimplify.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DebugInfo.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/Module.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
#include <map>

#include "Sgxpass.hpp"

using namespace llvm;

// Return true if the Instruction needs to be syncronized
bool Sgxpass::sync_needed(const Instruction* I) {
  const CallInst *CI = dyn_cast<CallInst>(I);
  if (CI != nullptr) {
    if (auto it = find(MonochromaticFuncs.begin(), MonochromaticFuncs.end(),
		       CI->getCalledFunction());
	it != MonochromaticFuncs.end()) {
      return true;
    }
  }
  return false;
}

// update BBAttributes associated with BasicBlocks with follows the branchinst
// place the inst* sync in needSync
// and update the int pointed by needSync if there is an instruction which needs sync
void Sgxpass::update_BBWhileCase(BasicBlock *B, Function *F, int *sync) {
  BranchInst* BI = dyn_cast<BranchInst>(B->getTerminator());

  if (BI == nullptr) {
    return;
  }

  for (auto *CB : BI->successors()) {
    if (BBColor.find(CB) != BBColor.end()) {
      if (BBColor[CB].whileCase) {
	continue;
      } else {
	BBColor[CB].whileCase = true;
	BBColor[CB].needSync = sync;
	for (Instruction &I : *B) {
	  if (sync_needed(&I)) {
	    *sync = 1;
	    break;
	  }
	}
      }
    }
    update_BBWhileCase(CB, F, sync);
  }
  return;
}

void Sgxpass::update_BBAttributes(Function *F, BasicBlock *root) {
  // If the basic block is colored and whileCase
  // No need to continue because all following blocks will be whileCase
  if (BBColor.find(root) != BBColor.end()) {
    if (BBColor[root].whileCase) {
      return;
    }
  }

  BranchInst* BI = dyn_cast<BranchInst>(root->getTerminator());
  if (Insts.find(BI) != Insts.end()) {
    if ((Insts[BI].compare("") != 0) &&
	(Insts[BI].compare(UNTRUSTED) != 0)) {
      // If the BranchInst BI is a BranchInst of a while case and colored
      // THe following basicblock is colored so they are already placed in BBColor
      // update BBAttributes associated with BasicBlocks with follows the branchinst
      if (is_while_case(root, root, F)) {
	NeedSyncVar[BI] = 0;
	update_BBWhileCase(root, F, &(NeedSyncVar[BI]));
	return;
      }
    }
  }

  DominatorTree DT = DominatorTree (*F);
  auto *DTN = DT.getNode (root);
  auto &Children = DTN->getChildren();
  for (auto N : Children) {
    BasicBlock* CB = N->getBlock();
    update_BBAttributes(F, CB);
  }
  return;
}

//get callInst argument information as an annotation vector
std::vector<std::string> Sgxpass::get_arg_vect(const CallInst *CI, const Function* old_F,
					       std::vector<std::string> vect) {
  std::vector<std::string> arg_annot;
  unsigned i=0;
  for (const auto* arg = CI->arg_begin(); arg != CI->arg_end(); arg++ ) {
    // To get arg_annot same argsize as CallInst
    arg_annot.push_back("");

    Argument* A = dyn_cast<Argument>(arg);
    if (A != nullptr) {
      if (vect[A->getArgNo()].compare("") != 0) {
	arg_annot[i] = vect[A->getArgNo()];
      }
    }
    Value* V = dyn_cast<Value>(arg);
    if (V != nullptr) {
      if (Values.find(V) != Values.end()) {
	arg_annot[i] = Values[V];
      } else {
	Instruction* Ia = dyn_cast<Instruction>(arg);
	if (Ia != nullptr) {
	  for (unsigned j=0; j<old_F->arg_size(); j++) {
	    if (Funcs[old_F].constraints[Ia].dependancy[j]) {
	      if (vect[j].compare("") != 0) {
		arg_annot[i] = vect[j];
	      }
	    }
	  }
	  for (unsigned j=old_F->arg_size(); j<Funcs[old_F].constraints[Ia].dependancy.size(); j++) {
	    if (Funcs[old_F].constraints[Ia].dependancy[j]) {
	      arg_annot[i] = Annotations[j-old_F->arg_size()];
	    }
	  }
	}
      }
    }
    i++;
  }
  return arg_annot;
}

std::string Sgxpass::get_color_inst(std::vector<bool> dependancy, std::vector<std::string> vect) {
  if (dependancy.size() == 0) {
    return "";
  }

  // if the instruction depends on any color return the color
  for (unsigned i=vect.size(); i<dependancy.size(); ++i) {
    if (dependancy[i]) {
      return Annotations[i-vect.size()];
    }
  }

  // return the color if the depending argument is colored
  for (unsigned i=0; i<vect.size(); ++i) {
    if (dependancy[i] && vect[i].compare("") != 0) {
      return vect[i];
    }
  }

  return "";
}

BasicBlock *Sgxpass::CloneBasicBlock_attr(std::vector<std::string> vect,
					  const BasicBlock *BB, ValueToValueMapTy &VMap,
					  const Twine &NameSuffix, Function *F,
					  ClonedCodeInfo *CodeInfo,
					  DebugInfoFinder *DIFinder) {
  DenseMap<const MDNode *, MDNode *> Cache;
  BasicBlock *NewBB = BasicBlock::Create(BB->getContext(), "", F);
  if (BB->hasName())
    NewBB->setName(BB->getName() + NameSuffix);
 
  bool hasCalls = false, hasDynamicAllocas = false;
  Module *TheModule = F ? F->getParent() : nullptr;
 
  // Loop over all instructions, and copy them over.
  for (const Instruction &I : *BB) {
    if (DIFinder && TheModule)
      DIFinder->processInstruction(*TheModule, I);
 
    Instruction *NewInst = I.clone();
    if (I.hasName())
      NewInst->setName(I.getName() + NameSuffix);
    NewBB->getInstList().push_back(NewInst);
    VMap[&I] = NewInst; // Add instruction map to value.
 
    hasCalls |= (isa<CallInst>(I) && !isa<DbgInfoIntrinsic>(I));
    if (const AllocaInst *AI = dyn_cast<AllocaInst>(&I)) {
      if (!AI->isStaticAlloca()) {
	hasDynamicAllocas = true;
      }
    }

    // Change CalledFunction of the CallInst to adequate Function
    // according to argument's colors
    const Function* old_F = BB->getParent();
    const CallInst* CI = dyn_cast<CallInst>(&I);
    if (CI != nullptr && CI->getCalledFunction()) {
      bool skip = false;
      if (auto it = find(IgnoredFuncs.begin(), IgnoredFuncs.end(), CI->getCalledFunction());
	  it != IgnoredFuncs.end()) {
	std::vector<std::string> arg_annot = std::vector<std::string>(CI->getCalledFunction()->arg_size(), UNTRUSTED);
	Function* new_F = add_new_fn(CI->getCalledFunction(), arg_annot);
	dyn_cast<CallInst>(NewInst)->setCalledFunction(new_F);
	skip = true;
      } else if (ExternalSafeFuncs.find(CI->getCalledFunction()) != ExternalSafeFuncs.end()) {
	std::string BB_color = get_color_inst(Funcs[old_F].BBconstraints[BB].dependancy, vect);
	Function* get_CF = ExternalSafeFuncs[CI->getCalledFunction()].get_function(BB_color);
	// instead of cloning the called function we clone_attr the interface function
	// this part is to automatically treat the wrapper which is inside the interface function
	// the rewriteattributes of interface function will be change afterwards
	if (get_CF != nullptr) {
	  std::vector<std::string> arg_annot;
	  if (ExternalSafeFuncs_wrap.find(CI->getCalledFunction()) != ExternalSafeFuncs_wrap.end()) {
	    arg_annot = get_arg_vect(CI, old_F, vect);
	  } else {
	    arg_annot = std::vector<std::string>(CI->getCalledFunction()->arg_size(), "");
	  }
	  Function* new_F = add_new_fn(get_CF, arg_annot);
	  dyn_cast<CallInst>(NewInst)->setCalledFunction(new_F);
	  InterfaceFuncs[new_F] = BB_color;
	  skip = true;
	}
      }

      if (!skip) {
	std::vector<std::string> arg_annot = get_arg_vect(CI, old_F, vect);
	if (CI->getCalledFunction()->getInstructionCount() != 0) {
	  Function* new_F = add_new_fn(CI->getCalledFunction(), arg_annot);
	  dyn_cast<CallInst>(NewInst)->setCalledFunction(new_F);
	}
      }
    }

    Funcs[F].constraints[NewInst] = Funcs[old_F].constraints[&I];
    std::string inst_color = get_color_inst(Funcs[F].constraints[NewInst].dependancy, vect);

    // to add the color to the instructions like "llvm.ptr.annotation"
    if (inst_color.compare("") == 0) {
      if (Insts[&I].compare("") != 0) {
	inst_color = Insts[&I];
      }
    }

    if (inst_color.compare("") != 0) {
      Insts[NewInst] = inst_color;
      auto pos = AnnotationList.find(Insts[NewInst]);
      int position = std::distance(AnnotationList.begin(), pos);
      RewriteAttrs[F].master_table[position] = RewriteAttrs[F].master_table[position]+1;
    }
  }

  if (CodeInfo) {
    CodeInfo->ContainsCalls          |= hasCalls;
    CodeInfo->ContainsDynamicAllocas |= hasDynamicAllocas;
  }
  return NewBB;
}

// Clone OldFunc into NewFunc, transforming the old arguments into references to
// VMap values.
//
void Sgxpass::CloneFunctionInto_attr(std::vector<std::string> vect,
				     Function *NewFunc, const Function *OldFunc,
				     ValueToValueMapTy &VMap,
				     bool ModuleLevelChanges,
				     SmallVectorImpl<ReturnInst*> &Returns,
				     const char *NameSuffix, ClonedCodeInfo *CodeInfo,
				     ValueMapTypeRemapper *TypeMapper,
				     ValueMaterializer *Materializer) {
  assert(NameSuffix && "NameSuffix cannot be null!");
 
#ifndef NDEBUG
  for (const Argument &I : OldFunc->args())
    assert(VMap.count(&I) && "No mapping from source argument specified!");
#endif
 
  // Copy all attributes other than those stored in the AttributeList.  We need
  // to remap the parameter indices of the AttributeList.
  AttributeList NewAttrs = NewFunc->getAttributes();
  NewFunc->copyAttributesFrom(OldFunc);
  NewFunc->setAttributes(NewAttrs);

  // Fix up the personality function that got copied over.
  if (OldFunc->hasPersonalityFn())
    NewFunc->setPersonalityFn(
			      MapValue(OldFunc->getPersonalityFn(), VMap,
				       ModuleLevelChanges ? RF_None : RF_NoModuleLevelChanges,
				       TypeMapper, Materializer));
 
  SmallVector<AttributeSet, 4> NewArgAttrs(NewFunc->arg_size());
  AttributeList OldAttrs = OldFunc->getAttributes();

  // Clone any argument attributes that are present in the VMap.
  for (const Argument &OldArg : OldFunc->args()) {
    if (Argument *NewArg = dyn_cast<Argument>(VMap[&OldArg])) {
      NewArgAttrs[NewArg->getArgNo()] =
	OldAttrs.getParamAttributes(OldArg.getArgNo());
    }
  }

  NewFunc->setAttributes(
			 AttributeList::get(NewFunc->getContext(), OldAttrs.getFnAttributes(),
					    OldAttrs.getRetAttributes(), NewArgAttrs));
 
  bool MustCloneSP =
    OldFunc->getParent() && OldFunc->getParent() == NewFunc->getParent();
  DISubprogram *SP = OldFunc->getSubprogram();
  if (SP) {
    assert(!MustCloneSP || ModuleLevelChanges);
    // Add mappings for some DebugInfo nodes that we don't want duplicated
    // even if they're distinct.
    auto &MD = VMap.MD();
    MD[SP->getUnit()].reset(SP->getUnit());
    MD[SP->getType()].reset(SP->getType());
    MD[SP->getFile()].reset(SP->getFile());
    // If we're not cloning into the same module, no need to clone the
    // subprogram
    if (!MustCloneSP)
      MD[SP].reset(SP);
  }
 
  SmallVector<std::pair<unsigned, MDNode *>, 1> MDs;
  OldFunc->getAllMetadata(MDs);
  for (auto MD : MDs) {
    NewFunc->addMetadata(
			 MD.first,
			 *MapMetadata(MD.second, VMap,
				      ModuleLevelChanges ? RF_None : RF_NoModuleLevelChanges,
				      TypeMapper, Materializer));
  }
 
  // When we remap instructions, we want to avoid duplicating inlined
  // DISubprograms, so record all subprograms we find as we duplicate
  // instructions and then freeze them in the MD map.
  // We also record information about dbg.value and dbg.declare to avoid
  // duplicating the types.
  DebugInfoFinder DIFinder;
 
  // Loop over all of the basic blocks in the function, cloning them as
  // appropriate.  Note that we save BE this way in order to handle cloning of
  // recursive functions into themselves.
  //
  for (Function::const_iterator BI = OldFunc->begin(), BE = OldFunc->end();
       BI != BE; ++BI) {
    const BasicBlock &BB = *BI;
 
    // Create a new basic block and copy instructions into it!
    BasicBlock *CBB = CloneBasicBlock_attr(vect, &BB, VMap, NameSuffix, NewFunc, CodeInfo,
					   ModuleLevelChanges ? &DIFinder : nullptr);

    Funcs[NewFunc].BBconstraints[CBB] = Funcs[OldFunc].BBconstraints[&BB];
    std::string BB_color = get_color_inst(Funcs[NewFunc].BBconstraints[CBB].dependancy, vect);
    // BB_color is updated for newly created function with BB's exact color depending on vect
    if (BB_color.compare("") != 0) {
      BBColor[CBB] = BBAttributes();
      BBColor[CBB].color = BB_color;
    }
    // Add basic block mapping.
    VMap[&BB] = CBB;
 
    // It is only legal to clone a function if a block address within that
    // function is never referenced outside of the function.  Given that, we
    // want to map block addresses from the old function to block addresses in
    // the clone. (This is different from the generic ValueMapper
    // implementation, which generates an invalid blockaddress when
    // cloning a function.)
    if (BB.hasAddressTaken()) {
      Constant *OldBBAddr = BlockAddress::get(const_cast<Function*>(OldFunc),
					      const_cast<BasicBlock*>(&BB));
      VMap[OldBBAddr] = BlockAddress::get(NewFunc, CBB);
    }
 
    // Note return instructions for the caller.
    if (ReturnInst *RI = dyn_cast<ReturnInst>(CBB->getTerminator()))
      Returns.push_back(RI);
  }
 
  for (DISubprogram *ISP : DIFinder.subprograms())
    if (ISP != SP)
      VMap.MD()[ISP].reset(ISP);
 
  for (DICompileUnit *CU : DIFinder.compile_units())
    VMap.MD()[CU].reset(CU);
 
  for (DIType *Type : DIFinder.types())
    VMap.MD()[Type].reset(Type);
 
  // Loop over all of the instructions in the function, fixing up operand
  // references as we go.  This uses VMap to do all the hard work.
  for (Function::iterator BB =
	 cast<BasicBlock>(VMap[&OldFunc->front()])->getIterator(),
	 BE = NewFunc->end();
       BB != BE; ++BB)
    // Loop over all instructions, fixing each one as we find it...
    for (Instruction &II : *BB)
      RemapInstruction(&II, VMap,
		       ModuleLevelChanges ? RF_None : RF_NoModuleLevelChanges,
		       TypeMapper, Materializer);
 
  // Register all DICompileUnits of the old parent module in the new parent module
  auto* OldModule = OldFunc->getParent();
  auto* NewModule = NewFunc->getParent();
  if (OldModule && NewModule && OldModule != NewModule && DIFinder.compile_unit_count()) {
    auto* NMD = NewModule->getOrInsertNamedMetadata("llvm.dbg.cu");
    // Avoid multiple insertions of the same DICompileUnit to NMD.
    SmallPtrSet<const void*, 8> Visited;
    for (auto* Operand : NMD->operands())
      Visited.insert(Operand);
    for (auto* Unit : DIFinder.compile_units())
      // VMap.MD()[Unit] == Unit
      if (Visited.insert(Unit).second)
	NMD->addOperand(Unit);
  }
}
 
/// Return a copy of the specified function and add it to that function's
/// module.  Also, any references specified in the VMap are changed to refer to
/// their mapped value instead of the original one.  If any of the arguments to
/// the function are in the VMap, the arguments are deleted from the resultant
/// function.  The VMap is updated to include mappings from all of the
/// instructions and basicblocks in the function from their old to new values.
///
Function *Sgxpass::CloneFunction_attr(std::vector<std::string> vect,
				      Function *F, ValueToValueMapTy &VMap,
				      ClonedCodeInfo *CodeInfo) {
  std::vector<Type*> ArgTypes;
 
  // The user might be deleting arguments to the function by specifying them in
  // the VMap.  If so, we need to not add the arguments to the arg ty vector
  //
  // Change type while creating the new function type with required type
  for (const Argument &I : F->args())
    if (VMap.count(&I) == 0) // Haven't mapped the argument to anything yet?
      {
	if (OutsideFns.find(F) != OutsideFns.end()) {
	  ArgTypes.push_back(get_changed_type(I.getType()));
	} else {
	  ArgTypes.push_back(I.getType());
	}
      }

  // Create a new function type...
  FunctionType *FTy;
  // create modified function type if it is a outside function
  if (OutsideFns.find(F) != OutsideFns.end()) {
    FTy = FunctionType::get(get_changed_type(F->getFunctionType()->getReturnType()),
			    ArgTypes, F->getFunctionType()->isVarArg());
  } else {
    FTy = FunctionType::get(F->getFunctionType()->getReturnType(),
			    ArgTypes, F->getFunctionType()->isVarArg());
  }

  // Create the new function...
  Function *NewF = Function::Create(FTy, F->getLinkage(), F->getAddressSpace(),
				    F->getName(), F->getParent());

  SmallVector<ReturnInst*, 8> Returns;  // Ignore returns cloned.

  // Loop over the arguments, copying the names of the mapped arguments over...
  Function::arg_iterator DestI = NewF->arg_begin();
  for (const Argument & I : F->args())
    if (VMap.count(&I) == 0) {     // Is this argument preserved?
      DestI->setName(I.getName()); // Copy the name over...
      VMap[&I] = &*DestI++;        // Add mapping to VMap
    }

  if (OutsideFns.find(F) != OutsideFns.end()) {
    // Clone the function and return it without treating fattr
    CloneFunctionInto(NewF, F, VMap, true, Returns);
    FunctionAttributes fattr = FunctionAttributes(NewF, AnnotationNum);
    Funcs.emplace(std::make_pair(NewF, fattr));
    CallFns[F].add_function(vect, NewF);
    RewriteAttr rew = RewriteAttr(AnnotationNum, Annotations, vect);
    rew.Colors.emplace(UNTRUSTED);
    rew.master = UNTRUSTED;
    RewriteAttrs[NewF] = rew;
    RewriteAttrs[F] = rew;
    return NewF;
  }

  FunctionAttributes fattr = FunctionAttributes(NewF, AnnotationNum);
  Funcs.emplace(std::make_pair(NewF, fattr));
  CallFns[F].add_function(vect, NewF);

  // Collect rewriting related information for the new function
  RewriteAttr rew = RewriteAttr(AnnotationNum, Annotations, vect);
  RewriteAttrs[NewF] = rew;

  CloneFunctionInto_attr(vect, NewF, F, VMap, F->getSubprogram() != nullptr, Returns, "",
			 CodeInfo);

  // Update rewriting related information for the new function
  RewriteAttrs[NewF].set_master();
  RewriteAttrs[NewF].set_colors();
  update_BBAttributes(NewF, &(NewF->getEntryBlock()));
  return NewF;
}
