#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif

#include "llvm/IR/Value.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/Operator.h"

using namespace llvm;
unsigned int get_num_GlobalVariable(const GlobalVariable& G);
std::string get_annotation(const GlobalVariable& G, unsigned int i);
Value* get_GlobalVariable(const GlobalVariable& G, unsigned int i);
std::string get_annotation(const CallInst& CI);
Value* get_Value(const CallInst& CI);
bool is_parent_of(BasicBlock* PBB, BasicBlock* CBB);
Function* get_fn_from_GV(const GlobalVariable& G);
