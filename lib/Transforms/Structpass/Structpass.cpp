#include "Structpass.hpp"
#include <iostream>

//-------------------------------------
// Structpass::initialize_StructColors
//-------------------------------------

// Rename all struct with "old." in prefix
// Create new struct with old name
// Initialize StructAttributes with "" for colors and old structs elements for elt_tys 
void Structpass::initialize_StructColors(Module &M) {
  auto STs = M.getIdentifiedStructTypes();
  LLVMContext &context = M.getContext();
  for (StructType* ST : STs) {
    std::string name = ST->getName().str();
    std::string new_name = "old." + ST->getName().str();
    ST->setName(new_name);
    StructType* new_ST = StructType::create(context, name);
    StructAttributes attr(ST, new_ST);
    StructColors.emplace(std::make_pair(ST, attr));
    StructMap.emplace(std::make_pair(ST, new_ST));
    //StructMap.emplace(std::make_pair(ST, ST));
  }
}

//-------------------------------------
// Structpass::update_structs
//-------------------------------------

void Structpass::update_elt_tys(Module &M) {
  for (auto it : StructColors) {
    auto ST = it.second.ST;
    auto new_ST = it.second.new_ST;
    for (unsigned i=0; i<ST->getNumElements(); ++i) {
      if (it.second.colors[i].compare("") != 0) {
	std::string new_name = new_ST->getName().str() + "."+ std::to_string(i);
	StructType* elt_ty = StructType::create(M.getContext(), new_name);
	elt_ty->setBody(get_changed_type(ST->getElementType(i)));
	it.second.elt_tys.push_back(elt_ty->getPointerTo());
      } else {
	it.second.elt_tys.push_back(get_changed_type(ST->getElementType(i)));
      }
    }
    // Create new struct with struct pointer towards colored elements
    // before
    // struct old.A {
    //   int a color(blue);
    //   int b;
    // }
    // after
    // struct A {
    //   struct A.0* a;
    //   int b;
    // }
    // struct A.0 {
    //   int a color(blue);
    // }
    new_ST->setBody(it.second.elt_tys);
  }
}


//---------------------------------
// Structpass::testing
//---------------------------------

void Structpass::testing(Module &M) {
  errs() << "\n\n\n";
  for (auto elt : StructColors) {
    elt.second.print();
  }
  errs() << "\n\n\n";
  auto STs = M.getIdentifiedStructTypes();
  for (StructType* ST : STs) {
    errs() << *ST << "\n";
  }
  errs() << "\n\n\n";
}

//---------------------------------
// Structpass::get_changed_type
//---------------------------------

// update struct body according to it's color type
Type* Structpass::get_changed_type(Type *src) {
  if (src == nullptr) {
    return src;
  }
  int count = 0;
  Type* ty = src;
  while (ty->isPointerTy()) {
    ty=dyn_cast<PointerType>(ty)->getElementType();
    ++count;
  }

  if ((!ty->isStructTy()) && (!ty->isFunctionTy()) && (!ty->isArrayTy()) && (!ty->isVectorTy())) {
    return src;
  }
  Type* new_ty;
  // this part create a new functiontype, ArrayType, VectorType according to new struct
  if (ty->isFunctionTy()) {
    FunctionType* fty = dyn_cast<FunctionType>(ty);
    std::vector<Type* > argTys;
    for (unsigned i=0; i<fty->getNumParams(); ++i) {
      argTys.push_back(get_changed_type(fty->getParamType(i)));
    }
    new_ty = FunctionType::get(get_changed_type(fty->getReturnType()), argTys, fty->isVarArg());
  } else if (ty->isArrayTy()) {
    new_ty = ArrayType::get(get_changed_type(ty->getArrayElementType()), ty->getArrayNumElements());
  } else if (ty->isVectorTy()) {
    new_ty = VectorType::get(get_changed_type(ty->getVectorElementType()), ty->getVectorNumElements(), dyn_cast<VectorType>(ty)->isScalable());
  } else if (ty->isStructTy()) {
    if (StructColors.find(dyn_cast<StructType>(ty)) == StructColors.end()) {
      return src;
    }
    new_ty = StructColors[dyn_cast<StructType>(ty)].new_ST;
  }
  while (count != 0) {
    new_ty=new_ty->getPointerTo();
    --count;
  }

  return new_ty;
}

//-------------------------------
// Structpass::treat_colored_struct
//-------------------------------

void Structpass::treat_colored_struct(const CallInst* CI, std::string color) {
  // CI : %3 = call i8* @llvm.ptr.annotation.p0i8(i8* %2, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), i32 14)
  // BI : %2 = bitcast i32* %a4 to i8*
  // GI :   %a4 = getelementptr inbounds %struct.test_color, %struct.test_color* %1, i32 0, i32 0
  // ST : %struct.test_color = type { i32, i32 }
  // GI->getOperand(2) : i32 0 or i32 2 ...
  // index : 0 or 2 ...
  BitCastInst *BI = dyn_cast<BitCastInst>(CI->getArgOperand(0));
  GetElementPtrInst *GI;
  StructType* ST;
  unsigned int index;
  if (BI == nullptr) {
    GI = dyn_cast<GetElementPtrInst>(CI->getArgOperand(0));
  } else {
    GI = dyn_cast<GetElementPtrInst>(BI->getOperand(0));
  }
  if (GI == nullptr) {
    GEPOperator *GEPO = dyn_cast<GEPOperator>(BI->getOperand(0));
    if (GEPO != nullptr) {
      if (!GEPO->getSourceElementType()->isStructTy()) {
	return;
      } else {
	ST = dyn_cast<StructType>(GEPO->getSourceElementType());
	index = dyn_cast<ConstantInt>(GEPO->getOperand(2))->getSExtValue();
      }
    } else {
      return;
    }
  } else {
    if (!GI->getSourceElementType()->isStructTy()) {
      return;
    } else {
      ST = dyn_cast<StructType>(GI->getSourceElementType());
      index = dyn_cast<ConstantInt>(GI->getOperand(2))->getSExtValue();
    }
  }
  StructColors[ST].colors[index] = color;
}

//---------------------------------
// Structpass::update_structatt_colors
//---------------------------------

// update the color member of all structattributes of StructColors
void Structpass::update_structatt_colors(Module &M){
  // Iterate over all functions of a Module
  for (Function &F : M) {
    // Iterate over all BasicBlocks of a Function
    for (BasicBlock &B : F) {
      // Iterate over all Instructions of a BasicBlock
      for (Instruction &I: B) {
	auto CI = dyn_cast<CallInst>(&I); // auto = CallInst*
	if (CI != nullptr) {
	  // check if the called function is a function pointer
	  if (!(CI->isIndirectCall())){
	    Function *CF=CI->getCalledFunction();
	    // to extract all llvm.ptr.annotation function and treat them
	    //in order to update structattr with appropriate colors
	    if (CF->getName().find("ptr.annotation") != std::string::npos) {
	      const std::string& color=get_annotation(*CI);
	      treat_colored_struct(CI, color);
	    }
	  }
	}
      }
    }
  }
}

//-------------------------------------
// Sgxpass::change_struct_getElementPtr
//-------------------------------------

//change colored struct's GetElementPtrInstructions
void Structpass::change_struct_getElementPtr(GetElementPtrInst *GI, StructType *ST, Module &M, Module &new_M) {

  // FROM
  //*GI->getSourceElementType
  //%struct.test_color = type { %struct.test_color.0*, %struct.test_color.1*, %struct.test_color.2* }
  //*GI->getType
  //i32*
  //*GI->getResultElementType
  //i32

  //%22 = load %struct.test_color*, %struct.test_color** @t_c, align 8, !dbg !105
  // GI:
  //%23 = getelementptr inbounds %struct.test_color, %struct.test_color* %22, i32 0, i32 2, !dbg !106
  //%24 = bitcast i32* %23 to i8*, !dbg !106
  //%25 = call i8* @llvm.ptr.annotation.p0i8(i8* %24, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.6, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), i32 31), !dbg !106

  // TO
  //*GI->getSourceElementType
  //%struct.test_color = type { %struct.test_color.0*, %struct.test_color.1*, %struct.test_color.2* }
  //*GI->getType
  //%struct.test_color.2**
  //*GI->getResultElementType
  //%struct.test_color.2*

  //%24 = load %struct.test_color*, %struct.test_color** @t_c, align 8, !dbg !105
  // GI:
  //%25 = getelementptr inbounds %struct.test_color, %struct.test_color* %24, i32 0, i32 2, !dbg !106
  //%26 = load %struct.test_color.2*, %struct.test_color.2** %25, !dbg !106
  //%27 = getelementptr inbounds %struct.test_color.2, %struct.test_color.2* %26, i32 0, i32 0, !dbg !106
  //%28 = bitcast i32* %27 to i8*, !dbg !106
  //%29 = call i8* @llvm.ptr.annotation.p0i8(i8* %28, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.6, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), i32 31), !dbg !106

  LLVMContext& context=ST->getContext();
  std::string old_name = "old." + ST->getName().str();
  unsigned int index = dyn_cast<ConstantInt>(GI->getOperand(2))->getSExtValue();
  StructType* old_ST = M.getTypeByName(old_name);
  if (StructColors.find(old_ST) == StructColors.end()) {
    return;
  }
  if (StructColors[old_ST].colors[index].compare("") != 0) {
    auto *ptr = GI->getPointerOperand();
    // Create a new_GI using GI and copy its attributes
    SmallVector<Value*, 2> gi_args;
    gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 0));
    gi_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 0));
    GetElementPtrInst *new_GI = GetElementPtrInst::CreateInBounds(ptr, gi_args);
    new_GI->setResultElementType( GI->getResultElementType());
    new_GI->setDebugLoc(GI->getDebugLoc());
    new_GI->mutateType(GI->getType());

    // Replace old call with new one.
    GI->replaceAllUsesWith(new_GI);

    // Modify GI with new struct types
    GI->mutateType(ST->getElementType(index)->getPointerTo());
    GI->setResultElementType(ST->getElementType(index));

    // Create LoadInst
    LoadInst *LI = new LoadInst(GI->getResultElementType(), GI);
    LI->setDebugLoc(GI->getDebugLoc());
    LI->insertAfter(GI);

    // Modify new_GI accordingly
    new_GI->setOperand(0,LI);
    new_GI->setSourceElementType(dyn_cast<PointerType>(ST->getElementType(index))->getElementType());
    new_GI->insertAfter(LI);
  }
  return;
}


//---------------------------
// Structpass::sort_function
//---------------------------

void Structpass::sort_function( Function* cur){
  for(Function* F : CallFuncs[cur]){
    //Function *F=CI->getCalledFunction();
    if(auto it = find(AllFuncs.begin(), AllFuncs.end(), F); it != AllFuncs.end()){
      AllFuncs.erase(it);
      sort_function(F);
    }
  }
  Sorted.push_back(cur);
  if(auto it = find(AllFuncs.begin(), AllFuncs.end(), cur); it != AllFuncs.end()){
      AllFuncs.erase(it);
  }
  return;
}

//-------------------------------
// Structpass::is_malloc
//-------------------------------

// update isMalloc of F to true if a use of CI is a returnInst
bool Structpass::is_malloc(std::string name) {
  return (name.compare("malloc") ==0);
}

//-------------------------------
// Structpass::update_malloc
//-------------------------------

// update isMalloc of F to true if a use of CI is a returnInst
void Structpass::update_isMalloc(CallInst* CI, Function* CF, Function* F) {
  if (Funcs[CF].isMalloc) {
    for (auto I : Funcs[CF].MallocInsts) {
      Funcs[F].MallocInsts.emplace(I);
    }
  }
  for (User *U : CI->users()) {
    ReturnInst* RI = dyn_cast<ReturnInst>(U);
    if ((RI != nullptr) && !Funcs[F].isMalloc) {
      Funcs[F].isMalloc = true;
      Funcs[F].MallocInsts.emplace(CI);
      stabilized = false;
    }
  }
  return;
}

//-------------------------------
// Structpass::is_struct_malloc
//-------------------------------

// return true if the malloc is then casted into colored struct
// return size of xalloc if the malloc is then casted into colored struct
// otherwise return -1
float Structpass::is_struct_malloc(CallInst* CI, Module &M, Module &old_M) {
  for (User *U : CI->users()) {
    BitCastInst* BI = dyn_cast<BitCastInst>(U);
    if (BI == nullptr) {
      return -1;
    }
    if (!BI->getDestTy()->isPointerTy()) {
      return -1;
    }
    if (BI->getDestTy()->getPointerElementType()->isStructTy()) {
      StructType* ST = dyn_cast<StructType>(BI->getDestTy()->getPointerElementType());
      std::string old_name = "old." + ST->getName().str();
      StructType* old_ST = old_M.getTypeByName(old_name);
      if (StructColors.find(old_ST) != StructColors.end()) {
	for (auto color : StructColors[old_ST].colors) {
	  if (color.compare("") != 0) {
	    auto DL = M.getDataLayout();
	    auto old_DL = old_M.getDataLayout();
	    float old_size = old_DL.getTypeStoreSize(old_ST);
	    float size = DL.getTypeStoreSize(ST);
	    return size/old_size;
	  }
	}
      }
    }
  }
  return -1;
}

//-------------------------------
// Structpass::check_malloc
//-------------------------------

// update isMalloc of F if CF is malloc
// add CI to MallocInsts if return value of CF is a malloc
void Structpass::check_malloc(CallInst* CI, Function* CF, Function* F, Module &M, Module &old_M) {
  if (is_malloc(CF->getName().str()) || Funcs[CF].isMalloc) {
    update_isMalloc(CI, CF, F);
    float res = is_struct_malloc(CI, M, old_M);
    if (res == -1) {
      return;
    }
    /*
    if (!is_struct_malloc(CI, old_M)) {
      return;
    }
    */
    if (malloc_to_xalloc(CI, M, res)) {
      CallFuncs[F].emplace(M.getFunction("xalloc"));
    }
    if (!is_malloc(CF->getName().str())) {
      for (auto CallI : Funcs[CF].MallocInsts) {
	if (malloc_to_xalloc(CallI, M, res)) {
	  CallFuncs[F].emplace(M.getFunction("xalloc"));
	}
      }
      Funcs[CF].MallocInsts.clear();
    }
  }
  return;
}

//---------------------------------
// Structpass::malloc_to_xalloc
//---------------------------------

// change malloc to xalloc
//if it is a malloc of struct with multiple colors
//return true if malloc is modified to xalloc
bool Structpass::malloc_to_xalloc(CallInst* CI, Module &M, float size) {
  Function *CF=CI->getCalledFunction();
  if (is_malloc(CF->getName().str()) &&
      (CI->getFunction()->getName().compare("xalloc") != 0)) {
    Function* xalloc = M.getFunction("xalloc");
    if (xalloc == nullptr) {
      xalloc = create_xalloc(M);
    }
    LLVMContext &context = M.getContext();
    SmallVector<Value*, 16> new_args;
    const int malloc_size = dyn_cast<ConstantInt>(CI->getOperand(0))->getSExtValue();
    float m_size = malloc_size*size;
    int m_s = static_cast<int>(m_size);

    new_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt64Ty(context), m_s));
    new_args.push_back(llvm::ConstantInt::get(llvm::Type::getInt64Ty(context), 42));

    CallInst* new_CI = CallInst::Create(xalloc, new_args, "", CI);
    new_CI->setCallingConv(CI->getCallingConv());
    new_CI->setAttributes(CI->getAttributes());
    new_CI->setDebugLoc(CI->getDebugLoc());
    // Replace old call with new one.
    CI->replaceAllUsesWith(new_CI);
    // take the old ones name
    new_CI->takeName(CI);
    // CI in StructMallocInst will be deleted from parent afterwards
    // CI->eraseFromParent();
    StructMallocInst.emplace(CI);
    return true;
  }
  return false;
}

//---------------------------------
// Structpass::create_xalloc
//---------------------------------

// create new function xalloc
Function* Structpass::create_xalloc(Module &M) {
  LLVMContext &context = M.getContext();
  IRBuilder<> builder(context);
  Function* malloc = M.getFunction("malloc");
  // Define function's signature
  std::vector<Type*> Integers(2, builder.getInt64Ty());
  auto *funcType = FunctionType::get(builder.getInt8PtrTy(), Integers, false);

  // create the function "xalloc" and bind it to the module with ExternalLinkage,
  // so we can retrieve it later
  Function* xalloc = Function::Create(funcType, Function::ExternalLinkage, "xalloc", M);
  xalloc->setDSOLocal(1);
  xalloc->setReturnDoesNotAlias ();
  // Define the entry block and fill it with an appropriate code
  auto *entry = BasicBlock::Create(context, "entry", xalloc);
  auto args = xalloc->arg_begin();
  builder.SetInsertPoint(entry);
  auto *result = builder.CreateCall(malloc, &(*args));
  builder.CreateRet(result);
  // Verify at the end
  verifyFunction(*xalloc);
  return xalloc;
}

//---------------------------
// Structpass::treat_function
//---------------------------

void Structpass::treat_function(Function* F, Module &M, Module &old_M){
  if (Funcs.find(F) == Funcs.end()) {
    FunctionAttributes f_attr=FunctionAttributes(F);
    Funcs.emplace(std::make_pair(F, f_attr));
  }
  // Iterate over all BasicBlocks of a Function
  for (BasicBlock &B : *F) {
    // Iterate over all Instructions of a BasicBlock
    for (Instruction &I: B) {
      auto CI = dyn_cast<CallInst>(&I);
      if (CI != nullptr) {
	Function *CF=CI->getCalledFunction();
	if (CI->isIndirectCall()) {
	  return;
	}
	check_malloc(CI, CF, F, M, old_M);
      }
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////_MAIN_/////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

//---------------------------------
// Structpass::runOnModule
//---------------------------------

bool Structpass::runOnModule(Module &M) {
  initialize_StructColors(M);
  update_structatt_colors(M);
  update_elt_tys(M);
  //testing(M);
  ValueToValueMapTy VM;
  MyMapper TypeMapper = MyMapper(StructMap);
  auto new_M = CloneModuleSgx(M, VM, &TypeMapper);
  // Change the getElementPtr according to changed struct
  for (Function &F : *new_M) {
    if ((F.getName()).compare("main") == 0) {
      main=&F;
    } else {
      AllFuncs.push_back(&F);
    }
    CallFuncs.emplace(&F,std::set<Function*>());
    for (BasicBlock &B : F) {
      // Iterate over all Instructions of a BasicBlock
      for (Instruction &I: B) {
	CallInst* CI = dyn_cast<CallInst>(&I);
	if (CI != nullptr) {
	  Function *CF=CI->getCalledFunction();
	  if (!(CI->isIndirectCall()) &&
	      (CF->getName().find(".annotation") == std::string::npos) &&
	      (CF->getName().find("llvm.dbg") == std::string::npos)) {
	    CallFuncs[&F].emplace(CF);
	  }
	}
	//GetElementPtrInst* GI = dyn_cast<GetElementPtrInst>(&I);
	auto GI = dyn_cast<GetElementPtrInst>(&I);
	if (GI != nullptr) {
	  if (GI->getSourceElementType()->isStructTy()) {
	    StructType* ST = dyn_cast<StructType>(GI->getSourceElementType());
	    change_struct_getElementPtr(GI, ST, M, *new_M);
	  }
	}
      }
    }
  }

  //add main at last if main exist
  if(main != nullptr){
    AllFuncs.push_back(main);
  }

  while(!(AllFuncs.empty())){
    Function* cur=AllFuncs.back();
    sort_function(cur);
  }

  stabilized=true;

  for (auto* F : Sorted){
    treat_function(F, *new_M, M);

    if(!((Funcs[F]).terminated)){
      Sorted2.push_back(F);
    }
  }

  while(!stabilized){
    stabilized=true;
    for (auto F : Sorted2) {
      treat_function(F, *new_M, M);
    }
  }

  for (auto CI : StructMallocInst) {
    CI->eraseFromParent();
  }

  // Change all malloc
  outs() << *new_M ;
  //testing(M);
  return true;
}

static RegisterPass<Structpass> X("structpass", "a pass to modify all structs");
char Structpass::ID = 0;

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////_MAIN_END_//////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
