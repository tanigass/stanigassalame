//===- CloneModule.cpp - Clone an entire module ---------------------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file implements the CloneModule interface which makes a copy of an
// entire module.
//
//===----------------------------------------------------------------------===//

#include "CloningSgx.h"
#include <set>

#include "llvm/IR/Constant.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Module.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
using namespace llvm;

Constant* get_initializer(const Constant *In, ValueMapTypeRemapper *TypeMapper, Module &New, GlobalVariable* I) {
  auto caz = dyn_cast<ConstantAggregateZero>(In);
  auto cs = dyn_cast<ConstantStruct>(In);
  auto cd = dyn_cast<ConstantPointerNull>(In);
  auto ca = dyn_cast<ConstantArray>(In);
  auto cv = dyn_cast<ConstantVector>(In);
  if (caz) {
    return ConstantAggregateZero::get(TypeMapper->remapType(In->getType()));
  }
  if (cs) {
    StructType* ST = dyn_cast<StructType>(In->getType());
    //StructType* ST = dyn_cast<StructType>(TypeMapper->remapType(In->getType()));
    std::vector<Constant *> elts;
    StructType* new_ST = dyn_cast<StructType>(TypeMapper->remapType(ST));
    for (unsigned i=0; i<ST->getNumElements(); ++i) {
      bool test = false;
      if ((ST->getElementType(i)->isStructTy()) &&
	  (new_ST->getElementType(i)->isStructTy())){
	std::string old_name = "old." + new_ST->getElementType(i)->getStructName().str();
	test = (ST->getElementType(i)->getStructName().str().compare(old_name) == 0);
      }
      if ((ST->getElementType(i) == new_ST->getElementType(i)) || test) {
	auto cst = get_initializer(In->getAggregateElement(i), TypeMapper, New, I);
	if (cst != nullptr) {
	  elts.push_back(cst);
	} else {
	  elts.push_back(In->getAggregateElement(i));
	}
      } else {
	GlobalVariable * GV_struct;
	GV_struct = new GlobalVariable(New,
				      dyn_cast<PointerType>(new_ST->getElementType(i))->getElementType(),
				      I->isConstant(), I->getLinkage(),
				      (Constant*) nullptr, I->getName(),
				      (GlobalVariable*) nullptr,
				      I->getThreadLocalMode(),
				      dyn_cast<PointerType>(new_ST->getElementType(i))->getAddressSpace());
	StructType* ST_elt=dyn_cast<StructType>(dyn_cast<PointerType>(new_ST->getElementType(i))->getElementType());
	auto cst = get_initializer(In->getAggregateElement(i), TypeMapper, New, I);
	if (cst != nullptr) {
	  Constant* csts = ConstantStruct::get(ST_elt, cst);
	  GV_struct->setInitializer(csts);
	} else {
	  Constant* csts = ConstantStruct::get(ST_elt, In->getAggregateElement(i));
	  GV_struct->setInitializer(csts);
	}
	elts.push_back(GV_struct->stripPointerCasts());
      }
    }
    return ConstantStruct::get(new_ST, elts);
  }
  if (cd) {
    PointerType* PT = dyn_cast<PointerType>(TypeMapper->remapType(In->getType()));
    return ConstantPointerNull::get(PT);
  }
  if (ca) {
    ArrayType* AT = dyn_cast<ArrayType>(TypeMapper->remapType(In->getType()));
    std::vector<Constant *> elts;
    for (unsigned i=0; i<AT->getNumElements(); ++i) {
      auto cst = get_initializer(In->getAggregateElement(i), TypeMapper, New, I);
      if (cst != nullptr) {
	elts.push_back(cst);
      } else {
	elts.push_back(In->getAggregateElement(i));
      }
    }
    return ConstantArray::get(AT, elts);
  }
  if (cv) {
    VectorType* VT = dyn_cast<VectorType>(TypeMapper->remapType(In->getType()));
    std::vector<Constant *> elts;
    for (unsigned i=0; i<VT->getNumElements(); ++i) {
      auto cst = get_initializer(In->getAggregateElement(i), TypeMapper, New, I);
      if (cst != nullptr) {
	elts.push_back(cst);
      } else {
	elts.push_back(In->getAggregateElement(i));
      }
    }
    return ConstantVector::get(elts);
  }
  return nullptr;
}

static void copyComdat(GlobalObject *Dst, const GlobalObject *Src) {
  const Comdat *SC = Src->getComdat();
  if (!SC)
    return;
  Comdat *DC = Dst->getParent()->getOrInsertComdat(SC->getName());
  DC->setSelectionKind(SC->getSelectionKind());
  Dst->setComdat(DC);
}

/// This is not as easy as it might seem because we have to worry about making
/// copies of global variables and functions, and making their (initializers and
/// references, respectively) refer to the right globals.
///
// added TypeMapper to treat colored structs
std::unique_ptr<Module> llvm::CloneModuleSgx(const Module &M) {
  // Create the value map that maps things from the old module over to the new
  // module.
  ValueToValueMapTy VMap;
  return CloneModuleSgx(M, VMap, nullptr);
}

// added TypeMapper to treat colored structs
std::unique_ptr<Module> llvm::CloneModuleSgx(const Module &M,
				       ValueToValueMapTy &VMap,
				       ValueMapTypeRemapper *TypeMapper) {
  return CloneModuleSgx(M, VMap, TypeMapper, [](const GlobalValue *GV) { return true; });
}

std::unique_ptr<Module> llvm::CloneModuleSgx(
    const Module &M, ValueToValueMapTy &VMap,
    ValueMapTypeRemapper *TypeMapper,
    function_ref<bool(const GlobalValue *)> ShouldCloneDefinition) {

  // First off, we need to create the new module.
  std::unique_ptr<Module> New =
      std::make_unique<Module>(M.getModuleIdentifier(), M.getContext());
  New->setSourceFileName(M.getSourceFileName());
  New->setDataLayout(M.getDataLayout());
  New->setTargetTriple(M.getTargetTriple());
  New->setModuleInlineAsm(M.getModuleInlineAsm());

  // Loop over all of the global variables, making corresponding globals in the
  // new module.  Here we add them to the VMap and to the new Module.  We
  // don't worry about attributes or initializers, they will come later.
  //
  for (Module::const_global_iterator I = M.global_begin(), E = M.global_end();
       I != E; ++I) {
    GlobalVariable *GV;
    GV = new GlobalVariable(*New,
			    //I->getValueType(),
			    TypeMapper->remapType(I->getValueType()),
			    I->isConstant(), I->getLinkage(),
			    (Constant*) nullptr, I->getName(),
			    (GlobalVariable*) nullptr,
			    I->getThreadLocalMode(),
			    //I->getType()->getAddressSpace());
			    dyn_cast<PointerType>(TypeMapper->remapType(I->getType()))->getAddressSpace());
    GV->copyAttributesFrom(&*I);
    VMap[&*I] = GV;
  }
  // Loop over the functions in the module, making external functions as before
  for (const Function &I : M) {
    FunctionType *fty = dyn_cast<FunctionType>(TypeMapper->remapType(I.getValueType()));
    Function *NF = Function::Create(fty, I.getLinkage(), I.getAddressSpace(),
				    I.getName(), New.get());
    NF->copyAttributesFrom(&I);
    VMap[&I] = NF;
  }

  // Loop over the aliases in the module
  for (Module::const_alias_iterator I = M.alias_begin(), E = M.alias_end();
       I != E; ++I) {
    if (!ShouldCloneDefinition(&*I)) {
      // An alias cannot act as an external reference, so we need to create
      // either a function or a global variable depending on the value type.
      // FIXME: Once pointee types are gone we can probably pick one or the
      // other.
      GlobalValue *GV;
      if (TypeMapper->remapType(I->getValueType())->isFunctionTy())
        GV = Function::Create(cast<FunctionType>(TypeMapper->remapType(I->getValueType())),
                              GlobalValue::ExternalLinkage,
                              I->getAddressSpace(), I->getName(), New.get());
      else
        GV = new GlobalVariable(
            *New, TypeMapper->remapType(I->getValueType()), false,
	    GlobalValue::ExternalLinkage,
            nullptr, I->getName(), nullptr,
            I->getThreadLocalMode(), dyn_cast<PointerType>(TypeMapper->remapType(I->getType()))->getAddressSpace());
      VMap[&*I] = GV;
      // We do not copy attributes (mainly because copying between different
      // kinds of globals is forbidden), but this is generally not required for
      // correctness.
      continue;
    }
    auto *GA = GlobalAlias::create(TypeMapper->remapType(I->getValueType()),
                                   TypeMapper->remapType(I->getType())->getPointerAddressSpace(),
                                   I->getLinkage(), I->getName(), New.get());
    GA->copyAttributesFrom(&*I);
    VMap[&*I] = GA;
  }

  // Now that all of the things that global variable initializer can refer to
  // have been created, loop through and copy the global variable referrers
  // over...  We also set the attributes on the global now.
  //
  for (Module::const_global_iterator I = M.global_begin(), E = M.global_end();
       I != E; ++I) {
    if (I->isDeclaration())
      continue;

    GlobalVariable *GV = cast<GlobalVariable>(VMap[&*I]);
    if (!ShouldCloneDefinition(&*I)) {
      // Skip after setting the correct linkage for an external reference.
      GV->setLinkage(GlobalValue::ExternalLinkage);
      continue;
    }
    if (I->hasInitializer()) {
      auto In = I->getInitializer();
      auto cst = get_initializer(In, TypeMapper, *New, GV);
      if (cst != nullptr) {
	GV->setInitializer(MapValue(cst, VMap));
      } else {
	GV->setInitializer(MapValue(In, VMap));
      }
    }

    SmallVector<std::pair<unsigned, MDNode *>, 1> MDs;
    I->getAllMetadata(MDs);
    for (auto MD : MDs)
      GV->addMetadata(MD.first,
                      *MapMetadata(MD.second, VMap, RF_MoveDistinctMDs));

    copyComdat(GV, &*I);
  }

  // Similarly, copy over function bodies now...
  //
  for (const Function &I : M) {
    if (I.isDeclaration())
      continue;
    Function *F = cast<Function>(VMap[&I]);

    if (!ShouldCloneDefinition(&I)) {
      // Skip after setting the correct linkage for an external reference.
      F->setLinkage(GlobalValue::ExternalLinkage);
      // Personality function is not valid on a declaration.
      F->setPersonalityFn(nullptr);
      continue;
    }

    Function::arg_iterator DestI = F->arg_begin();
    for (Function::const_arg_iterator J = I.arg_begin(); J != I.arg_end();
         ++J) {
      DestI->setName(J->getName());
      VMap[&*J] = &*DestI++;
    }

    SmallVector<ReturnInst *, 8> Returns; // Ignore returns cloned.
    CloneFunctionInto(F, &I, VMap, /*ModuleLevelChanges=*/true, Returns, "", nullptr, TypeMapper, nullptr);

    if (I.hasPersonalityFn())
      F->setPersonalityFn(MapValue(I.getPersonalityFn(), VMap));

    copyComdat(F, &I);
  }

  // And aliases
  for (Module::const_alias_iterator I = M.alias_begin(), E = M.alias_end();
       I != E; ++I) {
    // We already dealt with undefined aliases above.
    if (!ShouldCloneDefinition(&*I))
      continue;
    GlobalAlias *GA = cast<GlobalAlias>(VMap[&*I]);
    if (const Constant *C = I->getAliasee())
      GA->setAliasee(MapValue(C, VMap));
  }

  // And named metadata....
  const auto* LLVM_DBG_CU = M.getNamedMetadata("llvm.dbg.cu");
  for (Module::const_named_metadata_iterator I = M.named_metadata_begin(),
                                             E = M.named_metadata_end();
       I != E; ++I) {
    const NamedMDNode &NMD = *I;
    NamedMDNode *NewNMD = New->getOrInsertNamedMetadata(NMD.getName());
    if (&NMD == LLVM_DBG_CU) {
      // Do not insert duplicate operands.
      SmallPtrSet<const void*, 8> Visited;
      for (const auto* Operand : NewNMD->operands())
        Visited.insert(Operand);
      for (const auto* Operand : NMD.operands()) {
        auto* MappedOperand = MapMetadata(Operand, VMap);
        if (Visited.insert(MappedOperand).second)
          NewNMD->addOperand(MappedOperand);
      }
    } else
      for (unsigned i = 0, e = NMD.getNumOperands(); i != e; ++i)
        NewNMD->addOperand(MapMetadata(NMD.getOperand(i), VMap));
  }

  return New;
}

extern "C" {

LLVMModuleRef LLVMCloneModule(LLVMModuleRef M) {
  return wrap(CloneModuleSgx(*unwrap(M)).release());
}

}
