#pragma once // equivalent to #ifndef MYFILE #define MYFILE ... #endif

#include "CloningSgx.h"
#include "llvm_utils.hpp"

#include "llvm/Pass.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/InstrTypes.h"

#include "llvm/Transforms/Utils/Cloning.h"

#include "llvm/IR/ValueHandle.h"
#include "llvm/Transforms/Utils/ValueMapper.h"

#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/TypeSize.h"
#include "llvm/IR/DataLayout.h"

#include "llvm/IR/Operator.h"

#include <set>
#include <map>


using namespace llvm;

class MyMapper : public ValueMapTypeRemapper {
public:

  MyMapper() {
  }

  // constuctors/destructors
  MyMapper(std::map<Type*, Type*> myMap) {
    mapper = myMap;
  }

  std::map<Type*, Type*> mapper; /* old type -> new type */
  Type* remapType(Type* src) override {
    if (src == nullptr) {
      return src;
    }
    int count = 0;
    Type* ty = src;
    while (ty->isPointerTy()) {
      ty=dyn_cast<PointerType>(ty)->getElementType();
      ++count;
    }

    if ((!ty->isStructTy()) && (!ty->isFunctionTy()) && (!ty->isArrayTy()) && (!ty->isVectorTy())) {
      return src;
    }
    Type* new_ty;
    // this part create a new functiontype according to new struct
    if (ty->isFunctionTy()) {
      FunctionType* fty = dyn_cast<FunctionType>(ty);
	std::vector<Type* > argTys;
	for (unsigned i=0; i<fty->getNumParams(); ++i) {
	  argTys.push_back(remapType(fty->getParamType(i)));
	}
	new_ty = FunctionType::get(remapType(fty->getReturnType()), argTys, fty->isVarArg());
    } else if (ty->isArrayTy()) {
      new_ty = ArrayType::get(remapType(ty->getArrayElementType()), ty->getArrayNumElements());
    } else if (ty->isVectorTy()) {
      new_ty = VectorType::get(remapType(ty->getVectorElementType()), ty->getVectorNumElements(), dyn_cast<VectorType>(ty)->isScalable());
    } else if (ty->isStructTy()) {
      if (mapper.find(dyn_cast<StructType>(ty)) == mapper.end()) {
	return src;
      }
      new_ty = mapper[dyn_cast<StructType>(ty)];
    }
    while (count != 0) {
      new_ty=new_ty->getPointerTo();
      --count;
    }

    return new_ty;
  }

};

class StructAttributes final {
public:
  StructAttributes() {
  }
  // constuctors/destructors
  StructAttributes(StructType* ST, StructType* new_ST)
    : colors(ST->getNumElements(), "")
  {
    this->ST = ST;
    this->new_ST = new_ST;
  }
  void print() {
    errs() << "old  :  " << *ST<< "\n";
    errs() << "new  :  " << *new_ST<< "\n";
    errs() << "Colors : ";
    for (auto i : colors) {
      errs() << i<<", ";
    }
    errs() << "\n";
    errs() << "elements() : ";
    for (auto i : elt_tys) {
      errs() << *i<<", ";
    }
    errs() << "\n\n";
  }

public:
  StructType* ST;
  StructType* new_ST;
  std::vector<Type *> elt_tys;
  std::vector<std::string> colors;
};

class FunctionAttributes final {
public:
  // constuctors/destructors
  FunctionAttributes() {
    terminated = true;
    isMalloc = false;
  }
  FunctionAttributes(const Function* F) {
    terminated = true;
    isMalloc = false;
  }

public:
  bool terminated;
  bool correctness;
  bool isMalloc;
  std::set<CallInst*> MallocInsts;
};

class Structpass : public ModulePass {
  using parent_t = ModulePass;

public:
  // Constructors
  Structpass() : parent_t(ID) {}

  // Public methods
  bool runOnModule(Module &M) override;

  // Public for llvm internal work
  static char ID;

private:
  // private methods
  void initialize_StructColors(Module &M);
  void testing(Module &M);
  Type* get_changed_type(Type *ty_init);
  void update_elt_tys(Module &M);
  void update_structatt_colors(Module &M);
  void treat_colored_struct(const CallInst* CI, std::string color);
  void change_struct_getElementPtr(GetElementPtrInst *GI, StructType *ST, Module &M, Module &new_M);

  //malloc related
  void sort_function(Function* cur);
  void treat_function(Function* F, Module &M, Module &old_M);
  void check_malloc(CallInst* CI, Function* CF, Function* F, Module &M, Module &old_M);
  bool malloc_to_xalloc(CallInst* CI, Module &M, float size);
  Function* create_xalloc(Module &M);
  bool is_malloc(std::string name);
  float is_struct_malloc(CallInst* CI, Module &M, Module &old_M);
  void update_isMalloc(CallInst* CI, Function* CF, Function* F);

  // private members
  std::map<StructType*, StructAttributes> StructColors;
  std::map<Type*, Type*> StructMap;

  /////////////////////////_related_to_function_sorting_/////////////////////////
  // list of all function
  std::vector<Function*> AllFuncs;
  // map associating evry functions with it's called functions
  std::map<Function *,std::set<Function*>> CallFuncs;
  std::vector<Function*> Sorted;
  std::vector<Function*> Sorted2;
  bool stabilized = true;
  Function* main = nullptr;

  // malloc related
  std::map<const Function*, FunctionAttributes> Funcs;
  std::set<CallInst*> StructMallocInst;
};
