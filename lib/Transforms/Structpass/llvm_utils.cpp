#include "llvm_utils.hpp"

//-----------------------------------------
//get_num_GlobalVariable (llvm.global.annotations)
//-----------------------------------------

unsigned int get_num_GlobalVariable(const GlobalVariable& G){
  /*
    auto a5 = dyn_cast<ConstantArray>(G->getOperand(0))->getType();
    // *a5 :: [2 x { i8*, i8*, i8*, i32 }]
    */
  return dyn_cast<ConstantArray>(G.getOperand(0))->getType()->getNumElements();
}
  
//-----------------------------------------
//get_annotation (llvm.global.annotations)
//-----------------------------------------

std::string get_annotation(const GlobalVariable& G, unsigned int i){
  /*
  //guide to understand following line of code Example in case of 1 annoted global Variable
  auto a0 = dyn_cast<ConstantArray>(G.getOperand(0));
  // *a0 :: [1 x { i8*, i8*, i8*, i32 }] [{ i8*, i8*, i8*, i32 } { i8* bitcast (i32* @global_a to i8*), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.5, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.1, i32 0, i32 0), i32 12 }]
  auto a1=dyn_cast<ConstantStruct>(a0->getOperand(0));
  // *a1 :: { i8*, i8*, i8*, i32 } { i8* bitcast (i32* @global_a to i8*), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.5, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.1, i32 0, i32 0), i32 12 }
  auto a2=a1->getOperand(1);
  // *a2 :: i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.5, i32 0, i32 0)
  auto a3=dyn_cast<GlobalVariable>(a2->getOperand(0));
  // *a3 :: @.str.5 = private unnamed_addr constant [8 x i8] c"magenta\00", section "llvm.metadata"
  auto a4=dyn_cast<ConstantDataArray>(a3->getInitializer());
  // *a4 :: [8 x i8] c"magenta\00"
  StringRef global_color_1=a4->getAsCString();
  //color :: magenta
  */
  return dyn_cast<ConstantDataArray>
    (dyn_cast<GlobalVariable>
     (
      (dyn_cast<ConstantStruct>
       (dyn_cast<ConstantArray>
	(G.getOperand(0)
	 )->getOperand(i)
	)->getOperand(1)
       )->getOperand(0)
      )->getInitializer()
     )->getAsCString();
}

//------------------------------------
//get_GlobalVariable
//-----------------------------------

Value* get_GlobalVariable(const GlobalVariable& G, unsigned int i){
  return dyn_cast<Value>
    (
     (dyn_cast<ConstantStruct>
      (dyn_cast<ConstantArray>
       (G.getOperand(0)
	)->getOperand(i)
       )->getOperand(0)
      )->getOperand(0)
     );
}

//-----------------------------------------
//get_annotation (llvm.ptr.annotations, llvm.var.annotations)
//-----------------------------------------

std::string get_annotation(const CallInst& CI){
  /*
  //guide to understand following line of code
  auto a1=dyn_cast<ConstantExpr>(CI->getArgOperand(1));
  //*a :: i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0)
  auto a2=dyn_cast<GlobalVariable>(a1->getOperand(0));
  //*a2 :: @.str = private unnamed_addr constant [5 x i8] c"cyan\00", section "llvm.metadata"
  auto a3=dyn_cast<ConstantDataArray>(a2->getInitializer());
  //*a3 :: [5 x i8] c"cyan\00"
  StringRef color=a3->getAsCString();
  //color :: cyan
  */
  return dyn_cast<ConstantDataArray>
    (dyn_cast<GlobalVariable>
     (dyn_cast<ConstantExpr>
      (CI.getArgOperand(1)
       )->getOperand(0)
      )->getInitializer()
     )->getAsCString();

}

//------------------------------------
//get_Value ( llvm.var.annotations)
//-----------------------------------

Value* get_Value(const CallInst& CI){
  /*
  //guide to understand following line of code
  Value *v1 =CI->getOperand(0);
  // v1 :: %d2 = bitcast i32* %d to i8*
  Value *v2 = (dyn_cast<Instruction>(v))->getOperand(0);
  // v2 :: %d = alloca i32, align 4
  */
  return (dyn_cast<Instruction>
	  (CI.getOperand(0)
	   )
	  )->getOperand(0);
}


//------------------------------------
//is_parent_of
//-----------------------------------

//return true if PBB is a parent of CBB
bool is_parent_of(BasicBlock* PBB, BasicBlock* CBB){
  for (auto BB=pred_begin(CBB); BB!=pred_end(CBB); ++BB){
    // To test if the PPB is a parent node of CBB
    if (PBB == *BB) {
      return true;
    }
  }
  return false;
}

//----------------------------------
//get_fn_from_GV
//---------------------------------
Function* get_fn_from_GV(const GlobalVariable& G) {
  /*
  //guide to understand following line of code
  // G
  // @__monochromatic_printf = internal global i8* bitcast (i32 (i8*, ...)* @printf to i8*), align 8, !dbg !0
  Value* a1=G.getOperand(0);
  // i8* bitcast (i32 (i8*, ...)* @printf to i8*)
  Value* a2=(dyn_cast<BitCastOperator>(a1))->getOperand(0);
  // ; Function Attrs: nounwind
  // declare dso_local i8* @memcpy(i8*, i8*, i64) #1
  Function* a3=dyn_cast<Function>(a2);
  // ; Function Attrs: nounwind
  // declare dso_local i8* @memcpy(i8*, i8*, i64) #1
  */
  Value* a1=G.getOperand(0);
  Value* a2=(dyn_cast<BitCastOperator>(a1))->getOperand(0);
  Function* a3=dyn_cast<Function>(a2);
  return a3;
}
